//
//  AlertController.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

typealias ActionHandler  = (_ title: String) -> Void
typealias FieldActionHandler = (_ title: String,_ fieldText: String) -> Void

extension UIAlertController {
    
    //MARK:  Show Message
    class func show(_ sender: UIViewController, _ title : String?, _ message : String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
    }
    
    //MARK:  Show Message with Multiple Options
    class func showWithAction(_ sender: UIViewController, _ title: String?, _ message: String?, _ cancelTitle: String? = nil, _ destructiveOptions: [String]? = nil, _ otherOptions: [String]? = nil, _ isAlert: Bool = true, _ completion: @escaping ActionHandler) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: isAlert ? .alert : .actionSheet)
        
        if let cancel =  cancelTitle {
            alertController.addAction(UIAlertAction(title: cancel, style: .cancel, handler: {
                (action : UIAlertAction) -> Void in
                completion(action.title!)
            }))
        }
        if let distructive = destructiveOptions, distructive.count > 0 {
            for title in distructive {
                alertController.addAction(UIAlertAction(title: title, style: .destructive, handler: {
                    (action : UIAlertAction) -> Void in
                    completion(action.title!)
                }))
            }
        }
        if let others = otherOptions, others.count > 0 {
            for title in others {
                alertController.addAction(UIAlertAction(title: title, style: .default, handler: {
                    (action : UIAlertAction) -> Void in
                    completion(action.title!)
                }))
            }
        }
        
        DispatchQueue.main.async {
            if !isAlert && UIDevice.current.userInterfaceIdiom == .pad {
                alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                alertController.popoverPresentationController?.sourceRect = self.sourceRectForBottomAlertController(alertController)();
                alertController.popoverPresentationController!.sourceView = sender.view;
                sender.present(alertController, animated: true, completion: nil)
            }
            else {
                sender.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:  Show Message with textFields
    class func showAlertWithFields(_ sender: UIViewController, _ title: String?, _ message: String?, _ placeholderString: String? = nil, _ cancelTitle: String? = nil, _ destructiveOptions: [String]? = nil, _ otherOptions: [String]?, _ completion: @escaping FieldActionHandler) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController .addTextField(configurationHandler: { (textField) in
            if let placeholder = placeholderString {
                textField.attributedPlaceholder = NSAttributedString(string:placeholder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
            }
        })
        if let cancel =  cancelTitle {
            alertController.addAction(UIAlertAction(title: cancel, style: .cancel, handler: {
                (action : UIAlertAction) -> Void in
                if let textField = alertController.textFields?[0] {
                    completion(action.title!, textField.text!)
                }
            }))
        }
        if let distructive = destructiveOptions, distructive.count > 0 {
            for title in distructive {
                alertController.addAction(UIAlertAction(title: title, style: .destructive, handler: {
                    (action : UIAlertAction) -> Void in
                    if let textField = alertController.textFields?[0] {
                        completion(action.title!, textField.text!)
                    }
                }))
            }
        }
        if let others = otherOptions, others.count > 0 {
            for title in others {
                alertController.addAction(UIAlertAction(title: title, style: .default, handler: {
                    (action : UIAlertAction) -> Void in
                    if let textField = alertController.textFields?[0] {
                        completion(action.title!, textField.text!)
                    }
                }))
            }
        }
        sender.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:  Private Methods
    private func sourceRectForBottomAlertController() -> CGRect {
        var sourceRect :CGRect = CGRect.zero
        sourceRect.origin.x = self.view.bounds.midX - self.view.frame.origin.x/2.0
        sourceRect.origin.y = self.view.bounds.midY
        return sourceRect
    }
}
