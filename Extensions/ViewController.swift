//
//  FMViewController.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIViewController {
   
    private func _storyboard() ->  UIStoryboard? {
        return UIStoryboard(name: AppKey.main, bundle:nil)
    }
    private func accountStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.accountSB,bundle:nil)
    }
    private func portfolioStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.portfolioSB,bundle:nil)
    }
    private func watchListStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.watchList,bundle:nil)
    }
    private func companyStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.companyDetail,bundle:nil)
    }
    private func historyStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.history,bundle:nil)
    }
    private func bankingStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.banking,bundle:nil)
    }
    private func settingStoryboard() -> UIStoryboard?{
        return UIStoryboard(name: AppKey.setting,bundle:nil)
    }
    
    //MARK: Main Storyboard
    
    public func leftBarVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: LeftBarVC.identifier()) as? LeftBarVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func firstVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: FirstVC.identifier()) as? FirstVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func loginVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: LoginVC.identifier()) as? LoginVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpNameVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpNameVC.identifier()) as? SignUpNameVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpEmailVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpEmailVC.identifier()) as? SignUpEmailVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpPasswordVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpPasswordVC.identifier()) as? SignUpPasswordVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpToPhoneNoVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpPhoneNoVC.identifier()) as? SignUpPhoneNoVC {
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpDOBVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpDOBVC.identifier()) as? SignUpDOBVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpSSNVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpSSNVC.identifier()) as? SignUpSSNVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpVeriCodeVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpVeriCodeVC.identifier()) as? SignUpVeriCodeVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpResidentialVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpResidentialVC.identifier()) as? SignUpResidentialVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpMapVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpMapVC.identifier()) as? SignUpMapVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpResidentialDetailVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpResidentialDetailVC.identifier()) as? SignUpResidentialDetailVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpCountryCodeVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpCountryCodeVC.identifier()) as? SignUpCountryCodeVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpInvestmentExpVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpInvestmentExpVC.identifier()) as? SignUpInvestmentExpVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpEmployementVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpEmployementVC.identifier()) as? SignUpEmployementVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpVeriIdentityVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpVeriIdentityVC.identifier()) as? SignUpVeriIdentityVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpBrokerVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpBrokerVC.identifier()) as? SignUpBrokerVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpPublicCompanyVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpPublicCompanyVC.identifier()) as? SignUpPublicCompanyVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpVerifyBankAccount() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpVerifyBankAccount.identifier()) as? SignUpVerifyBankAccount{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpTermAndConVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpTermAndConVC.identifier()) as? SignUpTermAndConVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpDocumentVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpDocumentVC.identifier()) as? SignUpDocumentVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpDocumentOriginVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpDocumentOriginVC.identifier()) as? SignUpDocumentOriginVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpDocumentImageVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpDocumentImageVC.identifier()) as? SignUpDocumentImageVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func cameraViewController() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: CameraViewController.identifier()) as? CameraViewController{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpFinancialVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpFinancialVC.identifier()) as? SignUpFinancialVC{
            return viewController
        }
        return UIViewController()
    }
    public func signUpGoalVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpGoalVC.identifier()) as? SignUpGoalVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func signUpThankyouVC() -> UIViewController {
        if let viewController = _storyboard()?.instantiateViewController(withIdentifier: SignUpThankyouVC.identifier()) as? SignUpThankyouVC{
            return viewController
        }
        return UIViewController()
    }
    
    
    //MARK: Account Storyboard
    public func homeVC() -> UIViewController {
            if let viewController = accountStoryboard()?.instantiateViewController(withIdentifier: HomeVC.identifier()) as? HomeVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func searchCompanyVC() -> UIViewController {
        if let viewController = accountStoryboard()?.instantiateViewController(withIdentifier: SearchCompanyVC.identifier()) as? SearchCompanyVC{
            return viewController
        }
        return UIViewController()
    }
    
    //MARK: Portfolio Storyboard
    public func portfolioVC() -> UIViewController{
        if let viewController = portfolioStoryboard()?.instantiateViewController(withIdentifier: PortfolioVC.identifier()) as? PortfolioVC{
            return viewController
        }
        return UIViewController()
        
    }
    
    //MARK: watchlist Storyboard
    
    public func watchListVC() -> UIViewController {
        if let viewController = watchListStoryboard()?.instantiateViewController(withIdentifier: WatchListVC.identifier()) as? WatchListVC{
            return viewController
        }
        return UIViewController()
    }
    
    //MARK: Company Details Storyboard
    public func companyVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: "DashboardTabBarController") as? DashboardTabBarController{
        //if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC{
            return viewController
        }
        return UIViewController()
    }
    public func companyDetailsVC() -> UIViewController {
            if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC{
            return viewController
        }
        return UIViewController()
    }
    public func statsVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: StatsVC.identifier()) as? StatsVC{
            return viewController
        }
        return UIViewController()
    }
    public func newsVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: NewsVC.identifier()) as? NewsVC{
            return viewController
        }
        return UIViewController()
    }
    public func infoVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: InfoVC.identifier()) as? InfoVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func buySharesVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: BuySharesVC.identifier()) as? BuySharesVC{
            return viewController
        }
        return UIViewController()
    }
    public func purchasedShareDetailsVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: PurchasedShareDetailsVC.identifier()) as? PurchasedShareDetailsVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func locateVC() -> UIViewController {
        if let viewController = companyStoryboard()?.instantiateViewController(withIdentifier: LocateVC.identifier()) as? LocateVC{
            return viewController
        }
        return UIViewController()
    }
    
    //MARK: History Storyboard

    public func historyVC() -> UIViewController {
        if let viewController = historyStoryboard()?.instantiateViewController(withIdentifier: HistoryVC.identifier()) as? HistoryVC{
            return viewController
        }
        return UIViewController()
    }
    public func orderDetailVC() -> UIViewController {
        if let viewController = historyStoryboard()?.instantiateViewController(withIdentifier: OrderDetailVC.identifier()) as? OrderDetailVC{
            return viewController
        }
        return UIViewController()
    }

    //MARK: Banking Storyboard
    
    public func bankingVC() -> UIViewController {
        if let viewController = bankingStoryboard()?.instantiateViewController(withIdentifier: BankingVC.identifier()) as? BankingVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func transferToBankVC() -> UIViewController {
        if let viewController = bankingStoryboard()?.instantiateViewController(withIdentifier: TransferToBankVC.identifier()) as? TransferToBankVC{
            return viewController
        }
        return UIViewController()
    }
    
    //MARK: Setting Storyboard
    public func settingVC() -> UIViewController {
        if let viewController = settingStoryboard()?.instantiateViewController(withIdentifier: SettingVC.identifier()) as? SettingVC{
            return viewController
        }
        return UIViewController()
    }
    
    public func profileUpdateVC() -> UIViewController {
        if let viewController = settingStoryboard()?.instantiateViewController(withIdentifier: ProfileUpdateVC.identifier()) as? ProfileUpdateVC{
            return viewController
        }
        return UIViewController()
    }
    
    



}
