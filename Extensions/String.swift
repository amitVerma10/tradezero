//
//  FMString.swift
//  TradeZero
//
//  Created by Amit Verma on 5/22/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

extension String {
    
    public func isValidEmail() -> Bool {
        let stricterFilterString : String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest : NSPredicate = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return emailTest.evaluate(with: self)
    }
    public func isValidSocialSecurityNumber() -> Bool {//"^\d{9}|\d{3}-\d{2}-\d{4}$
        let stricterFilterString : String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest : NSPredicate = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return emailTest.evaluate(with: self)
    }
    
    public func validatePhoneNumber() -> String {
        var phoneNumber = self.replacingOccurrences(of: "-", with: "")
        phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
        phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
        return phoneNumber
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    public func convertTimeStampToDate() -> String {
        let unixTimestamp:UInt32 = UInt32(self)!
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    public func convertTimeStampToDate64Bit() -> String {
        let unixTimestamp:UInt64 = UInt64(self)!
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    public func convertTimeStampToDateString() -> String {
        let unixTimestamp:UInt64 = UInt64(self)!
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    public func convertTimeStampToTimeString() -> String {
        let unixTimestamp:UInt64 = UInt64(self)!
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    public func trimSpace() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func length() -> Int {
        return self.characters.count
    }
    
    func heightForWithFont(_ width: CGFloat, _ insets: UIEdgeInsets) -> CGFloat {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width + insets.left + insets.right, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: "Helventica", size: 15.0)
        label.text = self
        
        label.sizeToFit()
        return label.frame.height + insets.top + insets.bottom
    }
    
    func sizeOfString (_ width: CGFloat = CGFloat.greatestFiniteMagnitude, font : UIFont, height: CGFloat = CGFloat.greatestFiniteMagnitude, drawingOption: NSStringDrawingOptions = NSStringDrawingOptions.usesLineFragmentOrigin) -> CGSize {
        return NSString(string: self).boundingRect(with: CGSize(width: width, height: height), options: drawingOption, attributes: [NSAttributedStringKey.font: font], context: nil).size
    }
    
    func utf8String(_ plusForSpace: Bool=false) -> String {
        var encoded = ""
        let unreserved = "*-._"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        
        if plusForSpace {
            allowed.addCharacters(in: " ")
        }
        if let encode = addingPercentEncoding(withAllowedCharacters:allowed as CharacterSet) {
            if plusForSpace {
                encoded = encode.replacingOccurrences(of: " ", with: "+")
            }
        }
        return encoded
    }
    
    func attributedString() -> NSMutableAttributedString {
        let range = (self as NSString).range(of: "*")
        let attributedString = NSMutableAttributedString(string:self)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColor.lightGray, range: NSRange(location: 0, length: self.characters.count))
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: AppColor.lightGray, range: range)
        return attributedString
    }
    
    var languageCode: String {
        switch self {
        case KEnglish:
            return "en"
        default:
            return "en"
        }
    }
    
    func localized() -> String {
        if let path = Bundle.main.path(forResource: UserPreferences.getLanguage().languageCode, ofType: "lproj") {
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        else if let path = Bundle.main.path(forResource: "Base", ofType: "lproj") {
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        return self
    }
 
}

extension Array {
    
    func containsObject<T>(_ obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
    
    public func jsonString() -> String {
        do {
            let jsonData: NSData =  try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
            
            return String(data: jsonData as Data, encoding: String.Encoding.utf8)!
            
        } catch {
            print(error.localizedDescription)
        }
        return String()
    }
}

extension Data {
    
    var string: String {
        return String(data: self, encoding: .utf8) ?? ""
    }
}


extension UINavigationController {
    
    public func setupNavigationBar() {
        self.interactivePopGestureRecognizer?.isEnabled = false
        self.isNavigationBarHidden = true
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.tintColor = AppColor.lightGray
        self.navigationBar.clipsToBounds = false
      //  self.navigationBar.titleTextAttributes = [NSFontAttributeName : UIFont(name: AppFont.helventica, size: Device.UserInterfaceIdiom.pad ? 20.0 : 18.0) as Any, NSForegroundColorAttributeName: AppColor.lightGray]
    }
}

extension UITableView {
    
    public func deleteEmptyRow() {
        self.tableHeaderView = UIView(frame: CGRect.zero)
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
}


extension UIView {
    func applyGradient(_ colours: [UIColor]) -> Void {
        self.applyGradient(colours, nil)
    }
    
    func applyGradient(_ colours: [UIColor], _ locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
}
