//
//  FMImageView.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

extension UIImageView {
    
    public func loadGif(_ name: String) {
        DispatchQueue.global().async {
            let image = UIImage.gif(name: name)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
    public func circularImageView(_ color: UIColor = AppColor.lightGray) -> UIImageView {
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.size.height / 2
            self.clipsToBounds = true
            self.layer.borderWidth = 1.0
            self.layer.borderColor = color.cgColor
        }
        return self
    }
        
}
