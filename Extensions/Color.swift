//
//  FMColor.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

extension UIColor {
    
    //MARK:  Get color from RGB
    public class func color(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat? = 1.0) -> UIColor {
        return UIColor(red:red / 255.0, green:green / 255.0, blue:blue / 255.0, alpha:CGFloat(alpha!))
    }
    
    //MARK:  Get color from hex string
    public class func color(_ hexString: String, _ alpha: Double) -> UIColor {
        let hexValue:UInt32 = UInt32(hexString)!
        let red = CGFloat((hexValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hexValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hexValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    //MARK:  Get color from hex string
    public class func hexColor(_ hex: String) -> UIColor {
        var cString = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0)
        )
    }
    
      public  class func hexStr ( hexStr : NSString, alpha : CGFloat) -> UIColor {
            let hexStr = hexStr.replacingOccurrences(of: "#", with: "")
            let scanner = Scanner(string: hexStr as String)
            var color: UInt32 = 0
            if scanner.scanHexInt32(&color) {
                let r = CGFloat((color & 0xFF0000) >> 16) / 255.0
                let g = CGFloat((color & 0x00FF00) >> 8) / 255.0
                let b = CGFloat(color & 0x0000FF) / 255.0
                return UIColor(red:r,green:g,blue:b,alpha:alpha)
            } else {
                print("invalid hex string", terminator: "")
                return UIColor.white;
            }
        }
    
}
