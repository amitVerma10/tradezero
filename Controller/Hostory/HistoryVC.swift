//
//  HistoryVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/21/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import PageMenu

class HistoryVC: BaseVC ,CAPSPageMenuDelegate{
    
    var pageMenu : CAPSPageMenu?
    
    //Table view outlets
    @IBOutlet weak var tblAll: UITableView!
    @IBOutlet weak var tblAdd: UITableView!
    @IBOutlet weak var tblTransfer: UITableView!
    @IBOutlet weak var tblBuyOrSell: UITableView!
    
    var allOrderArr = [Any]()
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "HistoryVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let storyboard = UIStoryboard(name: "History", bundle: nil)
        let channelVC = storyboard.instantiateViewController(withIdentifier: "HistoryAll") as! HistoryAll//replace with Order
        let channelVC1 = storyboard.instantiateViewController(withIdentifier: "HistoryAdd") as! HistoryAdd// replace with Trade
       
        
        channelVC.title = "Order"
        channelVC.parentNavigationController = self.navigationController
        channelVC1.title = "Trade"
        channelVC1.parentNavigationController = self.navigationController
       
        controllerArray.append(channelVC)
        controllerArray.append(channelVC1)
    
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor(red: 13.0/255.0, green: 11.0/255.0, blue: 32.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .menuItemFont(UIFont(name: "Raleway-SemiBold", size: 17.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 84.0, width: self.view.frame.width, height: self.view.frame.height - 84.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
        
        

        // Do any additional setup after loading the view.
    }
    
    @objc func openOrders(_ notification: NSNotification){
        guard let orderArr = notification.userInfo!["orders"] as? [KeyValue],orderArr.count > 0 else{
            UIAlertController.show(self, "Message", "No News found!")
            return
        }
        allOrderArr = orderArr
        tblAll.reloadData()
        
    }
    
    @IBAction func filterAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in ["All", "Accepted", "Fill", "Cancel","Reject"] {
            alert.view.tintColor = AppColor.backGroundColor
            alert.addAction(UIAlertAction(title: i, style: .default, handler: actionSheetHandler))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func actionSheetHandler(action: UIAlertAction) {
        //Use action.title
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OrderHistoryFilter"), object: nil, userInfo: ["type":action.title ?? "All"])
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
    }
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
    }
    @IBAction func backAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HistoryVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(orderDetailVC(), animated: true)
    }
    
}
