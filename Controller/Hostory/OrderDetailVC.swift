//
//  OrderDetailVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/21/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class OrderDetailVC: BaseVC {
    
    var orderDic = KeyValue()
    var orderId = String()
    var orderType = String()
    var timeStamp = String()
    var orderStatus = String()
    var quantity = String()
    
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var lblTimeInForce: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblLimitPrice: UILabel!
    @IBOutlet weak var lblEnteredQuantity: UILabel!
    
    @IBOutlet weak var btnCancel: CustomButton!
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "OrderDetailVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // call order details api
        
        lblOrderStatus.text = orderStatus
        lblDate.text = timeStamp
        lblOrderTime.text = orderType
        lblEnteredQuantity.text = quantity
        
        if orderStatus == "Cancel" || orderStatus == "Fill" || orderStatus == "Reject"{
            btnCancel.isHidden = true
        }else{
            btnCancel.isHidden = false
        }
        
        orderDic["orderId"] = orderId
        orderDic["accountId"] = userAccountId
        WebSocketManager.sharedInstance.orderDetailRequrst(orderDic: orderDic)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.orderDetailsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.orderDetailsResponse), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.cancelOrderData(_:)), name: NSNotification.Name(rawValue: SocketConstants.orderApiResponse), object: nil)

        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func orderDetailsData(_ notification : NSNotification){
        guard let orderData = notification.userInfo!["data"] as? KeyValue else{
            return
        }
        self.loadvalue(orderData)
        
    }
    
    @objc func cancelOrderData(_ notification : NSNotification){
        
        guard let orderData = notification.userInfo!["data"] as? KeyValue else{
            return
        }
        if orderData["eventType"] as! String == "Cancel"{
            btnCancel.isHidden = true
            lblOrderStatus.text = "Cancel"
        }else{
            UIAlertController.show(self, AppTitle.appTital, orderData["message"] as? String)
        }
       
        
    }
    
    private func loadvalue(_ orderDic : KeyValue ){
        print(orderDic)
        
        lblLimitPrice.text = orderDic["status"] as? String
        lblTimeInForce.text = orderDic["timeInForce"] as? String
        
        
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        
         WebSocketManager.sharedInstance.cancelOrderRequrst(orderId : orderId)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    

}
