//
//  SegmentVC.swift
//  TradeZero
//
//  Created by Amit Verma on 12/4/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import PageMenu


class SegmentVC: UIViewController ,CAPSPageMenuDelegate{
     var pageMenu : CAPSPageMenu?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.navigationController?.isNavigationBarHidden = true
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let storyboard = UIStoryboard(name: "History", bundle: nil)
        let channelVC = storyboard.instantiateViewController(withIdentifier: "HistoryTableVC") as! HistoryTableVC
        let channelVC1 = storyboard.instantiateViewController(withIdentifier: "HistoryTableVC") as! HistoryTableVC
        let channelVC2 = storyboard.instantiateViewController(withIdentifier: "HistoryTableVC") as! HistoryTableVC
        let channelVC3 = storyboard.instantiateViewController(withIdentifier: "HistoryTableVC") as! HistoryTableVC
        
        channelVC.title = "1"
        channelVC1.title = "12"
        channelVC2.title = "123"
        channelVC3.title = "123"
        controllerArray.append(channelVC)
        controllerArray.append(channelVC1)
        controllerArray.append(channelVC2)
        controllerArray.append(channelVC3)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)),
            .menuMargin(20.0),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 64.0, width: self.view.frame.width, height: self.view.frame.height - 64.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)

        // Do any additional setup after loading the view.
    }

    @IBAction func dotAction(_ sender: Any) {
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
    }
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
