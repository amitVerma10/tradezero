//
//  HistoryAdd.swift
//  TradeZero
//
//  Created by Amit Verma on 12/5/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import NotificationBannerSwift



class HistoryAdd: UIViewController {
//Class History Add is using in Trade History
    
    
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    var strFromDate = String()
    var strToDate = String()
    
    
    @IBOutlet weak var btnApply: CustomButton!
    
    var parentNavigationController : UINavigationController?
    var historyArr = [KeyValue]()
    
    @IBOutlet weak var tblAdd: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*btnApply.isUserInteractionEnabled = false
        btnApply.alpha = 0.5*/
        
        let datePickerView: UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        txtFromDate.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(handelDatePicker(_:)), for: UIControlEvents.valueChanged)
        
        
        let datePickerViewTo: UIDatePicker = UIDatePicker()
        datePickerViewTo.datePickerMode = UIDatePickerMode.date
        datePickerViewTo.maximumDate = Date()
        txtToDate.inputView = datePickerViewTo
        
        datePickerViewTo.addTarget(self, action: #selector(handelDatePickerTo(_:)), for: UIControlEvents.valueChanged)
        
    
        
        txtFromDate.addTarget(self, action: #selector(HistoryAdd.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtToDate.addTarget(self, action: #selector(HistoryAdd.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tradeHistoryData(_:)), name: NSNotification.Name(rawValue: SocketConstants.tradeHistoryNotification), object: nil)

        // Do any additional setup after loading the view.
    }
    
    
    @objc func tradeHistoryData(_ notification : NSNotification){
        
        guard let orderArr = notification.userInfo!["history"] as? [KeyValue],orderArr.count > 0 else{
            
            let banner = StatusBarNotificationBanner(title: "No Record found!", style: .danger)
            banner.show()
            
            return
            
        }
        historyArr = orderArr
        tblAdd.reloadData()
    }
    
    @objc func handelDatePicker(_ sender: UIDatePicker)
    {
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        strFromDate = dateFormatter.string(from:sender.date)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let textFrom = dateFormatter1.string(from:sender.date)
        txtFromDate.text = textFrom
        
    }
    @objc func handelDatePickerTo(_ sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        strToDate = dateFormatter.string(from:sender.date)
        
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MM/dd/yyyy"
        let textTo = dateFormatter1.string(from:sender.date)
        txtToDate.text = textTo
    }
    
    @IBAction func applyAction(_ sender: Any) {
        
        print(calculateStartAndEndDate())
        var dateDictionary = [String:AnyObject]()
        dateDictionary["startTime"] = strFromDate as AnyObject//calculateStartAndEndDate().0 as AnyObject
        dateDictionary["endTime"] = strToDate as AnyObject//calculateStartAndEndDate().1 as AnyObject
        
        WebSocketManager.sharedInstance.tradeHistoryRequest(accountId: userAccountId, startDate: dateDictionary["startTime"] as! String, endDate: dateDictionary["endTime"] as! String)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func calculateStartAndEndDate()-> (String,String){
        
        //For Start Date
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone //OR NSTimeZone.localTimeZone()
        let  dateAtMidnight = calendar.startOfDay(for: Date())
        
        //For End Date
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let dateAtEnd = calendar.date(byAdding: components , to: dateAtMidnight)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        let dateMidString = dateFormatter.string(from: dateAtMidnight)
        let dateEndString = dateFormatter.string(from: dateAtEnd!)
        
        return (dateMidString,dateEndString)
        
    }

    

}
extension HistoryAdd:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArr.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HistoryCell
        cell.selectionStyle = .none
        
        cell.lblOrderName.text = historyArr[indexPath.row]["Symbol"] as? String ?? ""
        cell.lblOrderDate.text = historyArr[indexPath.row]["TradeDate"] as? String ?? "N/A"
        if let priceValue = historyArr[indexPath.row]["Gross Proceed"] as? Double{
            cell.lblOrderStatus.text = String(describing : priceValue)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(orderDetailVC(), animated: true)
    }
    
}

extension HistoryAdd {
    
   @objc func textFieldDidChange(_ textField : UITextField){
       if txtFromDate.text == ""   ||   txtToDate.text == ""  {
        btnApply.isUserInteractionEnabled = false
        btnApply.alpha = 0.5
        
       }
    
       else{
        btnApply.isUserInteractionEnabled = true
        btnApply.alpha = 1.0
        
       }
    
    }
    
}

