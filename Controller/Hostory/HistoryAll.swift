//
//  HistoryAll.swift
//  TradeZero
//
//  Created by Amit Verma on 12/5/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class HistoryAll: UIViewController {

    var parentNavigationController : UINavigationController?
    //Class HistoryAll is using in OrderHistory
    @IBOutlet weak var tblAll: UITableView!
    var orderEventArr = Array<Any>()
    var orderHistory = [OrderHistoryModel]()
    var filterItems = [OrderHistoryModel]()
    var isFilter: Bool = false
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(calculateStartAndEndDate())
        
        
        
        
      
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderEventData(_:)), name: NSNotification.Name(rawValue: SocketConstants.orderEventHistoryResponse), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderHistory(_:)), name: NSNotification.Name(rawValue: "OrderHistoryFilter"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(true)
        
        var dateDictionary = [String:AnyObject]()
        dateDictionary["startTime"] = (calculateStartAndEndDate().0)*1000 as AnyObject
        dateDictionary["endTime"] = (calculateStartAndEndDate().1)*1000 as AnyObject
        WebSocketManager.sharedInstance.orderEventHistoryRequest(dateDic: dateDictionary)
    }
    
    @objc func orderEventData(_ notification : NSNotification){
        
        guard let orderArr = (notification.userInfo!["data"] as! KeyValue)["events"] as? [KeyValue],orderArr.count > 0 else{
            
            let banner = StatusBarNotificationBanner(title: "No Record found!", style: .danger)
            banner.show()
           // UIAlertController.show(self, "Message", "No Record found!")
            return
        }
        orderEventArr = orderArr
        
        for i in 0 ..< orderEventArr.count {
            
            if let  dataDic = (orderEventArr[i] as! KeyValue)["data"] as? KeyValue{
                orderHistory.append(OrderHistoryModel.init(dataDic)!)
            }
       }
        
        filterItems = orderHistory.filter { $0.orderStatus == "Fill" }
        print(filterItems)
        
        self.tblAll.reloadData()
        
    }
    
    @objc func orderHistory(_ notification : NSNotification){
        
        
        
        if let filtertype = notification.userInfo!["type"] as? String{
            
            if filtertype == "All"{
                isFilter = false
            }else{
                isFilter = true
                filterItems = orderHistory.filter { $0.orderStatus == filtertype}
                print(filterItems)
            }
            self.tblAll.reloadData()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func calculateStartAndEndDate()-> (Int,Int){
        
        //For Start Date
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone //OR NSTimeZone.localTimeZone()
        let  dateAtMidnight = calendar.startOfDay(for: Date())
        
        //For End Date
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let dateAtEnd = calendar.date(byAdding: components , to: dateAtMidnight)
        
        return (Int(dateAtMidnight.timeIntervalSince1970),Int(dateAtEnd!.timeIntervalSince1970))
        
    }
    
    
    
}

extension HistoryAll:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFilter == false{
            return orderEventArr.count;
        }else{
            return filterItems.count;

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HistoryCell
        cell.selectionStyle = .none
        
        if isFilter == false{
            cell.lblOrderName.text = orderHistory[indexPath.row].symbolName
            cell.lblOrderDate.text = orderHistory[indexPath.row].date
            cell.lblOrderStatus.text = orderHistory[indexPath.row].orderStatus
        }else{
            cell.lblOrderName.text = filterItems[indexPath.row].symbolName
            cell.lblOrderDate.text = filterItems[indexPath.row].date
            cell.lblOrderStatus.text = filterItems[indexPath.row].orderStatus
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*
         var orderType = String()
         var timeStamp = String()
         var orderStatus = String()
         var quantity = String()
 */
        
        let storyboard = UIStoryboard(name: AppKey.history, bundle:nil)
        if let orderVC = storyboard .instantiateViewController(withIdentifier: OrderDetailVC.identifier()) as? OrderDetailVC {
            if isFilter == false{
                orderVC.orderId = String(describing : orderHistory[indexPath.row].orderId!)
                orderVC.orderType = orderHistory[indexPath.row].orderType!
                orderVC.timeStamp = orderHistory[indexPath.row].date!
                orderVC.orderStatus = orderHistory[indexPath.row].orderStatus!
                orderVC.quantity = String(describing : orderHistory[indexPath.row].quantity!)

                
            }else{
                orderVC.orderId = String(describing : filterItems[indexPath.row].orderId!)
                orderVC.orderType = filterItems[indexPath.row].orderType!
                orderVC.timeStamp = filterItems[indexPath.row].date!
                orderVC.orderStatus = filterItems[indexPath.row].orderStatus!
                orderVC.quantity = String(describing :filterItems[indexPath.row].quantity!)
            }
            parentNavigationController?.pushViewController(orderVC, animated: true)
        }
    }
    
}

