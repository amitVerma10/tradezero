//
//  FirstVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/23/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class FirstVC: BaseVC {
    
    // pagecontroller Outlet
    @IBOutlet weak var pageController: UIPageControl!
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "FirstVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Button Actions
    @IBAction func btnSignInAction(_ sender: Any) {
        self.navigationController?.pushViewController(loginVC(), animated: true)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        self.navigationController?.pushViewController(signUpNameVC(), animated: true)
    }
    

}

extension FirstVC{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width
        let fractionalPage = Float((scrollView.contentOffset.x / pageWidth))
        pageController.currentPage = Int(fractionalPage)
       
    }

}
