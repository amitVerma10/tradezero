//
//  SignUpPublicCompanyVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/19/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpPublicCompanyVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpPublicCompanyVC"
    }
    
    @IBOutlet weak var lblFedral: UILabel!
    let arr = ["NO","YES"]
    let arrValue = [false,true]

    override func viewDidLoad() {
        super.viewDidLoad()

        if signUpDictionary[AppKey.countryCode] as! String == "US"{
            lblFedral.isHidden = false
        }else{
            lblFedral.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func signUpUpdateAction(){
        
        //http://tradezero.co:2701/api/SignProcess/Emplyeestatus?AccountId=9819
        guard let acoountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let Url = "\(API.signUpWithEmpStatus)\(String(describing: acoountId))"
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUpWithSSNAndStep3(signUpDictionary,Url) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                //KAppDelegate.goToRootViewAfterLogin()
                //self.navigationController?.pushViewController(self.signUpVerifyBankAccount(), animated: true)
                self.navigationController?.pushViewController(self.signUpFinancialVC(), animated: true)
                
            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
            
        }
        
        
    }

  

}

extension SignUpPublicCompanyVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            cell.lblCountryName.text = arr[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        signUpDictionary[AppKey.isShareHolderCompanyName] = arrValue[indexPath.row]//set shareHolderCompany status id
         self.signUpUpdateAction()
    }
    
    
}
