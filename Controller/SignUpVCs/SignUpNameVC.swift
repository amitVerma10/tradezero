//
//  NameVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign up screen textfield
fileprivate enum SignUpField: Int {
    case firstName = 100
    case lastName
}

class SignUpNameVC: BaseVC {

    var keyboardHeight : CGFloat = 0
    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpNameVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nextButton initian frame
        setnextButtonFrame()
        
        //self.txtFirstName.becomeFirstResponder()
        self.txtFirstName.autocorrectionType = UITextAutocorrectionType.no
        self.txtLastName.autocorrectionType = UITextAutocorrectionType.no
        self.txtFirstName.autocapitalizationType = .sentences;
        self.txtLastName.autocapitalizationType = .sentences;
        

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtFirstName.addTarget(self, action: #selector(SignUpNameVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtLastName.addTarget(self, action: #selector(SignUpNameVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    
    //MARK:  buttons Action
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        txtFirstName.resignFirstResponder()//dismiss Keyboard
        txtLastName.resignFirstResponder()//dismiss Keyboard
        
        signUpDictionary[AppKey.firstName] = txtFirstName.text
        signUpDictionary[AppKey.lastName] = txtLastName.text
        self.navigationController?.pushViewController(signUpDOBVC(), animated: true)

    }
    
    // MARK:  Private Methods
    private func _signUp() {
        guard let firstNameField = txtFirstName , _isValidRequest(field: .firstName, textField: txtFirstName) else {
            return
        }
        guard let lastNameField = txtLastName , _isValidRequest(field: .lastName, textField: txtLastName) else {
            return
        }
        signUpToEmailPush(firstNameField, lastNameField)
    }
    
    
    private func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .firstName:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterFirstName)
                return false
            }
            
            return true
            
        case .lastName:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterLastName)
                return false
            }
            return true
        }
    }
    
    // MARK:  API Method
    private func signUpToEmailPush(_ firstNameField: UITextField, _ lastNameField: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        //self.navigationController?.pushViewController(signUpEmailVC(), animated: true)
        
    }
    
    

}

extension SignUpNameVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        if txtFirstName.text?.length() == 0 || txtLastName.text?.length() == 0{
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1
        }
    }
}
