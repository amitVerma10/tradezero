//
//  SignUpFinancialVC.swift
//  TradeZero
//
//  Created by Amit Verma on 4/27/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpFinancialVC: BaseVC {
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpFinancialVC"
    }
    
    @IBOutlet weak var btnFinacial1: UIButton!
    @IBOutlet weak var btnFinancial2: UIButton!
    @IBOutlet weak var btnFinancial3: UIButton!
    @IBOutlet weak var btnFinancila4: UIButton!
    
    @IBOutlet weak var btnLNW1: UIButton!
    @IBOutlet weak var btnLNW2: UIButton!
    @IBOutlet weak var btnLNW3: UIButton!
    @IBOutlet weak var btnLNW4: UIButton!
    
    
    @IBOutlet weak var nextBtn: CustomButton!
    
    
    

    let annualIncomeArr:[String] = ["Under US $25,000","US $25,000 - US $80,000", "US $80,000 - US $150,000", "Over $150,000"]
    let liquedNetWorthArr:[String] = ["Under US $50,000","US $50,000 - US $100,000", "US $100,000 - US $500,000", "Over $500,000"]
    let fundingsArr:[String] = ["Income","Pension or retirement saving","Funds from another account","Gift","Sale of business or property","Insurance payout", "Inheritance" ,"Social Security Benifits", "Home Equity line of Credit/Reverse Morlgage","Other"]
    
    var selectedFundingArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nextBtn.alpha = 0.5
        nextBtn.isUserInteractionEnabled = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Annual Income action
    @IBAction func finantialAction(_ sender: UIButton) {
        
        btnFinacial1.isSelected = false
        btnFinancial2.isSelected = false
        btnFinancial3.isSelected = false
        btnFinancila4.isSelected = false
        
        
        if sender.isSelected == true{
            sender.isSelected = false
        }else{
            sender.isSelected = true
            signUpDictionary[AppKey.annualIncome] = annualIncomeArr[sender.tag]
            
            self.checkNextButtonIntraction()
        }
    }
    
    //MARK:- Liquid Net Worth action
    
    @IBAction func liquidNetWorthAction(_ sender: UIButton) {
        btnLNW1.isSelected = false
        btnLNW2.isSelected = false
        btnLNW3.isSelected = false
        btnLNW4.isSelected = false
        
        if sender.isSelected == true{
            sender.isSelected = false
        }else{
            sender.isSelected = true
            signUpDictionary[AppKey.liquidNetWorth] = liquedNetWorthArr[sender.tag]
            
            self.checkNextButtonIntraction()
            
            
        }
        
        
    }
    
    
    //MARK:- Funding action
    
    @IBAction func fundingAction(_ sender: UIButton) {
        if sender.isSelected == true{
            sender.isSelected = false
            
            let index = selectedFundingArr.enumerated().filter {
                $0.element == (fundingsArr[sender.tag])
                }.map{$0.offset}
            
            print(index)
            selectedFundingArr.remove(at: index[0])
            
            
        }else{
            sender.isSelected = true
            selectedFundingArr.append(fundingsArr[sender.tag])
            
        }
        
        
        self.checkNextButtonIntraction()
    }
    
    private func checkNextButtonIntraction(){
        nextBtn.alpha = 0.5
        nextBtn.isUserInteractionEnabled = false
        if  signUpDictionary[AppKey.annualIncome] != nil && signUpDictionary[AppKey.liquidNetWorth] != nil && selectedFundingArr.count>0{
            nextBtn.alpha = 1
            nextBtn.isUserInteractionEnabled = true
        }
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        
        let fundString = selectedFundingArr.joined(separator: ",")
        signUpDictionary[AppKey.fundingOption] = fundString
        self.navigationController?.pushViewController(signUpGoalVC(), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  

}
