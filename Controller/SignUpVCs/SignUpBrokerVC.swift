//
//  SignUpBrokerVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/19/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpBrokerVC:BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpBrokerVC"
    }
    
    @IBOutlet weak var lblFedral: UILabel!
    let arr = ["NO","YES"]
    let arrValue = [false,true]

    override func viewDidLoad() {
        super.viewDidLoad()

        if signUpDictionary[AppKey.countryCode] as! String == "US"{
            lblFedral.isHidden = false
        }else{
            lblFedral.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SignUpBrokerVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            cell.lblCountryName.text = arr[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        signUpDictionary[AppKey.isBrokerDealer] = arrValue[indexPath.row]//set brokerage status id
        self.navigationController?.pushViewController(self.signUpPublicCompanyVC(), animated: true)
        
    }
    
    
}
