//
//  SignUpInvestmentExpVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/22/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpInvestmentExpVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpInvestmentExpVC"
    }
    
    @IBOutlet weak var lblFedral: UILabel!
    @IBOutlet weak var tblInvestExp: UITableView!
    let investmentArr = ["NONE","1-5 years","5-10 years","10+ years"]
    let investmentArrValue = ["0","1-5","5-10","10+"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if signUpDictionary[AppKey.countryCode] as! String == "US"{
            lblFedral.isHidden = false
        }else{
            lblFedral.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SignUpInvestmentExpVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return investmentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            cell.lblCountryName.text = investmentArr[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        signUpDictionary[AppKey.yearMutalFunds] = investmentArrValue[indexPath.row]//set employement status id
        self.navigationController?.pushViewController(self.signUpEmployementVC(), animated: true)
        
    }
    
    
    
}
