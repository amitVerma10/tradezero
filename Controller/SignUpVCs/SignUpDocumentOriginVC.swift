//
//  SignUpDocumentOriginVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/1/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpDocumentOriginVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpDocumentOriginVC"
    }
    
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblFedral: UILabel!
    @IBOutlet weak var tblCitizen: UITableView!
    private var countryList = NSMutableArray()
    var filteredNFLTeams = [Any]()
    
    let searchController = UISearchController(searchResultsController: nil)


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if signUpDictionary[AppKey.countryCode] as? String == "US"{
                  lblFedral.isHidden = false
            }
        else{
            lblFedral.isHidden = true
        }
        
        
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        tblCitizen.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        tblCitizen.tableFooterView = UIView()
        //check condition for user is from USA
        if  signUpDictionary[AppKey.countryCode] as? String == "US" && signUpDictionary[AppKey.documentTypeId] as? Int == 1   {
            
            lblDetail.text = "Before scanning your drivers license, please select the state of issuance."
            
            AppInstance.applicationInstance.countryArray = BundleDataClass.getStateListOfUSAList()
            for i in 0..<AppInstance.applicationInstance.countryArray.count{
                
                countryList.add(AppInstance.applicationInstance.countryArray[i] as! NSDictionary )
                
            }
            
        }else{
            
            if signUpDictionary[AppKey.documentTypeId] as? Int == 1{
                lblDetail.text = "Before scaning your driver licence, Please select it's country of origin."
                
            }else if signUpDictionary[AppKey.documentTypeId] as? Int == 2{
                lblDetail.text = "Before scaning your passport, Please select it's country of origin."
            }
            else {
                lblDetail.text = "Before scaning your utility bill, Please select it's country of origin."
            }
            
            AppInstance.applicationInstance.countryArray = BundleDataClass.getCountryList()
            for i in 0..<AppInstance.applicationInstance.countryArray.count{
                //countryList.append((((AppInstance.applicationInstance.countryArray[i] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!)
                countryList.add(AppInstance.applicationInstance.countryArray[i] as! NSDictionary )
                
            }
            
        }
        
        tblCitizen.reloadData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    


}

extension SignUpDocumentOriginVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return filteredNFLTeams.count
        }
        return countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            
            if isFiltering() {
                cell.lblCountryName.text = ((filteredNFLTeams[indexPath.row] as! NSDictionary).value(forKey: "name") as? String)!.uppercased()
            } else {
                cell.lblCountryName.text = ((countryList[indexPath.row] as! NSDictionary).value(forKey: "name") as? String)!.uppercased()
            }
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let countryName : String
        var countryCode : String
        
        if isFiltering() {
            countryName = (((filteredNFLTeams[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!
            if  signUpDictionary[AppKey.countryCode] as? String == "US" && signUpDictionary[AppKey.documentTypeId] as? Int == 1 {
                
                countryCode = (((filteredNFLTeams[indexPath.row] as? NSDictionary)?.value(forKey: "abbreviation") as? String)?.uppercased())!
                
            }else{
                countryCode = (((filteredNFLTeams[indexPath.row] as? NSDictionary)?.value(forKey: "dial_code") as? String)?.uppercased())!
            }
            
        }else{
            countryName = (((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!
            
            if  signUpDictionary[AppKey.countryCode] as? String == "US" && signUpDictionary[AppKey.documentTypeId] as? Int == 1 {
                countryCode = (((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "abbreviation") as? String)?.uppercased())!
            }else{
                countryCode = (((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "dial_code") as? String)?.uppercased())!
            }
            
        }
        countryCode = countryCode.replacingOccurrences(of: "+", with: "", options: .literal, range: nil)
        
        let alert = UIAlertController(title: AppTitle.appTital,
                                      message: "You have selected \(String(describing: countryName)), is this correct?",
            preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .cancel, handler: nil)
        let okAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
            //signUpDictionary[AppKey.countryIdForDoc] = countryCode upto date dddfdfdg
            signUpDictionary[AppKey.stateIdForDoc] = countryCode
            self.navigationController?.pushViewController(self.cameraViewController(), animated: true)
            //self.signUpUpdateAction()
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
        
    }
    
}

extension SignUpDocumentOriginVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text, !searchText.isEmpty
        {
            filteredNFLTeams = countryList.filtered(using: NSPredicate(format: "name contains[cd] %@", searchText))
        }
        else {
            filteredNFLTeams = countryList as! [Any]
        }
        tblCitizen.reloadData()
    }
    
    func willDismissSearchController(_ searchController: UISearchController){
        
    }
    
    
}

