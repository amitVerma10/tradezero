 
 //
//  SignUpDocumentImageVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/1/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpDocumentImageVC: BaseVC ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpDocumentImageVC"
    }

    var image: UIImage?
    @IBOutlet weak var imageView: UIImageView!
    var dicImage = [String : Data]()
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var popover:UIPopoverController?=nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
        if let data:Data = UIImageJPEGRepresentation(image!, 1.0) {
            dicImage["idproof"] = data
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnTryAgainAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        /*let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        // Add the actions
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        */
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(picker!, animated: true, completion: nil)
        }
      
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker .dismiss(animated: true, completion: nil)
        imageView.image=info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        
        self.signUpDocumentUpload()
        
    }
    
    fileprivate func signUpDocumentUpload(){
        
        //http://tradezero.co:2701/api/SignProcess/IdentificationProcessNew?accountId=10754&Documenttypeid=1&countryid=1&stateid=1
        guard let acoountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let Url = "\(API.signUpUploadDocs)\(String(describing: acoountId))&Documenttypeid=\(String(describing: signUpDictionary[AppKey.documentTypeId]!))&countryid=\(String(describing:signUpDictionary[AppKey.countryCodeID]!))&stateAbb=\(String(describing:signUpDictionary[AppKey.stateIdForDoc] ?? ""))"
        //let Url = "http://tradezero.co:2701/api/SignProcess/IdentificationProcessNew?accountId=10754&Documenttypeid=1&countryid=US&stateid=AL"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUpDocumentUpload(Url, [dicImage]) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                signUpDictionary.removeAll()
                 self.navigationController?.pushViewController(self.signUpThankyouVC(), animated: true)
               /* if let status1 = response!["Status1"] as? String{
                    signUpDictionary.removeAll()
                    if status1 == "Success"{
                        
                        self.navigationController?.pushViewController(self.signUpThankyouVC(), animated: true)
                        
                    }else{
                        self.navigationController?.pushViewController(self.signUpThankyouVC(), animated: true)
                        
                       /* let alert = UIAlertController(title: KAlertTitle,
                                                      message: status1,
                                                      preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                            
                            self.navigationController?.pushViewController(self.signUpThankyouVC(), animated: true)
                            
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true)*/
                    }
                }*/
                
                
                
                
                
                
                /**/
                
                
                //self.navigationController?.pushViewController(self.firstVC(), animated: true)
                
            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
        }
        
        
    }
    
    private func gotoSignInView(){
        
        let alert = UIAlertController(title: KAlertTitle,
                                      message: "Please check you Email Id for login.",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
            KAppDelegate.lououtAction()
        }
        alert.addAction(okAction)
        self.present(alert, animated: true)
    }
    
    
    

}
