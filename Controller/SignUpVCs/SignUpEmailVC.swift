//
//  SignUpEmailVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign Up screen textfield
fileprivate enum SignUpField: Int {
    case email = 100
}

class SignUpEmailVC: BaseVC {
    var keyboardHeight : CGFloat = 0
    
    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    @IBOutlet weak var txtEmail: UITextField!


    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpEmailVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nextButton initian frame
        setnextButtonFrame()
        
        //self.txtEmail.becomeFirstResponder()
        self.txtEmail.autocorrectionType = UITextAutocorrectionType.no
        self.txtEmail.autocapitalizationType = .none

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtEmail.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }

    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    
    //MARK:  buttons Action
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        signUpDictionary[AppKey.email] = txtEmail.text
        self._signUpAPI(txtEmail.text!)
        
    }
    
    // MARK:  API Methods
    private func _signUpAPI(_ email: String) {
        //http://tradezero.co:2701/api/SignProcess/checkEmailAddress?emailAddress=anandgali4521789@gmail.com

        
        let url  = "\(API.signUp)/checkEmailAddress?emailAddress=\(email)"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                self.txtEmail.resignFirstResponder()//dismiss Keyboard

                self.navigationController?.pushViewController(self.signUpPasswordVC(), animated: true)
                ActivityIndicator.shared.hide()
            default:
                
                guard let statusValue  = response!["status"] as? Int,statusValue == 1 else{
                    self.errorHandling(response, status, error)
                    return
                }
                
                 self.txtEmail.resignFirstResponder()//dismiss Keyboard
                ActivityIndicator.shared.hide()
                let actionSheetController: UIAlertController = UIAlertController(title: AppTitle.appTital, message: "That email is already associated with an existing account. Click Cancel to login or Continue to create a new account.", preferredStyle: .alert)
                
                let OkAction: UIAlertAction = UIAlertAction(title: "Continue", style: .destructive) { action -> Void in

                    self.navigationController?.pushViewController(self.signUpPasswordVC(), animated: true)

                }
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                    //Just dismiss the action sheet
                    KAppDelegate.lououtAction()
                }
                actionSheetController.addAction(OkAction)
                actionSheetController.addAction(cancelAction)
                self.present(actionSheetController, animated: true, completion: nil)
                
                
            }
        }
    }
    
    
    
    
    
    // MARK:  Private Methods
    private func _signUp() {
        guard let emailField = txtEmail , _isValidRequest(field: .email, textField: txtEmail) else {
            return
        }
        signUpToPasswordPush(emailField)
    }
    
    
    private func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .email:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterEmail)
                return false
            }
            if !(textField.text?.isValidEmail())! {
                UIAlertController.show(self, KAlertTitle, KEnterValidEmail)
                return false
            }
            return true
            
    }
 }
    // MARK:  API Method
    private func signUpToPasswordPush(_ email: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        self.navigationController?.pushViewController(signUpPasswordVC(), animated: true)
        
    }
    
   
}

extension SignUpEmailVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        if (textField.text?.isValidEmail())! {
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }else{
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }
    }
}

