//
//  SignUpSSNVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign in screen textfield
fileprivate enum SignUpField: Int {
    case ssn = 100
}

class SignUpSSNVC: BaseVC {
    
    var keyboardHeight : CGFloat = 0
    
    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    
    @IBOutlet weak var txtSSN: UITextField!
    

    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpSSNVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nextButton initian frame
        setnextButtonFrame()
        
        //self.txtSSN.becomeFirstResponder()
        self.txtSSN.autocorrectionType = UITextAutocorrectionType.no

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtSSN.addTarget(self, action: #selector(SignUpSSNVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }
    
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:  Button Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        signUpDictionary[AppKey.SSN] = txtSSN.text
        _signUpAPI(txtSSN.text!)
    }
    
    
    // MARK:  API Methods
    private func _signUpAPI(_ ssnField: String) {
        
        guard let accountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        
        //http://tradezero.co:2701/api/SignProcess/SSNumberVerify?accountId=12732&ssnno=6986541023
        let url  = "\(API.signUp)/SSNumberVerify?accountId=\(String(describing: accountId))&ssnno=\(ssnField)"
       // let url  = "\(API.signUp)/SSNumberVerify?accountId=12732&ssnno=\(ssnField)"
        print(url)
        
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            print(status)
            ActivityIndicator.shared.hide()
            switch status {
            case .success:
                
                guard let statusValue  = response!["status"] as? Int,statusValue == 0 else{
                    self.errorHandling(response, status, error)
                    return
                }
                self.txtSSN.resignFirstResponder()//dismiss Keyboard
                if let status1 = response!["Status1"] as? String{
                    if status1 == "Success"{
                        
                        self.navigationController?.pushViewController(self.signUpInvestmentExpVC(), animated: true)
                    }else{
                        self.navigationController?.pushViewController(self.signUpInvestmentExpVC(), animated: true)
                        /*let alert = UIAlertController(title: KAlertTitle,
                                                      message: status1,
                                                      preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                            
                            self.navigationController?.pushViewController(self.signUpInvestmentExpVC(), animated: true)
                        }
                        alert.addAction(okAction)
                        self.present(alert, animated: true)*/
                    }
                }
                
                
            default:
                
                self.errorHandling(response, status, error)
            }
        }
    }

}
extension SignUpSSNVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        
        if textField.text?.length() == 0 || textField.text?.length() < 11
        {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }
      
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        return checkSocialSecurityNumberFormat(string: string, str: str)
        
    }
    
   private func checkSocialSecurityNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.characters.count < 3{
            
            if str!.characters.count == 1{
                
                //txtSSN.text = ""
            }
            
        }else if str!.characters.count == 4{
            
            txtSSN.text = txtSSN.text! + "-"
            
        }else if str!.characters.count == 7{
            
            txtSSN.text = txtSSN.text! + "-"
            
        }else if str!.characters.count > 11{
            
            return false
        }
        
        return true
    }
    
}
