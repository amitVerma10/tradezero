//
//  SignUpVerifyBankAccount.swift
//  TradeZero
//
//  Created by Amit Verma on 1/9/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit
import LinkKit

class SignUpVerifyBankAccount: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpVerifyBankAccount"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func skipAction(_ sender: Any) {
        self.navigationController?.pushViewController(self.signUpTermAndConVC(), animated: true)
    }
    
    
    
    @IBAction func nextAction(_ sender: Any) {
        
        //Plaid integration
        
        
        // With custom configuration
        
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        // With shared configuration from Info.plist
         let linkViewDelegate = self
         let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
         if (UI_USER_INTERFACE_IDIOM() == .pad) {
         linkViewController.modalPresentationStyle = .formSheet;
         }
         present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // Bank Account Authentication
    // MARK:  API Methods
    private func getAccessTokenAPI(_ publicToken: String,plaidAccountId: String) {
        
        
        //http://tradezero.co:2701/api/SignProcess/GetAccessToken?public_token=public-sandbox-e6665ab7-2fc4-4e08-85e6-ccb4e4ad2463
        //http://tradezero.co:2701/api/SignProcess/GetCustomerToken?public_token=public-sandbox-3cf817af-3458-4248-bd62-0f12fc18508e&plaidaccount_id=yLJXxJbdg1Upo6z4dnxZCxWnpK7RpockXgep9&Accountid=10609'
        
        
        guard let accountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        
        signUpDictionary[AppKey.plaidAccountId] = accountId// save plaid account id
        
        let url  = "\(API.signUp)/GetCustomerToken?public_token=\(publicToken)&plaidaccount_id=\(plaidAccountId)&Accountid=\(accountId)"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                
                guard let accessCustomerId = response?[AppKey.plaidCustomerId] else{
                    return
                }
                signUpDictionary[AppKey.plaidCustomerId] = accessCustomerId
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                 self.navigationController?.pushViewController(self.signUpTermAndConVC(), animated: true)
                
                //self.getBankToken(plaidAccessToken: accessToken as! String, plaidAccountId: accountId)
                
            default:
                self.errorHandling(response, status, error)
            }
        }
    }
    
   /* private func getBankToken(plaidAccessToken: String,plaidAccountId:String){
        
        
        //http://tradezero.co:2701/api/SignProcess/GetBankToken?Accountid=9779&Access_token=access-sandbox-d8feb1c8-8dc4-4e6b-8735-6eeda21f66f3&account_id=avAKMv6EZKhlRAnw9Gd9HogwLzEwDzfp3lrjp
        
        guard let accountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let url  = "\(API.signUp)/GetBankToken?Accountid=\(accountId)&Access_token=\(plaidAccessToken)&account_id=\(plaidAccountId)"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                ActivityIndicator.shared.hide()//StripeBankToken
                guard let stripeBankToken = response?[AppKey.StripeBankToken] else{
                    return
                }
                signUpDictionary[AppKey.StripeBankToken] = stripeBankToken
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                self.generateAccessToken()
                
            default:
                self.errorHandling(response, status, error)
            }
        }
    }
    
    
    private  func generateAccessToken(){
        
        ActivityIndicator.shared.show(self.view)
         BusinessLayer.accessToken(userAccountId, "718testing") { (response, status, error) in
            switch status {
                
             case .success:
            print(response!)
            let tokenStr = response!["token"] as! String
            ActivityIndicator.shared.show(self.view)
            WebSocketManager.sharedInstance.connect(with: URL(string:API.WebsocketUrl+tokenStr)!)
            //ActivityIndicator.shared.hide()
            
            DispatchQueue.main.async(execute: {
                self.navigationController?.pushViewController(self.signUpTermAndConVC(), animated: true)
            })
        default:
            self.errorHandling(response, status, error)
            
        }
      }
    
    }
   */

}


extension SignUpVerifyBankAccount : PLKPlaidLinkViewDelegate{
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        dismiss(animated: true) {
            // Handle success, e.g. by storing publicToken with your service
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            
            self.getAccessTokenAPI(publicToken, plaidAccountId: metadata!["account_id"] as! String)
            //self.handleSuccessWithToken(publicToken, metadata: metadata)
        }
    }
    
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        dismiss(animated: true) {
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                //self.handleError(error, metadata: metadata)
            }
            else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
                //self.handleExitWithMetadata(metadata)
            }
        }
    }
}

