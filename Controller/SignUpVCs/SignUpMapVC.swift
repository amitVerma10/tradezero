//
//  SignUpMapVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/14/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces

class SignUpMapVC: BaseVC  {
    var keyboardHeight : CGFloat = 0
    
    //Outlets
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var txtResidential: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblSubAddress: UILabel!
    
    //Variables
    let locationManager = LocationManager()
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpMapVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.txtResidential.autocorrectionType = UITextAutocorrectionType.no

        setnextButtonFrame()
        lblAddress.text = signUpDictionary[AppKey.address] as? String
        lblSubAddress.text = signUpDictionary[AppKey.addressSecondry] as? String
        
        self.mapView.delegate = self
        print(signUpDictionary)
        self.getLatLongFromPlaceID(placeID: signUpDictionary[AppKey.addressPlaceId] as! String)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
//        btnNext.isUserInteractionEnabled = false
//        btnNext.alpha = 0.5
        txtResidential.addTarget(self, action: #selector(SignUpMapVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
      private func setnextButtonFrame(){
        
         self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc private func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:  Button Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        signUpDictionary[AppKey.apartNumberName] = txtResidential.text ?? ""
        self.navigationController?.pushViewController(signUpResidentialDetailVC(), animated: true)
    }
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    //MARK: GETLATLONGFROMPLACEID
    private func getLatLongFromPlaceID(placeID: String) {
        
        ActivityIndicator.shared.show(self.view)
        
        GMSPlacesClient().lookUpPlaceID(placeID, callback: { (place: GMSPlace?, error: Error?) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            if let place = place {
                signUpDictionary[AppKey.addressLat] = place.coordinate.latitude
                signUpDictionary[AppKey.addressLong] = place.coordinate.longitude
                
                // Add below code to get address for touch coordinates.
                let geoCoder = CLGeocoder()
                let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                    
                    ActivityIndicator.shared.hide()
                    
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    
                    // Address dictionary
                    print(placeMark.addressDictionary as Any)
                    
                    // Location name
                    if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                        print(locationName)
                    }
                    // Street address
                    if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                        print(street)
                    }
                    // City
                    if let city = placeMark.addressDictionary!["City"] as? NSString {
                        print(city)
                    }
                    // Zip code
                    if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                        print(zip)
                    }
                    // Country
                    if let countryCode = placeMark.addressDictionary!["CountryCode"] as? NSString {
                        signUpDictionary[AppKey.countryCode] = countryCode
                        print(countryCode)
                    }
                    
                    // Country code
                    if let country = placeMark.addressDictionary!["Country"] as? NSString {
                        print(country)
                    }
                    
                    
                    //let formatedAddress:String = place.formattedAddress!
                    guard let formatedAddress = place.formattedAddress else{
                        return
                    }
                    let fullAddArr = formatedAddress.components(separatedBy: ",")
                    print(fullAddArr)
                    signUpDictionary[AppKey.streetAddress1] = formatedAddress
                    if fullAddArr.count >= 3{
                        signUpDictionary[AppKey.city] = fullAddArr[(fullAddArr.count)-3]
                    }
                    if fullAddArr.count >= 2{
                        signUpDictionary[AppKey.state] = fullAddArr[(fullAddArr.count)-2]
                        let addresArr = (fullAddArr[(fullAddArr.count)-2]).split(separator: " ")
                        if let zipcode = Int(addresArr[addresArr.count - 1]){
                            signUpDictionary[AppKey.zipcode] = String(zipcode)
                            signUpDictionary.removeValue(forKey: AppKey.state)
                            signUpDictionary[AppKey.state] = String(addresArr[addresArr.count - 2])
                        }
                    }
                    
                    signUpDictionary[AppKey.countryName] = fullAddArr[(fullAddArr.count)-1]
                    
                })
                
                
                self.setMapView()
              
                
                
            } else {
                print("No place details for \(placeID)")
            }
        })
        
        
    }
    
    private func getPostalCode(_ stateStr: String) -> String?{
        let AddArr = stateStr.components(separatedBy: " ")
        
        if AddArr.count == 2{
            return AddArr[1]
        }
        return ""
    }
    
    //set mapview according to coordinate
    private func setMapView(){
        let latitude:CLLocationDegrees = signUpDictionary[AppKey.addressLat] as? CLLocationDegrees ?? 0.000
        let longitude:CLLocationDegrees = signUpDictionary[AppKey.addressLong] as? CLLocationDegrees ?? 0.000
        
        let latDelta:CLLocationDegrees = 0.05
        let longDelta:CLLocationDegrees = 0.05
        
        let theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(pointLocation, theSpan)
        mapView.setRegion(region, animated: true)
        
        let pinLocation : CLLocationCoordinate2D =  CLLocationCoordinate2DMake(latitude, longitude)
        let objectAnnotation = MKPointAnnotation()
        objectAnnotation.coordinate = pinLocation
        objectAnnotation.title = signUpDictionary[AppKey.address] as? String
        self.mapView.addAnnotation(objectAnnotation)
    }
    

}
extension SignUpMapVC{
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        /*if textField.text?.length() == 0 {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1
        }*/
    }
    
}
extension SignUpMapVC : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKPointAnnotation {
            
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            
            pinAnnotationView.isDraggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            
            return pinAnnotationView
        }
        
        return nil
        
    }
    
}


