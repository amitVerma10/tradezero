//
//  SignUpVeriCodeVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign up screen textfield
fileprivate enum SignUpField: Int {
    case textFirst = 100
    case textSecond
    case textThird
    case textFourth
}

class SignUpVeriCodeVC: BaseVC {
    var keyboardHeight : CGFloat = 0
    
    @IBOutlet var countDownLabel: UILabel!
    
    var count = 60
    var timer = Timer()
    var isFirstTime = false //This will be used to make sure only one timer is created at a time.
    
    @IBOutlet weak var btnResendOTP: UIButton!
    
    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    
    @IBOutlet weak var txtCode1: UITextField!
    @IBOutlet weak var txtCode2: UITextField!
    @IBOutlet weak var txtCode3: UITextField!
    @IBOutlet weak var txtCode4: UITextField!

    @IBOutlet weak var lblDetail: UILabel!
    
    var mobileStr : String = "846-182-1546"
    
    var myString : String = "Please type the verification code sent to "
    var myMutableString = NSMutableAttributedString()
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpVeriCodeVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileStr = signUpDictionary[AppKey.phoneNumber] as? String ?? "1234567890"
        myString = "Please type the verification code sent to \(mobileStr)"
        
        
        let attrs1 = [NSAttributedStringKey.font : UIFont(name: "Raleway-medium", size: 13.0), NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let attrs2 = [NSAttributedStringKey.font : UIFont(name: "Raleway-Bold", size: 15.0), NSAttributedStringKey.foregroundColor : UIColor.white]
        
        let attributedString1 = NSMutableAttributedString(string:"Please type the verification code has been sent to ", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:mobileStr, attributes:attrs2)
        
        attributedString1.append(attributedString2)
        lblDetail.attributedText = attributedString1
        

    
        
        
        // nextButton initian frame
        setnextButtonFrame()
        
        self.txtCode1.becomeFirstResponder()
        self.txtCode1.autocorrectionType = UITextAutocorrectionType.no

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardDidHide, object: nil)
        
        
        
        txtCode1.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtCode2.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtCode3.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtCode4.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        
        print(signUpDictionary)
        
        //set timer for first Time
        isFirstTime = true
        self.applyTimer()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    //set timer for resend OTP
    func applyTimer(){
        //set timer for resend OTP
        btnResendOTP.isUserInteractionEnabled = false
        btnResendOTP.alpha = 0.5
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    //Update method for timer
    @objc func updateTimer() {
        
        if(count > 0){
            let minutes = String(count / 60)
            let seconds = String(count % 60)
            countDownLabel.text = minutes + ":" + seconds
            count = count - 1
        }else{
            timer.invalidate()
            btnResendOTP.isUserInteractionEnabled = true
            btnResendOTP.alpha = 1
            countDownLabel.text = "0:0"
            if isFirstTime != true{
                count = 180
            }else{
                isFirstTime = false
                count = 60
            }
        }
        
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
       
            self.buttonTopriviousPosition()
        
        
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        
        
        if textField == txtCode1{
            if txtCode1.text?.length() == 1{
                txtCode1.resignFirstResponder()
                txtCode2.becomeFirstResponder()
            }
            else if txtCode1.text?.length() == 0{
            }
        }
        if textField == txtCode2{
            if txtCode2.text?.length() == 1{
                txtCode2.resignFirstResponder()
                txtCode3.becomeFirstResponder()
            }
            else if txtCode2.text?.length() == 0{
                //txtCode1.becomeFirstResponder()
                //txtCode2.resignFirstResponder()
            }
        }
        if textField == txtCode3{
            if txtCode3.text?.length() == 1{
                txtCode3.resignFirstResponder()
                txtCode4.becomeFirstResponder()
            }
            else if txtCode3.text?.length() == 0{
                //txtCode2.becomeFirstResponder()
                //txtCode3.resignFirstResponder()
            }
        }
        if textField == txtCode4{
            if txtCode4.text?.length() == 1{
                
            }
            else if txtCode4.text?.length() == 0{
                //txtCode3.becomeFirstResponder()
            }
        }
        
        if txtCode1.text?.length() == 0 || txtCode2.text?.length() == 0 || txtCode3.text?.length() == 0 || txtCode4.text?.length() == 0 {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
            
        }
        
        
        
        //remove entry
        
        
        
        
        
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }
    
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

    // MARK:  Button Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        
        let otpStr = "\(String(describing: txtCode1.text!))\(String(describing: txtCode2.text!))\(String(describing: txtCode3.text!))\(String(describing: txtCode4.text!))"
        signUpDictionary[AppKey.codeOTP] = Int(otpStr)
        print(signUpDictionary)
        self._signUpAPI(signUpDictionary)
        
    }
    
    // MARK:  API Methods
    private func _signUpAPI(_ signUpDic: [String : Any]) {
        ActivityIndicator.shared.show(self.view)
        
        
        BusinessLayer.checkOtp(signUpDic){ (response, status, error) in
            ActivityIndicator.shared.hide()
            switch status {
            case .success:
                self.navigationController?.pushViewController(self.signUpResidentialVC(), animated: true)
            default:
                self.errorHandling(response, status, error)
            }
        }
    }
    
    
    @IBAction func resendOtpAction(_ sender: Any) {
    //http://tradezero.co:2701/api/SignProcess/PhoneNumberVerification?AccountId=9702&phoneNumber=+919642509886&type=
        
        let url  = "\(API.signUp)/PhoneNumberVerification?AccountId=\(String(describing: signUpDictionary[AppKey.accountId]!))&phoneNumber=\(String(describing: signUpDictionary[AppKey.phoneNumber]!))&type="
        print(url)
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            print(status)
            ActivityIndicator.shared.hide()
            switch status {
            case .success:
                if let requestId = response?["request_id"]{
                    signUpDictionary[AppKey.requestId] = requestId
                }
                signUpDictionary[AppKey.typeOTP] = ""
                
                
            default:
                self.applyTimer()
                self.errorHandling(response, status, error)
            }
        }
    
    }
    
    
    
    
    
    
    
    
    
    // MARK:  Private Methods
    private func _signUp() {
        guard let textField1 = txtCode1 , _isValidRequest(field: .textFirst, textField: txtCode1) else {
            return
        }
        guard let textField2 = txtCode1 , _isValidRequest(field: .textSecond, textField: txtCode2) else {
            return
        }
        guard let textField3 = txtCode1 , _isValidRequest(field: .textThird, textField: txtCode3) else {
            return
        }
        guard let textField4 = txtCode1 , _isValidRequest(field: .textFourth, textField: txtCode4) else {
            return
        }
        
        _signUpAPI(textField1,textField2,textField3,textField4)
    }
    
    
    private func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .textFirst:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterFirst)
                return false
            }
        case .textSecond:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterSecond)
                return false
            }
        case .textThird:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterThird)
                return false
            }
        case .textFourth:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterFourth)
                return false
            }
           
        }
         return true
    }
    
    // MARK:  API Method
    private func _signUpAPI(_ textField1: UITextField,_ textField2: UITextField,_ textField3: UITextField,_ textField4: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        self.navigationController?.pushViewController(signUpResidentialVC(), animated: true)
        //ActivityIndicator.shared.show(self.view)
        
    }
    
}
extension SignUpVeriCodeVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
    }
  
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtCode4 || textField == txtCode1 ||  textField == txtCode2 || textField == txtCode3{
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 1
        }
        
        
        return true
    }
    
    
}

