//
//  SignUpResidentialDetailVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class SignUpResidentialDetailVC: BaseVC {
     var keyboardHeight : CGFloat = 0

    
     @IBOutlet weak var btnNext: CustomButton!
     @IBOutlet weak var txtStreetAddress1: UITextField!
     @IBOutlet weak var txtStreetAddress2: UITextField!
     @IBOutlet weak var txtZipCode: UITextField!
    
    
    

    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpResidentialDetailVC"
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // nextButton initian frame
        setnextButtonFrame()
        self.navigationController?.isNavigationBarHidden = true
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 100.0
        
        //self.txtFirstName.becomeFirstResponder()
       
        
        self.txtStreetAddress2.autocorrectionType = UITextAutocorrectionType.no
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtStreetAddress1.addTarget(self, action: #selector(SignUpResidentialDetailVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtStreetAddress2.addTarget(self, action: #selector(SignUpResidentialDetailVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtZipCode.addTarget(self, action: #selector(SignUpResidentialDetailVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
       
        if let state = signUpDictionary[AppKey.state] as? String{
            
            txtStreetAddress2.text = state
            // condition for country search
            if let street1 = signUpDictionary[AppKey.address] as? String{
                txtStreetAddress1.text = street1
            }
        }
        if let zipcode = signUpDictionary[AppKey.zipcode] as? String{
            txtZipCode.text = zipcode
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc private func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame()
    {
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
       
        
           /* UIView.animate(withDuration: 0.1,
                           delay: 0.0,
                           options: UIViewAnimationOptions.curveEaseIn,
                           animations: { () -> Void in
                            
                            self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
            }, completion: { (finished) -> Void in
                
            })*/
       
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        /*UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })*/
        
        
    }
    
    
    
    
    // MARK:  Button Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        
        signUpDictionary[AppKey.streetAddress1] = txtStreetAddress1.text
        signUpDictionary[AppKey.streetAddress2] = txtStreetAddress2.text
        signUpDictionary[AppKey.zipcode] = txtZipCode.text
        
        self.txtZipCode.resignFirstResponder()//dismiss Keyboard

        self.navigationController?.pushViewController(self.signUpCountryCodeVC(), animated: true)
        
     //KAppDelegate.goToRootViewAfterLogin()
    }


}
extension SignUpResidentialDetailVC{
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        if textField.text?.length() == 0 {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }
        if txtZipCode.text?.length() > 6 || txtZipCode.text?.length() < 4{
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }
        else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1
        }
    }
}
