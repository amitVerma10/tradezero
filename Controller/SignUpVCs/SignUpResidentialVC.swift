//
//  SignUpResidentialVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import GooglePlaces


// enum for Sign in screen textfield
fileprivate enum SignUpField: Int {
    case residential = 100
}


class SignUpResidentialVC: BaseVC {
    var keyboardHeight : CGFloat = 0
    
    // Variables
    var autoCompleteTimer:Timer!
    fileprivate var user = AppUser()
    var searchText = String()
    var timerSearch :Timer?
    var arrPlace:[GMSAutocompletePrediction] = []
  
    
    //Outlets
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var txtResidential: UITextField!
    @IBOutlet weak var tblPlace: UITableView!
    

    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpResidentialVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true


        // nextButton initian frame
        setnextButtonFrame()

       self.txtResidential.autocorrectionType = UITextAutocorrectionType.no
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtResidential.addTarget(self, action: #selector(SignUpResidentialVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        

 
        
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }
    
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:  Button Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        self._signUp()
    }
    
    // MARK:  Private Methods
    private func _signUp() {
        guard let resiField = txtResidential , _isValidRequest(field: .residential, textField: txtResidential) else {
            return
        }
        
        _signUpAPI(resiField)
    }
    
    
    private func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .residential:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterResiAddress)
                return false
            }
            return true
        }
    }
    
    // MARK:  API Method
    private func _signUpAPI(_ ssnField: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        
        self.txtResidential.resignFirstResponder()//dismiss Keyboard

        self.navigationController?.pushViewController(signUpMapVC(), animated: true)
        //ActivityIndicator.shared.show(self.view)
        
    }
    
}
extension SignUpResidentialVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    @objc func textFieldDidChange(_ textField : UITextField){
        
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        
        searchText = textField.text!
        timerSearch = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.typingTimer), userInfo: nil, repeats: false)
        
    }
    
    @objc func typingTimer() -> Void {
        guard  searchText != ""  else{
            UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.tblPlace.frame = CGRect.init(x: self.tblPlace.frame.origin.x, y: self.tblPlace.frame.origin.y, width: self.tblPlace.frame.size.width, height: 0)
            }, completion: { (completed) in
            })
            return
        }
        _placeAutocomplete(searchText)
    }
    
    // MARK: - Google API
    fileprivate func _placeAutocomplete(_ searchstring: String) {
        
        print("SearchStr--\(searchstring)")
        guard !searchstring.isEmpty ||  searchstring != "" else {
            DispatchQueue.main.async {
            }
            return
        }
        
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        
        

        
        GMSPlacesClient().autocompleteQuery(searchstring, bounds: nil, filter: filter, callback: {
            (results, error) -> Void in
            //print("PlacesApi--\(error?.localizedDescription)")
            guard error == nil else {
                return
            }
                if let results = results {
                    if results.count > 0 {
                        
                        UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                            self.tblPlace.frame = CGRect.init(x: self.tblPlace.frame.origin.x, y: self.tblPlace.frame.origin.y, width: self.tblPlace.frame.size.width, height: 200)
                        }, completion: nil)
                        
                        self.arrPlace = results
                        self.tblPlace.reloadData()
                        print(results)
                    }else{
                        
                    }
                }else{
                    UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        self.tblPlace.frame = CGRect.init(x: self.tblPlace.frame.origin.x, y: self.tblPlace.frame.origin.y, width: self.tblPlace.frame.size.width, height: 0)
                    }, completion: nil)
            }
            
            
        })
    }

}

extension SignUpResidentialVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlace.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AddressCell
        cell.lblFirst.text = arrPlace[indexPath.row].attributedPrimaryText.string
        cell.lblSecond.text = arrPlace[indexPath.row].attributedSecondaryText?.string
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        signUpDictionary[AppKey.address] = arrPlace[indexPath.row].attributedPrimaryText.string
        signUpDictionary[AppKey.addressSecondry] = arrPlace[indexPath.row].attributedSecondaryText?.string ?? ""
        signUpDictionary[AppKey.addressPlaceId] = arrPlace[indexPath.row].placeID
        //UserPreferences.setUserDetails(signUpDictionary)
        
        
        self.txtResidential.text = arrPlace[indexPath.row].attributedFullText.string
        btnNext.isUserInteractionEnabled = true
        btnNext.alpha = 1.0
    }
    
    
}


