//
//  SignUpVeriIdentityVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/23/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class SignUpVeriIdentityVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpVeriIdentityVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        self.navigationController?.pushViewController(signUpSSNVC(), animated: true)
    }
    
}
