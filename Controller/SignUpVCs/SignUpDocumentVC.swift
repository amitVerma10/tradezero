//
//  SignUpDocumentVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/1/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpDocumentVC: BaseVC {
    
    @IBOutlet weak var lblFedral: UILabel!
    var docuArr = ["Driving License","Passport","Utility Bill"]
    var docuIdArr = [1,2,3]

    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpDocumentVC"
    }

    @IBOutlet weak var tblDocument: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if signUpDictionary[AppKey.countryCode] as! String == "US"{
             docuArr = ["Driving License","Passport"]
             docuIdArr = [1,2]
            lblFedral.isHidden = false
        }
       else{
            lblFedral.isHidden = true
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension SignUpDocumentVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            cell.lblCountryName.text = docuArr[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        signUpDictionary[AppKey.documentTypeId] = docuIdArr[indexPath.row]
        self.navigationController?.pushViewController(signUpDocumentOriginVC(), animated: true)
        
    }
    
    
}
