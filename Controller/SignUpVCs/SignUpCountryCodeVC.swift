//
//  SignUpCountryCodeVC.swift
//  TradeZero
//
//  Created by Amit Verma on 12/13/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class SignUpCountryCodeVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpCountryCodeVC"
    }
    
    //private var countryList = [String]()
    
    private var countryList = NSMutableArray()
    private var selectcountryArr = [Any]()
    //var dicti = NSArray()
    

    @IBOutlet weak var tblCitizen: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredNFLTeams = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        tblCitizen.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        tblCitizen.tableFooterView = UIView()
        
        
        print(AppInstance.applicationInstance.countryArray)
        
       
        AppInstance.applicationInstance.countryArray = BundleDataClass.getCountryList()
        for i in 0..<AppInstance.applicationInstance.countryArray.count{
            //countryList.append((((AppInstance.applicationInstance.countryArray[i] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!)
            countryList.add(AppInstance.applicationInstance.countryArray[i] as! NSDictionary )
            
        }
        tblCitizen.reloadData()
        
        // filter for select country from place search API
        
        //selectcountryArr = countryList.filtered(using: NSPredicate(format: "code contains[cd] %@", (signUpDictionary[AppKey.countryCode] as! String).trimmingCharacters(in: .whitespaces)))

        // Do any additional setup after loading the view.
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func signUpUpdateAction(){
        
        //http://tradezero.co:2701/api/SignProcess?AccountId=9698
        //http://tradezero.co:2701/api/SignProcess/residentialAddress?AccountId=9698
        guard let acoountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let Url = "\(API.signUpWithAdderss)\(String(describing: acoountId))"
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUpWithResidential(signUpDictionary,Url) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                //KAppDelegate.goToRootViewAfterLogin()
                if signUpDictionary[AppKey.countryCode] as! String == "CA" || signUpDictionary[AppKey.countryCode] as! String == "US"{
                    self.navigationController?.pushViewController(self.signUpVeriIdentityVC(), animated: true)
                }else{
                    self.navigationController?.pushViewController(self.signUpInvestmentExpVC(), animated: true)
                }
                
                
            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
            
        }
        
        
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        let countryName : String
        var countryCode : String
        var countryCodeId : String
        if selectcountryArr.count > 0{
            countryName = (((selectcountryArr[0] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!
            countryCode = (((selectcountryArr[0] as? NSDictionary)?.value(forKey: "code") as? String)?.uppercased())!
            countryCodeId = (((selectcountryArr[0] as? NSDictionary)?.value(forKey: "dial_code") as? String)?.uppercased())!
            
            
            countryCode = countryCode.replacingOccurrences(of: "+", with: "", options: .literal, range: nil)
            countryCodeId = countryCodeId.replacingOccurrences(of: "+", with: "", options: .literal, range: nil)
            countryCodeId = countryCodeId.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            
            let alert = UIAlertController(title: AppTitle.appTital,
                                          message: "You have selected \(String(describing: countryName)), is this correct?",
                preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "No",
                                             style: .cancel, handler: nil)
            let okAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
                
                signUpDictionary[AppKey.countryCode] = countryCode
                signUpDictionary[AppKey.countryCodeID] = countryCodeId
                self.signUpUpdateAction()
            }
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
        }else{
            UIAlertController.show(self, AppTitle.appTital, "Please select your citizenship")
        }
        
        
        
        
    }
    

}

extension SignUpCountryCodeVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return filteredNFLTeams.count
        }
       return countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            
            cell.btnCheck.isSelected = false
            
            if isFiltering() {
                if selectcountryArr.count > 0{
                if ((selectcountryArr[0] as! NSDictionary).value(forKey: "code") as! String) == ((filteredNFLTeams[indexPath.row] as! NSDictionary).value(forKey: "code") as? String)!{
                    cell.btnCheck.isSelected = true
                    }
                    
                }
                
                cell.lblCountryName.text = ((filteredNFLTeams[indexPath.row] as! NSDictionary).value(forKey: "name") as? String)!.uppercased()
            } else {
                
                if selectcountryArr.count > 0{
                    if ((selectcountryArr[0] as! NSDictionary).value(forKey: "code") as! String) == ((countryList[indexPath.row] as! NSDictionary).value(forKey: "code") as? String)!{
                        cell.btnCheck.isSelected = true
                    }
                }
                
                
                cell.lblCountryName.text = ((countryList[indexPath.row] as! NSDictionary).value(forKey: "name") as? String)!.uppercased()
            }
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let countryName : String
        var countryCode : String
        
        let cell = tableView.cellForRow(at: indexPath) as? CountryCell
        
        if isFiltering() {
            
            if cell?.btnCheck.isSelected == true{
                selectcountryArr = [Any]()
            }else{
                selectcountryArr = [filteredNFLTeams[indexPath.row]]
            }
            
            
            
            /*countryName = (((filteredNFLTeams[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!
            countryCode = (((filteredNFLTeams[indexPath.row] as? NSDictionary)?.value(forKey: "code") as? String)?.uppercased())!*/
        }else{
            
           if cell?.btnCheck.isSelected == true{
                selectcountryArr = [Any]()
            }else{
                selectcountryArr = [AppInstance.applicationInstance.countryArray[indexPath.row]]
            }
            
            
            
            /*countryName = (((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased())!
            countryCode = (((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "dial_code") as? String)?.uppercased())!*/
        }
        tblCitizen.reloadData()
        
        
       /* countryCode = countryCode.replacingOccurrences(of: "+", with: "", options: .literal, range: nil)
        
        let alert = UIAlertController(title: AppTitle.appTital,
                                      message: "Do you want to go with \(String(describing: countryName))?",
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "No",
                                         style: .cancel, handler: nil)
        let okAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default) { action -> Void in
            
            signUpDictionary[AppKey.countryCode] = countryCode
            self.signUpUpdateAction()
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)*/
        
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    
}

extension SignUpCountryCodeVC: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text, !searchText.isEmpty 
        {
            filteredNFLTeams = countryList.filtered(using: NSPredicate(format: "name contains[cd] %@", searchText))

        } else {
            filteredNFLTeams = countryList as! [Any]
        }
        tblCitizen.reloadData()
    }
    
    func willDismissSearchController(_ searchController: UISearchController){
        
    }
    
    
}
