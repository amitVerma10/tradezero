//
//  SignUpPasswordVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign Up screen textfield
fileprivate enum SignUpField: Int {
    case password = 100
}

class SignUpPasswordVC: BaseVC {
    var keyboardHeight : CGFloat = 0
    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    @IBOutlet weak var txtPassword: UITextField!

    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpPasswordVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // nextButton initian frame
        setnextButtonFrame()
        
        //self.txtPassword.becomeFirstResponder()
        self.txtPassword.autocorrectionType = UITextAutocorrectionType.no
        self.txtPassword.autocapitalizationType = .none

        

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtPassword.addTarget(self, action: #selector(SignUpPasswordVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    
    //MARK:  buttons Action
    
    @IBAction func btnEyeOnOffAction(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
        }else{
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextBtnAction(_ sender: Any) {
        signUpDictionary[AppKey.password] = txtPassword.text
        signUpDictionary[AppKey.phoneNumber] = ""
        print(signUpDictionary)
        self._signUpAPI(signUpDictionary)
    }
    
    
    
    
    
 // MARK:  API Methods
    private func _signUpAPI(_ signUpDic: [String : Any]) {
        //http://tradezero.co:2701/api/SignProcess/createSignupStep
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUp(signUpDic) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                if let accountId = response?["Account_id"]{
                    signUpDictionary[AppKey.accountId] = accountId
                }
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                self.navigationController?.pushViewController(self.signUpToPhoneNoVC(), animated: true)

            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
            
        }
        
    }
    
    
    
    
    // MARK:  Private Methods
    private func _signUp() {
        guard let passwordField = txtPassword , _isValidRequest(field: .password, textField: txtPassword) else {
            return
        }
        signUpToPhoneNumberPush(passwordField)
    }
    
    
    private func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .password:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterPassword)
                return false
            }
            if textField.text?.length() < KPasswordMinLength{
                UIAlertController.show(self, KAlertTitle, KMinPasswordLength)
                return false
            }
            if textField.text?.length() > KPasswordMaxLength{
                UIAlertController.show(self, KAlertTitle, KMaxPasswordLength)
                return false
            }
            return true
            
        }
    }
    // MARK:  API Method
    private func signUpToPhoneNumberPush(_ email: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        self.navigationController?.pushViewController(signUpToPhoneNoVC(), animated: true)
        
    }
    
    
}

extension SignUpPasswordVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        
        
        if textField.text?.length() == 0 ||
        textField.text?.length() < KPasswordMinLength ||
        textField.text?.length() > KPasswordMaxLength
        {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }
    }
}
