//
//  SignUpTermAndConVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/24/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpTermAndConVC: BaseVC {
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpTermAndConVC"
    }
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnCashAccount: UIButton!
    @IBOutlet weak var btnMarginAccount: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        
        getMarginTCDataAPI()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:  API Methods
    private func getMarginTCDataAPI() {
        
        let url  = "http://tradezero.co:2701/api/SignProcess/GetMarginContent"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendGetRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                self.loadHtmltoWebView(text: response!["Message"] as! String)
                ActivityIndicator.shared.hide()
            default:
                self.errorHandling(response, status, error)
            }
        }
    }
    //Load Html to webview
    private func loadHtmltoWebView(text: String) {
        var html = "<html><head><title></title></head><body style=\"background:transparent;\">"
        //continue building the string
        html += text//"body content here"
        html += "</body></html>"
        
        //make the background transparent
        webView.backgroundColor = UIColor.clear
        //pass the string to the webview
        webView.loadHTMLString(html.description, baseURL: nil)
        
       
       
    }

    @IBAction func btnCheckAction(_ sender: UIButton) {
        
        
        if sender.isSelected == true{
            btnCheck.isSelected = false
            btnCashAccount.isSelected = false
            btnMarginAccount.isSelected = false
            signUpDictionary[AppKey.isMarginLoanAgreement] = false//set check box value in parameter
            signUpDictionary[AppKey.isBorrowFundsAccount] = false
            
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5

        }else{
            btnCheck.isSelected = true
            signUpDictionary[AppKey.isMarginLoanAgreement] = true
            
            if btnMarginAccount.isSelected == true || btnCashAccount.isSelected == true{
                btnNext.isUserInteractionEnabled = true
                btnNext.alpha = 1
            }
            

        }
        
    }
    
    @IBAction func btnRadioAction(_ sender: UIButton) {
        
        if btnCheck.isSelected == true{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1
        }else{
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }
        
        
        if sender.tag == 100{
            btnCashAccount.isSelected = true
            btnMarginAccount.isSelected = false
            signUpDictionary[AppKey.isBorrowFundsAccount] = false//set parameter values
            
            
            
        }else{
            btnCashAccount.isSelected = false
            btnMarginAccount.isSelected = true
            signUpDictionary[AppKey.isBorrowFundsAccount] = true
        }
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton){
        
        // condition for api hit
        
        
        if btnMarginAccount.isSelected == true || btnCashAccount.isSelected == true {
            self.signUpUpdateAction()
        }
        
    }
    
    fileprivate func signUpUpdateAction(){
        
        //http://tradezero.co:2701/api/SignProcess/TermsandConditions?AccountId=9229
        
        guard let acoountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let Url = "\(API.signUpTermCondition)\(String(describing: acoountId))"
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUpWithTermCodition(signUpDictionary,Url) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                //KAppDelegate.goToRootViewAfterLogin()
                self.navigationController?.pushViewController(self.signUpDocumentVC(), animated: true)
                
            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
            
        }
        
        
    }
    

}
