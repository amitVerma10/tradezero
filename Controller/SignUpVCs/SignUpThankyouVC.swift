//
//  SignUpThankyouVC.swift
//  TradeZero
//
//  Created by Amit Verma on 5/9/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpThankyouVC: BaseVC {
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpThankyouVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        KAppDelegate.lououtAction()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
