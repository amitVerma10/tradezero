//
//  SignUpPhoneNoVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign up screen textfield
fileprivate enum SignUpField: Int {
    case textFirst = 100
    
}

class SignUpPhoneNoVC: BaseVC {
    var keyboardHeight : CGFloat = 0

    //button
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet weak var btnCountryCode: UIButton!
    
    //textFields
    
    @IBOutlet weak var txtNo1: UITextField!
    @IBOutlet weak var txtNo2: UITextField!
    @IBOutlet weak var txtNo3: UITextField!
    
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var tblCountryCode: UITableView!
    //Variables
    private var countryList = [Any]()
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpPhoneNoVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // nextButton initian frame
        setnextButtonFrame()
        
        //txtNo1.becomeFirstResponder()
        self.txtNo1.autocorrectionType = UITextAutocorrectionType.no
        

        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtNo1.addTarget(self, action: #selector(SignUpPhoneNoVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
 

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        
        
        // Do any additional setup after loading the view.
        
        signUpDictionary = UserPreferences.getSignUpUserDetails()
        
        // tapGesture work for remove countrycode list
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        tapGesture.delegate = self
        countryListView.addGestureRecognizer(tapGesture)

        
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tblCountryCode))!{
            return false
        }
        return true
    }
    
    
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        countryListView.removeFromSuperview()
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notif: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }
    
    private func setnextButtonFrame(){
        
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
        
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        var mobileStr = txtNo1.text
        mobileStr = mobileStr?.replacingOccurrences(of: "-", with: "")
        
        if let countryCodetext = btnCountryCode.titleLabel?.text {
            signUpDictionary[AppKey.phoneNumber] = "\(countryCodetext)\(String(describing: mobileStr!))"
            print(signUpDictionary)
            self._signUpAPI(phoneNumber: signUpDictionary[AppKey.phoneNumber] as! String)
        }
        
        
    }
    
 
    
    @IBAction func countryCodeAction(_ sender: Any) {
        
        self.txtNo1.resignFirstResponder()
        
        countryListView.frame = self.view.bounds
        self.view.addSubview(countryListView)
        
        
         AppInstance.applicationInstance.countryArray = BundleDataClass.getCountryList()
         tblCountryCode.reloadData()
    }
    
    
    // MARK:  API Methods
    private func _signUpAPI(phoneNumber: String) {
        //http://tradezero.co:2701/api/SignProcess?AccountId=9702&phoneNumber=+919642509886&type=
        //http://tradezero.co:2701/api/SignProcess/PhoneNumberVerification?AccountId=9702&phoneNumber=+919642509886&type=

        guard let accountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let url  = "\(API.signUp)/PhoneNumberVerification?AccountId=\(String(describing: accountId))&phoneNumber=\(phoneNumber)&type="
        print(url)
        
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            print(status)
             ActivityIndicator.shared.hide()
            switch status {
            case .success:
                if let requestId = response?[AppKey.requestId]{
                    signUpDictionary[AppKey.requestId] = requestId
                }
                signUpDictionary[AppKey.typeOTP] = ""
                
                self.txtNo1.resignFirstResponder()//dismiss Keyboard
                 self.navigationController?.pushViewController(self.signUpVeriCodeVC(), animated: true)
                
                
            default:
                if response?["status"] as? Int == 10{
                    if let requestId = response?[AppKey.requestId]{
                        signUpDictionary[AppKey.requestId] = requestId
                    }
                    signUpDictionary[AppKey.typeOTP] = ""
                    
                    self.txtNo1.resignFirstResponder()//dismiss Keyboard
                    self.navigationController?.pushViewController(self.signUpVeriCodeVC(), animated: true)
                }
                self.errorHandling(response, status, error)
            }
        }
    }
    


}

extension SignUpPhoneNoVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    @objc func textFieldDidChange(_ textField : UITextField){
        
        if textField.text?.length() == 0 || textField.text?.length() < 12
        {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }
        
       
        
    }
    
    
    
   
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if textField == txtNo1{
            
            return checkEnglishPhoneNumberFormat(string: string, str: str)
            
        }
        
      else if textField == txtNo3{
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 4
        }
        
        
        return true
    }
    
    
    
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.characters.count < 3{
            
            if str!.characters.count == 1{
                
                txtNo1.text = ""
            }
            
        }else if str!.characters.count == 4{
            
            txtNo1.text = txtNo1.text! + "-"
            
        }else if str!.characters.count == 8{
            
            txtNo1.text = txtNo1.text! + "-"
            
        }else if str!.characters.count > 12{
            
            return false
        }
        
        return true
    }
    
    
}

extension SignUpPhoneNoVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppInstance.applicationInstance.countryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            
            cell.lblCountryName.text = ((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "name") as? String)?.uppercased()
            cell.lblCountryCode.text = (AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "dial_code") as? String
            
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryListView.removeFromSuperview()
      btnCountryCode.setTitle((AppInstance.applicationInstance.countryArray[indexPath.row] as? NSDictionary)?.value(forKey: "dial_code") as? String,for: .normal)
    }
    
    
}

