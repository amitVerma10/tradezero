//
//  SignUpGoalVC.swift
//  TradeZero
//
//  Created by Amit Verma on 4/27/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpGoalVC: BaseVC {
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpGoalVC"
    }
    
    @IBOutlet weak var btnGoal1: UIButton!
    @IBOutlet weak var btnGoal2: UIButton!
    @IBOutlet weak var btnGoal3: UIButton!
    @IBOutlet weak var btnGoal4: UIButton!
    @IBOutlet weak var btnGoal5: UIButton!
    
    
    @IBOutlet weak var btnRisk1: UIButton!
    @IBOutlet weak var btnRisk2: UIButton!
    @IBOutlet weak var btnRisk3: UIButton!
    @IBOutlet weak var btnRisk4: UIButton!
    
    @IBOutlet weak var nextBtn: CustomButton!
    
    let goalsArr:[String] = ["Income","Long Term Growth","Short Term Trading","Active or Day Trading","Preservation or Capital"]
    let riskArr: [String] = ["Low", "Moderate", "Speculation" , "High Risk"]
    
    var selectedGoalArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextBtn.alpha = 0.5
        nextBtn.isUserInteractionEnabled = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func GoalAction(_ sender: UIButton) {
        
        /*btnGoal1.isSelected = false
        btnGoal2.isSelected = false
        btnGoal3.isSelected = false
        btnGoal4.isSelected = false
        btnGoal5.isSelected = false*/
        
        
        
        if sender.isSelected == true{
            sender.isSelected = false
            
            let index = selectedGoalArr.enumerated().filter {
                $0.element == (goalsArr[sender.tag])
                }.map{$0.offset}
            
            print(index)
            selectedGoalArr.remove(at: index[0])
            
            
        }else{
            sender.isSelected = true
            selectedGoalArr.append(goalsArr[sender.tag])
            
        }
        
        self.checkNextButtonIntraction()
        
        
    }
    
    @IBAction func riskAction(_ sender: UIButton) {
        
        btnRisk1.isSelected = false
        btnRisk2.isSelected = false
        btnRisk3.isSelected = false
        btnRisk4.isSelected = false
        
        
        if sender.isSelected == true{
            sender.isSelected = false
        }else{
            sender.isSelected = true
            signUpDictionary[AppKey.riskTolerance] = riskArr[sender.tag]
            
            self.checkNextButtonIntraction()
        }
    }
    
    
    private func checkNextButtonIntraction(){
        nextBtn.alpha = 0.5
        nextBtn.isUserInteractionEnabled = false
        if  signUpDictionary[AppKey.riskTolerance] != nil && selectedGoalArr.count>0{
            nextBtn.alpha = 1
            nextBtn.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        let goalString = selectedGoalArr.joined(separator: ",")
        signUpDictionary[AppKey.accountFuture] = goalString
        
        signUpUpdateAction()
    }
    
    fileprivate func signUpUpdateAction(){
        
        //http://tradezero.co:2701/api/SignProcess/saveFinancial?accountId=9685
        guard let acoountId = signUpDictionary[AppKey.accountId] else{
            return
        }
        let Url = "\(API.signUpFinancial)\(String(describing: acoountId))"
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.signUpWithFinancialAndGoal(signUpDictionary,Url) { (response, status, error) in
            print(status)
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                print(signUpDictionary)
                UserPreferences.setSignUpUserDetail(signUpDictionary)
                
                //KAppDelegate.goToRootViewAfterLogin()
                //babk work Will countinue in second phase
                //self.navigationController?.pushViewController(self.signUpVerifyBankAccount(), animated: true)
                self.navigationController?.pushViewController(self.signUpTermAndConVC(), animated: true)
                
            default:
                ActivityIndicator.shared.hide()
                self.errorHandling(response, status, error)
                
            }
            
        }
        
        
    }
    
    

}
