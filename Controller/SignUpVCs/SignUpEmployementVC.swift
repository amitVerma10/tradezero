//
//  SignUpEmployementVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/5/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SignUpEmployementVC: BaseVC {
   
    @IBOutlet weak var lblFedral: UILabel!
    let empArr = ["Employed","Self-Employed","Not-Employed","Retired","Student","Other"]
    let empArrValue = [1,2,3,4,5,6]
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SignUpEmployementVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if signUpDictionary[AppKey.countryCode] as! String == "US"{
            lblFedral.isHidden = false
        }else{
            lblFedral.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension SignUpEmployementVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return empArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? CountryCell{
            cell.selectionStyle = .none
            cell.lblCountryName.text = empArr[indexPath.row]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        signUpDictionary[AppKey.employmentStatusId] = empArrValue[indexPath.row]//set employement status id
        self.navigationController?.pushViewController(self.signUpBrokerVC(), animated: true)
        
    }
        
    
    
}

