//
//  LeftBarVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/30/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import TOPasscodeViewController

enum LeftMenu: Int {
    case watchlist = 0
    case portfolio
    //case banking
    case history
    case setting
    case help
}


class LeftBarVC: BaseVC{
    var passcode = String()
    let style = TOPasscodeViewStyle(rawValue: 0)
    var type = TOPasscodeType(rawValue: 0)
    
    @IBOutlet weak var lblUserID: UILabel!
    
    
    @IBOutlet weak var tblLeft: UITableView!
    //let tblArray = ["WatchList","Portfolio","Banking","History","Setting","Help"]
    let tblArray = ["WatchList","Portfolio","History","Setting","Help"]
   
    @IBOutlet weak var lblBuyingPower: UILabel!
    @IBOutlet weak var lblPortFolioValue: UILabel!
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "LeftBarVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        lblUserID.text = userAccountId
        tblLeft.isScrollEnabled = false
        
        if (UserDefaults.standard.value(forKey: "passcode") != nil){
            if UserDefaults.standard.bool(forKey: "pinCodeOn"){
                
                self.passcode = UserDefaults.standard.value(forKey: "passcode") as! String
                
                let passcodeViewController = TOPasscodeViewController(style: style!, passcodeType: type!)
                passcodeViewController.delegate = self
                present(passcodeViewController, animated: true) {() -> Void in }
            }
            
        }else{
            
            let settingsController = TOPasscodeSettingsViewController()
            settingsController.passcodeType = type!
            settingsController.delegate = self
            settingsController.requireCurrentPasscode = false
            let navController = UINavigationController(rootViewController:settingsController)
            self.slideMenuController()?.changeMainViewController(navController, close: true)
        }
        
       

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let bpValue = signUpDictionary[AppKey.buyingPower] as? Double{
            lblBuyingPower.text = "$ \(String(describing: bpValue))"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crossAction(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
    }
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .watchlist:
            //let navController = UINavigationController(rootViewController:homeVC())
            let navController = UINavigationController(rootViewController:watchListVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)
        case .portfolio:
            let navController = UINavigationController(rootViewController:portfolioVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)
       /* case .banking:
            let navController = UINavigationController(rootViewController:bankingVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)*/
        case .history:
            
            let navController = UINavigationController(rootViewController:historyVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)
        case .setting:
            let navController = UINavigationController(rootViewController:settingVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)
        case .help:
            /*let navController = UINavigationController(rootViewController:homeVC())
            self.slideMenuController()?.changeMainViewController(navController, close: true)*/
            break
        }
        
    }
    

}
extension LeftBarVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")as! LeftBarCell
        cell.selectionStyle = .none

        cell.lblType.text = tblArray[indexPath.row]
        cell.imgType.image = UIImage.init(named: tblArray[indexPath.row].lowercased())
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    
}

extension LeftBarVC : TOPasscodeViewControllerDelegate {
    
    func didTapCancel(in passcodeViewController: TOPasscodeViewController) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    func passcodeViewController(_ passcodeViewController: TOPasscodeViewController, isCorrectCode code: String) -> Bool {
        return code == passcode
    }
}

extension LeftBarVC :  TOPasscodeSettingsViewControllerDelegate{
    
    func passcodeSettingsViewController(_ passcodeSettingsViewController: TOPasscodeSettingsViewController, didAttemptCurrentPasscode passcode: String) -> Bool {
        return passcode == self.passcode
    }
    
    func passcodeSettingsViewController(_ passcodeSettingsViewController: TOPasscodeSettingsViewController, didChangeToNewPasscode passcode: String, of type: TOPasscodeType) {
        UserDefaults.standard.set(passcode, forKey: "passcode")
        self.passcode = passcode
        self.type = type
        let navController = UINavigationController(rootViewController:watchListVC())
        self.slideMenuController()?.changeMainViewController(navController, close: true)
        UserDefaults.standard.set(true, forKey: "pinCodeOn")
        
    }
    
}
