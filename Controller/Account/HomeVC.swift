//
//  AccountVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/22/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit



class HomeVC: BaseVC {
    
     let date = Date()
    
    @IBOutlet weak var headerImgHeight: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    private var watchListArr = [WatchList]()
    fileprivate var positionArr = [String]()
    
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    fileprivate var symbolArrTrade = [String]()
    
    //Position work
    var arrAccountDetails = [PositionListModel]()
    @IBOutlet weak var tblHome: UITableView!
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "HomeVC"
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
                 
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.accountDetailsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.accountDetailsResponse), object: nil)
        
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeInHomeVC), object: nil)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteInHomeVC), object: nil)
        
        
        headerImgHeight.constant = 0.0// give header image value
        titleHeight.constant = 40.0
        self.tblHome.tableHeaderView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 150)
        
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        UserDefaults.standard.set(true, forKey: "isHomeView")
        symbolArrTrade = [String]()
        arrAccountDetails = [PositionListModel]()
        tblHome.reloadData()
        
        // call account detail request
        WebSocketManager.sharedInstance.accountDetailRequrst(accountId: userAccountId)//APPS02
        
    }
    
    
    @IBAction func addMoreCompaniesAction(_ sender: Any) {
        self.searchAction(self)
    }
    
    
    

    @objc func accountDetailsData(_ notification : NSNotification){
        
        
        guard let accountDic = notification.userInfo!["data"] as? KeyValue else{
            return
        }
        signUpDictionary[AppKey.buyingPower] = accountDic[AppKey.buyingPower] as! Double
        
        guard let accountArr = accountDic["positions"] as? [KeyValue],accountArr.count > 0 else{
            return
        }
        if accountArr.count == 0{
            headerImgHeight.constant = 297// give header image value
            titleHeight.constant = 40.0
            self.tblHome.tableHeaderView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 150)
        }
        
        positionArr = [String]()
        
        for i in 0 ..< accountArr.count {
            if let  dataDic = accountArr[i] as? KeyValue{
                //arrAccountDetails.append(PositionListModel.init(dataDic)!)
                positionArr.append(dataDic["type"] as! String)
                //Call Company data from websocket
                WebSocketManager.sharedInstance.subscribeCompany(symbol: dataDic["symbol"] as! String)
                
            }
        }

        
        
        //Create a watchList
        WebSocketManager.sharedInstance.createWatchList(name: userAccountId)
       // NotificationCenter.default.addObserver(self, selector: #selector(self.createWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.createWatchList), object: nil)
        
    }
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            quoteDic = companyData
            
            //check for update model value
            if symbolArrTrade.containsObject(companyData["sym"] as! String){
                let index = symbolArrTrade.index{$0 == companyData["sym"] as! String}
                if arrAccountDetails.count > 0{
                    let model = arrAccountDetails[index!]
                    self.updateQuotesValuesInModel(model, companyData: companyData )
                }
            }
            
        }
    }
    
   
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadTradeData(quoteDic, tradeDic)
        }
        
    }
    
    
    
    @objc func createWatchList(_ notification : NSNotification){
        //retrieve a watchList
        WebSocketManager.sharedInstance.retrieveWatchList(name: userAccountId)
       // NotificationCenter.default.addObserver(self, selector: #selector(self.retrieveWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.retrieveWatchList), object: nil)
        
    }
    
    @objc func retrieveWatchList(_ notification : NSNotification){
        guard let watchListArr = notification.userInfo!["symbols"] as? [String]else{
            return
        }
        publicWatchListArr = watchListArr
        print(watchListArr)
    }
    
    //MARK: Load both Quote and Trade Value
    private func loadTradeData(_ quoteDic1:KeyValue, _ tradeDic1: KeyValue){
        //print(quoteDic,tradeDic)
        let dataDic = quoteDic1.merged(with: tradeDic1)
        
        // this is for update stock details value in real time
        if symbolArrTrade.containsObject(dataDic["sym"] as! String){
            let index = symbolArrTrade.index{$0 == dataDic["sym"] as! String} // 0
            if arrAccountDetails.count > 0{
                let model = arrAccountDetails[index!]
                self.updateTradeValuesInModel(model, companyData: dataDic)//tradeDic1
            }
            
        }else{
            symbolArrTrade.append(dataDic["sym"] as! String)
            arrAccountDetails.append(PositionListModel.init(dataDic)!)
            tblHome.reloadData()
        }
        
        
        
    }
    
    
    //MARK: update quote value when data change
    func updateQuotesValuesInModel(_ model : PositionListModel,companyData:KeyValue ){
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        
        tblHome.reloadData()
        
    }
    //MARK: update Trade value when data change
    func updateTradeValuesInModel(_ model : PositionListModel, companyData : KeyValue ){
        
        model.currentPrice = date.isBetweenTwoTime() ? companyData[AppKey.lastTradePrice] as? Double ?? 0.00 : companyData[AppKey.lastTradePrice] as? Double ?? 0.00
        model.isUp = calculateGainOrLoss(companyData)
        model.percentValue = calculateGainOrLossValue(companyData)
        model.time = companyData[AppKey.orderCreated] as? Int ?? 0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.volume = companyData[AppKey.tradeVolume] as? Int ?? 0
        model.stocktType = companyData[AppKey.orderType] as? String ?? "N/A"
      
        tblHome.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func searchAction(_ sender: Any) {
        self.navigationController?.pushViewController(searchCompanyVC(), animated: true)
    }
    
    
    //calculate gain or loss of stock
    private func calculateGainOrLoss(_ tradeDic:KeyValue)->Bool
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return false
        }
        
        if Double(truncating: lastTrade as! NSNumber) >= Double(truncating: prevoiusClose as! NSNumber) {
            return true
        }else{
            return false
        }
    }
    
    //calculate gain or loss of stock
    private func calculateGainOrLossValue(_ tradeDic:KeyValue)->String
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return "0.0 (0%)"
        }
        let diffrence = (Double(truncating: lastTrade as! NSNumber) - Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        let percentage = (diffrence*100/Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        
        return "\(String(diffrence)) (\(String(percentage))%)"
        
    }

    
}


extension HomeVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return arrAccountDetails.count
        }else{
            return watchListArr.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? HomeCell{
            cell.selectionStyle = .none
            cell.lblSymbol.text = arrAccountDetails[indexPath.row].companySym
            
            cell.lblCurrentPrice.text = String(describing : arrAccountDetails[indexPath.row].currentPrice!)
            cell.lblPercentage.text = arrAccountDetails[indexPath.row].percentValue
            
            /*if let timeStamp =  arrAccountDetails[indexPath.row].time{
                let timeString = String(timeStamp)
                cell.lblTime.text = timeString.convertTimeStampToDate64Bit()
            }*/
            
            cell.lblTime.text = "\(String(describing: arrAccountDetails[indexPath.row].hour!)) : \(String(describing: arrAccountDetails[indexPath.row].minut!))"
            cell.lblBid.text = "Bid : \(String(describing: arrAccountDetails[indexPath.row].bid!))"
            cell.lblAsk.text = "Ask : \(String(describing: arrAccountDetails[indexPath.row].ask!))"
            cell.lblVolume.text = "Volume : \(String(describing: arrAccountDetails[indexPath.row].volume!))"
            
            
            if arrAccountDetails[indexPath.row].isUp == true{
                cell.imgUpDown?.image = #imageLiteral(resourceName: "up_arrow")
                cell.lblPercentage.textColor = AppColor.gainColor
            }else{
                cell.imgUpDown?.image = #imageLiteral(resourceName: "down_arrow")
                cell.lblPercentage.textColor = AppColor.lossColor
            }
            
            return cell
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        //Unsubscribe All Symbol
        for i in 0 ..< symbolArrTrade.count {
           WebSocketManager.sharedInstance.unSubscribeCompany(symbol: symbolArrTrade[i])
        }
        
        let dic:KeyValue = [ "cc" : "USD",
                             "e" : "NQNM",
                             "s" : arrAccountDetails[indexPath.row].companySym!]
        let storyboard = UIStoryboard(name: AppKey.companyDetail, bundle:nil)
        if let companyVC = storyboard .instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC {
            CompanyVC.compDic = dic
            CompanyVC.positionValue = positionArr[indexPath.row]
           
        }
        self.navigationController?.pushViewController(companyVC(), animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "TRADELIST"
        }else{
            return "WATCHLIST"
        }
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.color(4.0/255.0, 4.0/255.0, 36.0/255.0, 1.0)
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "Raleway-Bold", size: 15)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tblHome, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)

        return headerView
    }
    
   
    
    
}

extension Dictionary {
    
    mutating func merge(with dictionary: Dictionary) {
        dictionary.forEach { updateValue($1, forKey: $0) }
    }
    
    func merged(with dictionary: Dictionary) -> Dictionary {
        var dict = self
        dict.merge(with: dictionary)
        return dict
    }
}


