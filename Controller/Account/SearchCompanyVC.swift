//
//  SearchCompanyVC.swift
//  TradeZero
//
//  Created by Amit Verma on 12/26/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class SearchCompanyVC: BaseVC {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblSearch: UITableView!
    var searchArr = [KeyValue]()
    var searchActive = Bool()
    var isSearchBySymbol = Bool()
    
    @IBOutlet weak var btnCompanyName: UIButton!
    @IBOutlet weak var btnCompanySumbol: UIButton!
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SearchCompanyVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // check saved value of search filter
        if UserDefaults.standard.bool(forKey: "isSearchBySymbol") == true{
            isSearchBySymbol = true
            searchBar.placeholder = "Search Symbol"
            btnCompanyName.isSelected = false
            btnCompanySumbol.isSelected = true
        }else{
            isSearchBySymbol = false
            btnCompanyName.isSelected = true
            btnCompanySumbol.isSelected = false
            searchBar.placeholder = "Search Company"
        }
        
        
        //Notification Center for
        NotificationCenter.default.addObserver(self, selector: #selector(self.searchCompany(_:)), name: NSNotification.Name(rawValue: SocketConstants.searchDataRecived), object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func searcByCompanyAction(_ sender: UIButton) {
        
        if sender.isSelected == true{
            searchBar.placeholder = "Search Symbol"
            UserDefaults.standard.set(true, forKey: "isSearchBySymbol")
            isSearchBySymbol = true
            
            btnCompanyName.isSelected = false
            btnCompanySumbol.isSelected = true
            
        }else{
            
            searchBar.placeholder = "Search Company"
            UserDefaults.standard.set(false, forKey: "isSearchBySymbol")
            isSearchBySymbol = false
            btnCompanyName.isSelected = true
            btnCompanySumbol.isSelected = false
        }
        
    }
    
    @IBAction func searchByCompanySymbol(_ sender: UIButton) {
        
        if sender.isSelected == true{
            searchBar.placeholder = "Search Company"
            UserDefaults.standard.set(false, forKey: "isSearchBySymbol")
            isSearchBySymbol = false
            
            btnCompanyName.isSelected = true
            btnCompanySumbol.isSelected = false
            
        }else{
            searchBar.placeholder = "Search Symbol"
            UserDefaults.standard.set(true, forKey: "isSearchBySymbol")
            isSearchBySymbol = true
            
            btnCompanyName.isSelected = false
            btnCompanySumbol.isSelected = true
        }
    }
    
    
    //Recive Quote Data notification response
    @objc func searchCompany(_ notification: NSNotification){
        if let companyData = notification.userInfo!["equities"] as? [KeyValue] {
            searchArr = companyData
            tblSearch.reloadData()
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension SearchCompanyVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SearchCell{
            cell.selectionStyle = .none
            cell.lblCompanyName.text = (searchArr[indexPath.row] )[AppKey.searchCompanyName] as? String ?? "N/A"
            cell.lblCompanySym.text  = (searchArr[indexPath.row] )[AppKey.companySym] as? String ?? "N/A"
            
            return cell
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let symbol = (searchArr[indexPath.row] )[AppKey.companySym] as? String {
            
            let dic:KeyValue = [ "cc" : "USD",
                                 "e" : "NQNM",
                                 "s" : symbol]
            let storyboard = UIStoryboard(name: AppKey.companyDetail, bundle:nil)
            if let companyVC = storyboard .instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC {
                CompanyVC.compDic = dic
                print(accountPositionDic,accountOpenQuantityDic,accountEntryPriceDic)//print global assign value of position
                
                CompanyVC.positionValue = "no position"// no position if not in stock for user
                if let position = accountPositionDic[symbol]{
                    print(position)
                    CompanyVC.positionValue = position
                }
                CompanyVC.entryPrice = 0.0 //predefine value  for entry price
                if let entryPrice = accountEntryPriceDic[symbol]{
                    CompanyVC.entryPrice = (entryPrice/100).rounded(toPlaces: 2)
                }
                CompanyVC.quantityOpen = 0//predefine value  for open Quantity
                if let openQuantity = accountOpenQuantityDic[symbol] {
                    CompanyVC.quantityOpen = openQuantity
                }
                
               
            }
             self.navigationController?.pushViewController(companyVC(), animated: true)
        }
        
        
        
        
//        CompanyVC.compDic = searchArr[indexPath.row]
//        self.navigationController?.pushViewController(companyVC(), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
}

extension SearchCompanyVC:UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //Call Search Company data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Search
            if isSearchBySymbol{
                dic["rt"] = "FindEquities" as AnyObject//
            }else{
                dic["rt"] = "FindEquitiesByCompany" as AnyObject// FindEquities
            }
            dic["prefix"] = searchText as AnyObject
            dic["maxResults"] = 10 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
        if(searchArr.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        //self.tableView.reloadData()
    }

}

