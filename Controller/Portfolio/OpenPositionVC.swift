//
//  OpenPositionVC.swift
//  TradeZero
//
//  Created by Amit Verma on 3/14/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OpenPositionVC: UIViewController {
    
    var parentNavigationController : UINavigationController?
    
    let date = Date()

    @IBOutlet weak var tblOpenPosition: UITableView!
    
    var arrOpenPosition = [OpenPositionModel]()
    var symbolArrClose = [String]()
    var quantityOpenArr = [Double]()
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    fileprivate var symbolArrTrade = [String]()
    fileprivate var symbolArrOpen = [String]()
    
    var istableCellSelect : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // companyDataRecivedTradeInHomeVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeOpenPositionVC), object: nil)
        // companyDataRecivedQuoteInHomeVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteOpenPositionVC), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadDataAfterCloseViewDisappper(_:)), name: NSNotification.Name(rawValue: "loadOpenPositionData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func loadDataAfterCloseViewDisappper(_ notification: NSNotification){
        
       self.loadOpenPositionData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
 
        symbolArrOpen.removeAll()
        arrOpenPosition.removeAll()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        istableCellSelect = false
        
    }
    
    //when data is not loaded
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
        
        self.loadOpenPositionData()
        
    }
    
    
     func loadOpenPositionData() {
        
        UserDefaults.standard.set(true, forKey: "isHomeView")
        
        symbolArrOpen.removeAll()
        arrOpenPosition.removeAll()
        UIView.performWithoutAnimation {
          tblOpenPosition.reloadData()
        }
        
        for i in 0 ..< accountPositionArr.count {
            if let  dataDic = accountPositionArr[i] as? KeyValue{
                if dataDic[AppKey.quantityOpen] as? Int > 0{
                    symbolArrOpen.append(dataDic["symbol"] as! String)
                }
            }
        }
        UIView.performWithoutAnimation {
            tblOpenPosition.reloadData()
        }
        
        
        for i in 0 ..< accountPositionArr.count {
            if let  dataDic = accountPositionArr[i] as? KeyValue{
                
                if dataDic[AppKey.quantityOpen] as? Int > 0{
                    arrOpenPosition.append(OpenPositionModel.init(dataDic)!)
                    WebSocketManager.sharedInstance.subscribeCompany(symbol: dataDic["symbol"] as! String)
                }
            }
        }
    }
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            quoteDic = companyData
            print(symbolArrOpen)
            //check for update model value
            
            if symbolArrOpen.contains(companyData["sym"] as! String){
                
                let indexes = symbolArrOpen.enumerated().filter {
                    $0.element == (companyData["sym"] as! String)
                    
                    }.map{$0.offset}
                   print(indexes)
                for i in 0..<indexes.count {
                    if  arrOpenPosition.count > 0 {
                        let model = arrOpenPosition[indexes[i]]
                        self.updateQuotesValuesInModel(model, companyData: companyData ,index: indexes[i] )
                        
                    }
                }
                
            }
            
        }
    }
    
    
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadTradeData(quoteDic, tradeDic )
        }
        
    }
    
    
    
    //MARK: Load both Quote and Trade Value
    private func loadTradeData(_ quoteDic1:KeyValue, _ tradeDic1: KeyValue){
        //print(quoteDic,tradeDic)
        let dataDic = quoteDic1.merged(with: tradeDic1)
        
        // this is for update stock details value in real time
        if symbolArrOpen.containsObject(dataDic["sym"] as! String){
            
            let indexes = symbolArrOpen.enumerated().filter {
                $0.element == (dataDic["sym"] as! String)
                }.map{$0.offset}
            
            print(indexes)
            
            for i in 0..<indexes.count {
                if arrOpenPosition.count > 0 {
                    let model = arrOpenPosition[indexes[i]]
                    self.updateTradeValuesInModel(model, companyData: dataDic, index:indexes[i])//tradeDic1
               }
            }
            
           /* let index = symbolArrOpen.index{$0 == dataDic["sym"] as! String}
            if  arrOpenPosition.count > 0 {
                let model = arrOpenPosition[index!]
                self.updateTradeValuesInModel(model, companyData: dataDic ,index: index!)
                
            }*/
            
        }else{
            symbolArrOpen.append(dataDic["sym"] as! String)
            arrOpenPosition.append(OpenPositionModel.init(dataDic)!)
            UIView.performWithoutAnimation {

                tblOpenPosition.reloadData()
                
            }
        }
        
        
        
    }
    
    
    //MARK: update quote value when data change
    func updateQuotesValuesInModel(_ model : OpenPositionModel,companyData:KeyValue, index:Int ){
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        
        
        UIView.performWithoutAnimation {
            
            let indexPath = IndexPath(item: index, section: 0)
            self.updatePerticularCell(indexPath: indexPath)
        }
        
        
    }
    //MARK: update Trade value when data change
    func updateTradeValuesInModel(_ model : OpenPositionModel, companyData : KeyValue, index:Int ){
        
        model.currentPrice =  companyData[AppKey.lastTradePrice] as? Double ?? 0.00
        model.volume = companyData[AppKey.tradeVolume] as? Int ?? 0
        model.stocktType = companyData[AppKey.orderType] as? String ?? "N/A"
        
        UIView.performWithoutAnimation {
            
            let indexPath = IndexPath(item: index, section: 0)
            self.updatePerticularCell(indexPath: indexPath)
        }
    }
    
    
    private func updatePerticularCell(indexPath : IndexPath){
        
        let cell = tblOpenPosition.cellForRow(at: indexPath) as? PositionCell
        
        cell?.lblSymbol.text = arrOpenPosition[indexPath.row].companySym
        cell?.lblShares.text = String(describing: Int(arrOpenPosition[indexPath.row].shares!))
        cell?.lblEntryDate.text = String(describing:arrOpenPosition[indexPath.row].enrtyDate!)
        cell?.lblEntryPrice.text = "$\(String(describing:arrOpenPosition[indexPath.row].entryPrice!))"
        cell?.lblCurrentPrice.text = "$\(String(describing:arrOpenPosition[indexPath.row].currentPrice!))"
        cell?.lblEntryTime.text = String(describing:arrOpenPosition[indexPath.row].enrtyTime!)
        
        
        //print(accountPositionDic)//print global assign value of position
        if let position = accountPositionDic[arrOpenPosition[indexPath.row].companySym!] as? String{
            //print(position)
            if position == "Long"{
                
                let plvalue : Double = (arrOpenPosition[indexPath.row].bid! - arrOpenPosition[indexPath.row].entryPrice!)*arrOpenPosition[indexPath.row].shares!
                cell?.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell?.lblPL.textColor = AppColor.gainColor
                }else{
                    cell?.lblPL.textColor = AppColor.lossColor
                }
                
                
            }else if position == "Short"{
                let plvalue : Double = (arrOpenPosition[indexPath.row].ask! - arrOpenPosition[indexPath.row].entryPrice!)*arrOpenPosition[indexPath.row].shares!
                cell?.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell?.lblPL.textColor = AppColor.gainColor
                }else{
                    cell?.lblPL.textColor = AppColor.lossColor
                }
            }
            
        }
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}

extension OpenPositionVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symbolArrOpen.count//arrOpenPosition.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PositionCell
        cell.selectionStyle = .none
        
        cell.lblSymbol.text = arrOpenPosition[indexPath.row].companySym
        cell.lblShares.text = String(describing: Int(arrOpenPosition[indexPath.row].shares!))
        cell.lblEntryDate.text = String(describing:arrOpenPosition[indexPath.row].enrtyDate!)
        cell.lblEntryPrice.text = "$\(String(describing:arrOpenPosition[indexPath.row].entryPrice!))"
        cell.lblCurrentPrice.text = "$\(String(describing:arrOpenPosition[indexPath.row].currentPrice!))"
        cell.lblEntryTime.text = String(describing:arrOpenPosition[indexPath.row].enrtyTime!)
        
        
        //print(accountPositionDic)//print global assign value of position
        if let position = accountPositionDic[arrOpenPosition[indexPath.row].companySym!] as? String{
            //print(position)
            if position == "Long"{
                
                let plvalue : Double = (arrOpenPosition[indexPath.row].bid! - arrOpenPosition[indexPath.row].entryPrice!)*arrOpenPosition[indexPath.row].shares! 
                cell.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell.lblPL.textColor = AppColor.gainColor
                }else{
                    cell.lblPL.textColor = AppColor.lossColor
                }
                
                
            }else if position == "Short"{
                let plvalue : Double = (arrOpenPosition[indexPath.row].ask! - arrOpenPosition[indexPath.row].entryPrice!)*arrOpenPosition[indexPath.row].shares!
                cell.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell.lblPL.textColor = AppColor.gainColor
                }else{
                    cell.lblPL.textColor = AppColor.lossColor
                }
            }
            
        }
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //unsubscribeAll symbol
                for i in 0 ..< symbolArrOpen.count {
                    WebSocketManager.sharedInstance.unSubscribeCompany(symbol: symbolArrOpen[i])
                }
        
        //istableCellSelect = true
        print(accountPositionArr)
        
        let dic:KeyValue = [ "cc" : "USD",
                             "e" : "NQNM",
                             "s" : symbolArrOpen[indexPath.row]]
        let storyboard = UIStoryboard(name: AppKey.companyDetail, bundle:nil)
        if let companyVC = storyboard .instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC {
            CompanyVC.compDic = dic
            print(accountPositionDic)//print global assign value of position
            
            CompanyVC.positionValue = "no position"// no position
            if let position = accountPositionDic[symbolArrOpen[indexPath.row]]{
                print(position)
                CompanyVC.positionValue = position
                CompanyVC.quantityOpen = arrOpenPosition[indexPath.row].shares!
                CompanyVC.entryPrice =  arrOpenPosition[indexPath.row].entryPrice!
            }
        }
        parentNavigationController?.pushViewController(companyVC(), animated: true)
        
    }
    
    
    
}
