//
//  OpenOrderVC.swift
//  TradeZero
//
//  Created by Amit Verma on 3/14/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OpenOrderVC: UIViewController {

    var parentNavigationController : UINavigationController?
    
    @IBOutlet weak var tblOpenOrder: UITableView!
    var arrOpenOrder = [OpenOrderModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Recive Notification value
        NotificationCenter.default.addObserver(self, selector: #selector(self.openOrders(_:)), name: NSNotification.Name(rawValue: SocketConstants.openOrderResponse), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        arrOpenOrder.removeAll()
        WebSocketManager.sharedInstance.opneOrder(accountId: userAccountId)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(true)

        if viewControllerName == "OpenPositionVC"{
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadOpenPositionData"), object: nil, userInfo: nil)
            
        }else if viewControllerName == "ClosePositionVC"{
            
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadClosePositionData"), object: nil, userInfo: nil)
            
        }
        
    }

    //when data is not loaded
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
        
        arrOpenOrder.removeAll()
        WebSocketManager.sharedInstance.opneOrder(accountId: userAccountId)
        
    }
    
    
    @objc func openOrders(_ notification: NSNotification){
        
        arrOpenOrder.removeAll()
        
        guard let orderArr = (notification.userInfo!["data"] as! KeyValue)["orders"]
            as? [KeyValue],orderArr.count > 0 else{
               // condition check for open position have values
                if accountPositionArr.count > 0{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "movedToOpenPosition"), object: nil, userInfo: nil)
                }
                
            return
        }
        print(orderArr)
        for i in 0 ..< orderArr.count {
            if let  dataDic = orderArr[i] as? KeyValue{
                arrOpenOrder.append(OpenOrderModel.init(dataDic)!)
            }
       }
        
        UIView.performWithoutAnimation {
            tblOpenOrder.reloadData()
        }
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension OpenOrderVC:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOpenOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let orderType : String = arrOpenOrder[indexPath.row].orderType! as? String{
            
            print(orderType )
            
            if orderType.contains("Limit")  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellLimit") as! OpenOrderLimitCell
                cell.selectionStyle = .none
                
                cell.lblSymbol.text = arrOpenOrder[indexPath.row].companySym
                cell.lblSide.text = arrOpenOrder[indexPath.row].side
                cell.lblRoute.text = arrOpenOrder[indexPath.row].route
                cell.lblShares.text = String(describing :Int(arrOpenOrder[indexPath.row].quantity!))
                cell.lblLimitPrice.text = "$\(String(describing : arrOpenOrder[indexPath.row].limitPrice!/100))"
                cell.lblStopPrice.text = "--"
                cell.lblOrderType.text = arrOpenOrder[indexPath.row].orderType
                cell.lblTimeInForce.text = arrOpenOrder[indexPath.row].timeInForce
               
                
                
                return cell
                
            }
            else  {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! OpenOrderCell
                cell.selectionStyle = .none
                
                cell.lblSymbol.text = arrOpenOrder[indexPath.row].companySym
                cell.lblSide.text = arrOpenOrder[indexPath.row].side
                cell.lblRoute.text = arrOpenOrder[indexPath.row].route
                cell.lblShares.text = String(describing :arrOpenOrder[indexPath.row].quantity!)
                cell.lblLastPrice.text = "N/A"
                cell.lblOrderType.text = arrOpenOrder[indexPath.row].orderType
                cell.lblTimeInForce.text = arrOpenOrder[indexPath.row].timeInForce
               
                
                
                return cell
                
            }
            
        }
        
        
     
       return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let storyboard = UIStoryboard(name: AppKey.history, bundle:nil)
        if let orderVC = storyboard .instantiateViewController(withIdentifier: OrderDetailVC.identifier()) as? OrderDetailVC {
            
            orderVC.orderId = arrOpenOrder[indexPath.row].clientOrderId!
            orderVC.orderType = arrOpenOrder[indexPath.row].orderType!
            //orderVC.timeStamp = arrOpenOrder[indexPath.row].date
            orderVC.orderStatus = arrOpenOrder[indexPath.row].orderStatus!
            orderVC.quantity = String(describing : arrOpenOrder[indexPath.row].quantity!)
            
            parentNavigationController?.pushViewController(orderVC, animated: true)
        }
        
    }
    
}
