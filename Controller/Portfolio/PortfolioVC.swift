//
//  PortfolioVC.swift
//  TradeZero
//
//  Created by Amit Verma on 3/14/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit
import PageMenu

class PortfolioVC: BaseVC ,CAPSPageMenuDelegate {
    
    let date = Date()

    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "PortfolioVC"
    }

    var pageMenu : CAPSPageMenu?
    
    //fileprivate var positionArr = [String]()
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    
    fileprivate var symbolArrTrade = [String]()
    
    //Position work
    var arrAccountDetails = [PositionListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.uploadPageMenu()//upload pagemenu controller
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.accountDetailsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.accountDetailsResponse), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.openOrders(_:)), name: NSNotification.Name(rawValue: "movedToOpenPosition"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
          // call account detail request
         self.uploadPageMenu()//upload pagemenu controller
        
        WebSocketManager.sharedInstance.accountDetailRequrst(accountId: userAccountId)//APPS02
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        for i in 0 ..< accountPositionArr.count {
            WebSocketManager.sharedInstance.unSubscribeCompany(symbol: accountPositionArr[i]["symbol"] as! String)
        }
    }
    
    
    @objc func accountDetailsData(_ notification : NSNotification){
        
        
        guard let accountDic = notification.userInfo!["data"] as? KeyValue else{
            return
        }
        signUpDictionary[AppKey.buyingPower] = accountDic[AppKey.buyingPower] as! Double
        
        guard let accountArr = accountDic["positions"] as? [KeyValue],accountArr.count > 0 else{
            return
        }
        //assign position value globaly
        accountPositionArr = accountArr
        
        guard let position = accountDic["positions"] as? [KeyValue],accountArr.count > 0 else{
            return
            
        }
         //assign value globaly
        for i in 0 ..< accountArr.count {
            if let  dataDic = position[i] as? KeyValue{
                accountPositionDic[dataDic["symbol"] as! String] = dataDic["type"] as? String
                accountOpenQuantityDic[dataDic["symbol"] as! String] = dataDic["quantityOpen"] as? Double
                accountEntryPriceDic[dataDic["symbol"] as! String] = dataDic["entryPriceCents"] as? Double
              }
        }
        
        
    }
    
    @objc func openOrders(_ notification : NSNotification){
        pageMenu?.moveToPage(1)
        NotificationCenter.default.removeObserver(self)
        viewControllerName = "OpenPositionVC"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadOpenPositionData"), object: nil, userInfo: nil)
    }
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            quoteDic = companyData
            
            //check for update model value
            if symbolArrTrade.containsObject(companyData["sym"] as! String){
                let index = symbolArrTrade.index{$0 == companyData["sym"] as! String}
                if arrAccountDetails.count > 0{
                    let model = arrAccountDetails[index!]
                    self.updateQuotesValuesInModel(model, companyData: companyData )
                }
            }
            
        }
    }
    
    
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadTradeData(quoteDic, tradeDic)
        }
        
    }
    
    //MARK: update quote value when data change
    func updateQuotesValuesInModel(_ model : PositionListModel,companyData:KeyValue ){
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        
        //tblHome.reloadData()
        
    }
    //MARK: update Trade value when data change
    func updateTradeValuesInModel(_ model : PositionListModel, companyData : KeyValue ){
        
        model.currentPrice = date.isBetweenTwoTime() ? companyData[AppKey.lastTradePrice] as? Double ?? 0.00 : companyData[AppKey.lastTradePrice] as? Double ?? 0.00
        model.isUp = calculateGainOrLoss(companyData)
        model.percentValue = calculateGainOrLossValue(companyData)
        model.time = companyData[AppKey.orderCreated] as? Int ?? 0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.volume = companyData[AppKey.tradeVolume] as? Int ?? 0
        model.stocktType = companyData[AppKey.orderType] as? String ?? "N/A"
        
        //tblHome.reloadData()
    }
    
    
    //MARK: Load both Quote and Trade Value
    private func loadTradeData(_ quoteDic1:KeyValue, _ tradeDic1: KeyValue){
        //print(quoteDic,tradeDic)
        let dataDic = quoteDic1.merged(with: tradeDic1)
        
        // this is for update stock details value in real time
        if symbolArrTrade.containsObject(dataDic["sym"] as! String){
            let index = symbolArrTrade.index{$0 == dataDic["sym"] as! String} // 0
            if arrAccountDetails.count > 0{
                let model = arrAccountDetails[index!]
                self.updateTradeValuesInModel(model, companyData: dataDic)//tradeDic1
            }
            
        }else{
            symbolArrTrade.append(dataDic["sym"] as! String)
            arrAccountDetails.append(PositionListModel.init(dataDic)!)
            //tblHome.reloadData()
        }
        
        
        
    }
    
    
    
    @IBAction func menuAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        
        if index == 1{
            viewControllerName = "OpenPositionVC"
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadOpenPositionData"), object: nil, userInfo: nil)
            
        }else if index == 2{
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadClosePositionData"), object: nil, userInfo: nil)
            viewControllerName = "ClosePositionVC"
        }
        
        print("did move to page", index)
    }
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page" , index)
        
        //assign value globaly
        for i in 0 ..< accountPositionArr.count {
            WebSocketManager.sharedInstance.unSubscribeCompany(symbol: accountPositionArr[i]["symbol"] as! String)
        }

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: UploadPageMenu
    func uploadPageMenu(){
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let storyboard = UIStoryboard(name: "Portfolio", bundle: nil)
        let channelVC  = storyboard.instantiateViewController(withIdentifier: "OpenOrderVC") as! OpenOrderVC
        let channelVC1 = storyboard.instantiateViewController(withIdentifier: "OpenPositionVC") as! OpenPositionVC
        let channelVC2 = storyboard.instantiateViewController(withIdentifier: "ClosePositionVC") as! ClosePositionVC
        
        channelVC.title = "Open Order"
        channelVC.parentNavigationController = self.navigationController
        channelVC1.title = "Open Position"
        channelVC1.parentNavigationController = self.navigationController
        channelVC2.title = "Close Position"
        channelVC2.parentNavigationController = self.navigationController
        controllerArray.append(channelVC)
        controllerArray.append(channelVC1)
        controllerArray.append(channelVC2)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor(red: 13.0/255.0, green: 11.0/255.0, blue: 32.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(AppColor.gainColor),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(AppColor.gainColor),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .menuItemFont(UIFont(name: "Raleway-SemiBold", size: 17.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(true),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y:84.0, width: self.view.frame.width, height: self.view.frame.height - 84.0), pageMenuOptions: parameters)
        
        // Optional delegate
        pageMenu!.delegate = self
        self.view.addSubview(pageMenu!.view)
        
        
    }
    
    
    
    
    //calculate gain or loss of stock
    private func calculateGainOrLoss(_ tradeDic:KeyValue)->Bool
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return false
        }
        
        if Double(truncating: lastTrade as! NSNumber) >= Double(truncating: prevoiusClose as! NSNumber) {
            return true
        }else{
            return false
        }
    }
    
    //calculate gain or loss of stock
    private func calculateGainOrLossValue(_ tradeDic:KeyValue)->String
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return "0.0 (0%)"
        }
        let diffrence = (Double(truncating: lastTrade as! NSNumber) - Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        let percentage = (diffrence*100/Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        
        return "\(String(diffrence)) (\(String(percentage))%)"
        
    }

    

}
