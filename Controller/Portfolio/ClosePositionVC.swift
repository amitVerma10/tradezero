//
//  ClosePositionVC.swift
//  TradeZero
//
//  Created by Amit Verma on 3/14/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class ClosePositionVC: UIViewController {
    
    var parentNavigationController : UINavigationController?
    
    
    
     var arrClosePosition = [ClosePosition]()
     var symbolArrClose = [String]()
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    var istableCellSelect : Bool = false

    @IBOutlet weak var tblClosePosition: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
        
        
        // companyDataRecivedTradeInHomeVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeClosePositionVC ), object: nil)
        // companyDataRecivedQuoteInHomeVC
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteClosePositionVC), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadDataAfterOpenViewDisappper(_:)), name: NSNotification.Name(rawValue: "loadClosePositionData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //symbolArrClose.removeAll()
        //tblClosePosition.reloadData()
        
        if accountPositionArr.count == 0{
            
            let banner = StatusBarNotificationBanner(title: "No Record found!", style: .danger)
            banner.show()
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

        symbolArrClose.removeAll()
        arrClosePosition.removeAll()
        
        
        
    }
    
    @objc func loadDataAfterOpenViewDisappper(_ notification: NSNotification) {
        
        self.loadClosePositionViewViewData()
    }
    
    //when data is not loaded
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
        
        self.loadClosePositionViewViewData()

    }
    
    
    
    private func loadClosePositionViewViewData(){
        
        UserDefaults.standard.set(true, forKey: "isHomeView")
        
        symbolArrClose.removeAll()
        arrClosePosition.removeAll()
        tblClosePosition.reloadData()
        
        
        for i in 0 ..< accountPositionArr.count {
            if let  dataDic = accountPositionArr[i] as? KeyValue{
                if dataDic[AppKey.quantityClosed] as? Int != 0{
                    symbolArrClose.append(dataDic["symbol"] as! String)
                }
            }
        }
        UIView.performWithoutAnimation {
            tblClosePosition.reloadData()
        }
        
        
        for i in 0 ..< accountPositionArr.count {
            if let  dataDic = accountPositionArr[i] as? KeyValue{
                
                if dataDic[AppKey.quantityClosed] as? Int != 0{
                    arrClosePosition.append(ClosePosition.init(dataDic)!)
                    //symbolArrClose.append(dataDic["symbol"] as! String)
                    WebSocketManager.sharedInstance.subscribeCompany(symbol: dataDic["symbol"] as! String)
                }
                print(symbolArrClose)
                
            }
        }
        
    }
    
    
    
    
    
    
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            quoteDic = companyData
            
            //check for update model value
            if symbolArrClose.containsObject(companyData["sym"] as! String){
                
                let indexes = symbolArrClose.enumerated().filter {
                    $0.element == (companyData["sym"] as! String)
                    
                    }.map{$0.offset}
                print(indexes)
                for i in 0..<indexes.count {
                    if  arrClosePosition.count > 0 {
                        let model = arrClosePosition[indexes[i]]
                        self.updateQuotesValuesInModel(model, companyData: companyData, index: indexes[i] )
                        
                    }
                }
               
             
            }
            
        }
    }
    
    
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadTradeData(quoteDic, tradeDic)
        }
        
    }
    
    
    
    //MARK: Load both Quote and Trade Value
    private func loadTradeData(_ quoteDic1:KeyValue, _ tradeDic1: KeyValue){
        //print(quoteDic,tradeDic)
        let dataDic = quoteDic1.merged(with: tradeDic1)
        
        // this is for update stock details value in real time
        if symbolArrClose.containsObject(dataDic["sym"] as! String){
            
            let indexes = symbolArrClose.enumerated().filter {
                $0.element == (dataDic["sym"] as! String)
                }.map{$0.offset}
            
            print(indexes)
            
            for i in 0..<indexes.count {
                if arrClosePosition.count > 0 {
                    let model = arrClosePosition[indexes[i]]
                    self.updateTradeValuesInModel(model, companyData: dataDic, index: indexes[i])//tradeDic1
                }
                
                
            }
            
            /*let index = symbolArrClose.index{$0 == dataDic["sym"] as! String} // 0
            print(index,symbolArrClose.count)
            if arrClosePosition.count > 0{
                let model = arrClosePosition[index!]
                self.updateTradeValuesInModel(model, companyData: dataDic)//tradeDic1
            }*/
            
        }else{
            symbolArrClose.append(dataDic["sym"] as! String)
            arrClosePosition.append(ClosePosition.init(dataDic)!)
            UIView.performWithoutAnimation {
                tblClosePosition.reloadData()
            }
            
        }
        
        
        
    }
    
    
    //MARK: update quote value when data change
    func updateQuotesValuesInModel(_ model : ClosePosition,companyData:KeyValue , index:Int){
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        UIView.performWithoutAnimation {
            let indexPath = IndexPath(item: index, section: 0)
            tblClosePosition.reloadRows(at: [indexPath], with: .none)
            //tblClosePosition.reloadData()
        }
        
    }
    //MARK: update Trade value when data change
    func updateTradeValuesInModel(_ model : ClosePosition, companyData : KeyValue , index:Int){
        
        model.currentPrice =  companyData[AppKey.lastTradePrice] as? Double ?? 0.00
        //model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        //model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.volume = companyData[AppKey.tradeVolume] as? Int ?? 0
        model.stocktType = companyData[AppKey.orderType] as? String ?? "N/A"
        
        UIView.performWithoutAnimation {
            let indexPath = IndexPath(item: index, section: 0)
            tblClosePosition.reloadRows(at: [indexPath], with: .none)
            //tblClosePosition.reloadData()
        }
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ClosePositionVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("count = ", arrClosePosition.count,symbolArrClose.count)
        return symbolArrClose.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PositionCell
        cell.selectionStyle = .none
        if arrClosePosition[indexPath.row].companySym == "N/A"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1")
            cell?.backgroundColor = .clear
            cell?.selectionStyle = .none
            return cell!
            
        }
        cell.lblSymbol.text = arrClosePosition[indexPath.row].companySym
        cell.lblShares.text = String(describing: Int(arrClosePosition[indexPath.row].shares!))
        cell.lblEntryDate.text = String(describing:arrClosePosition[indexPath.row].enrtyDate!)
        cell.lblEntryPrice.text = "$\(String(describing:arrClosePosition[indexPath.row].entryPrice!))"
        cell.lblCurrentPrice.text = "$\(String(describing:arrClosePosition[indexPath.row].closePrice!))"
        cell.lblEntryTime.text = String(describing:arrClosePosition[indexPath.row].enrtyTime!) 
        
        //print(accountPositionDic)//print global assign value of position
        if let position = accountPositionDic[arrClosePosition[indexPath.row].companySym!] as? String{
            print(position)
            if position == "Long"{
                
                let plvalue : Double = (arrClosePosition[indexPath.row].closePrice! - arrClosePosition[indexPath.row].entryPrice!)*arrClosePosition[indexPath.row].shares!
                cell.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell.lblPL.textColor = AppColor.gainColor
                }else{
                    cell.lblPL.textColor = AppColor.lossColor
                }
                
                
            }else if position == "Short"{
                let plvalue : Double = (arrClosePosition[indexPath.row].closePrice! - arrClosePosition[indexPath.row].entryPrice!)*arrClosePosition[indexPath.row].shares!
                cell.lblPL.text = "$\(String(describing:plvalue.rounded(toPlaces: 2)))"
                if plvalue > 0{
                    cell.lblPL.textColor = AppColor.gainColor
                }else{
                    cell.lblPL.textColor = AppColor.lossColor
                }
            }
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        
    }
    
    
}
