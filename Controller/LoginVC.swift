 //
//  LoginVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/10/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import Starscream
import LinkKit
import Crashlytics


// enum for Sign in screen textfield
fileprivate enum SignInField: Int {
    case email = 100
    case password
}

class LoginVC: BaseVC {
    
    var keyboardHeight : CGFloat = 0
  
    var socket = WebSocket(url: URL(string: "wss://api.turnkeymobile.solutions/apiv1?token=02108AQAAAAQAAAABAAAABAAAAAQAAABRCR1aAgAAAAYAAABhcHBzMDIDAAAADgAAADIwMy4xMjkuMjIwLjc2BgAAAA0AAAAxNTExODUyMzY5Njk5BwAAAAYAAAB0emRlbW8IAAAABAAAAO0AAAAJAAAACAAAAEcoAAAAAAAACgAAAAUAAABkZW1vMgsAAAAIAAAAbAkAAAAAAAAFAAAAIAAAAKJV7rJpbusjJUeMi9wwFJMdM3E1JX1gDxXGancgO2AZ%2F%2F%2F%2F%2Fw%3D%3D")!)
    
    //google key AIzaSyCdB8S4vD3UpP3c2IR_CGU1d5pCG5DWmXw
    // var socket = WebSocket(url: URL(string: "wss://echo.websocket.org")!)
   

    //button
    @IBOutlet weak var btnNext: CustomButton!
    
    //textFields
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var abc : String!
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "LoginVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        
        // nextButton initian frame
        setnextButtonFrame()
        
        //self.txtEmail.becomeFirstResponder()
        self.txtEmail.autocorrectionType = UITextAutocorrectionType.no
        self.txtPassword.autocorrectionType = UITextAutocorrectionType.no
        self.txtEmail.autocapitalizationType = .allCharacters
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        
        // work on next button validation
        btnNext.isUserInteractionEnabled = false
        btnNext.alpha = 0.5
        txtEmail.addTarget(self, action: #selector(LoginVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtPassword.addTarget(self, action: #selector(LoginVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        
        //Notification Center for
        NotificationCenter.default.addObserver(self, selector: #selector(self.socketConnection), name: NSNotification.Name(rawValue: SocketConstants.socketConnectedNotification), object: nil)
        
        
        // Do any additional setup after loading the view.
    
        
        
    }
    
    
    deinit {
        socket.disconnect(forceTimeout: 0)
        socket.delegate = nil
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        // Do something here
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
             keyboardHeight = keyboardRectangle.height
        }
        self.buttonAnimation()
    }
    
    @objc func keyboardDidHide(_ notification: Notification) {
        // Do something here
        self.buttonTopriviousPosition()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //method call when soket get connected
    @objc func socketConnection(){
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: SocketConstants.socketConnectedNotification), object: nil)
            KAppDelegate.goToRootViewAfterLogin()
        })
    }
    
    private func setnextButtonFrame(){
        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
    }
    
    
    //MARK:  nextButtonAnimation
    fileprivate func buttonAnimation(){
    
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - self.btnNext.frame.size.width - 17, y: self.view.frame.size.height - self.keyboardHeight - 50, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
        }, completion: { (finished) -> Void in
            
        })
    
    }
    
    fileprivate func buttonTopriviousPosition(){
        
        
         UIView.animate(withDuration: 0.1,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.btnNext.frame = CGRect.init(x: self.view.frame.size.width - 50 - 17, y: self.view.frame.size.height - 50 - 19, width: self.btnNext.frame.size.width, height: self.btnNext.frame.size.height)
                    }, completion: { (finished) -> Void in
                        
                        
                    })
    
      
    }
    
    //MARK:  button actions
    @IBAction func backAction(_ sender: Any) {
        //self.navigationController?.popToViewController(firstVC(), animated: true)
        KAppDelegate.firstVC()
    }
    
    @IBAction func btnEyeOnOffAction(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            txtPassword.isSecureTextEntry = true
        }else{
            sender.isSelected = true
            txtPassword.isSecureTextEntry = false
        }
    }
    @IBAction func forgotButtonAction(_ sender: Any) {
        
        
        
        let alertController = UIAlertController(title: "Please Fill Your User Name!", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "OK", style: .destructive, handler: {
            alert -> Void in
            
            let userNameTextField = alertController.textFields![0] as UITextField
            self.forgetPasswordAPI(userNameTextField.text!)
            
            print("firstName \(String(describing: userNameTextField.text))")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter User Name"
            textField.autocapitalizationType = UITextAutocapitalizationType.allCharacters//set key board to capital laters
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK:  API Methods
    private func forgetPasswordAPI(_ userName: String) {
        //http://tradezero.co:2701/api/SignProcess/ForgotPassword?username=TDDFDG

        
        let url  = "\(API.signUp)/ForgotPassword?username=\(userName)"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                UIAlertController.show(self, "Message!", response?["Message"] as! String)
            default:
                
                self.errorHandling(response, status, error)

            }
        }
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        generateAccessToken()
        
        
        //self._signIn()
        
    }
    
    // MARK:  Private Methods
    private func _signIn() {
        guard let phoneField = txtEmail , _isValidRequest(field: .email, textField: txtEmail) else {
            return
        }
        guard let passwordField = txtPassword , _isValidRequest(field: .password, textField: txtPassword) else {
            return
        }
        _signInAPI(phoneField, passwordField)
    }
    
    
    private func _isValidRequest(field: SignInField, textField: UITextField) -> Bool {
        switch field {
        case .email:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterEmail)
                return false
            }
            if !(textField.text?.isValidEmail())! {
                UIAlertController.show(self, KAlertTitle, KEnterValidEmail)
                return false
            }
            return true
            
        case .password:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterPassword)
                return false
            }
            if textField.text?.length() < KPasswordMinLength{
                UIAlertController.show(self, KAlertTitle, KMinPasswordLength)
                return false
            }
            if textField.text?.length() > KPasswordMaxLength{
                UIAlertController.show(self, KAlertTitle, KMaxPasswordLength)
                return false
            }
            return true
        }
    }
    
    // MARK:  API Method
    private func _signInAPI(_ emailField: UITextField, _ passwordField: UITextField) {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        self.navigationController?.pushViewController(signUpNameVC(), animated: true)
        //ActivityIndicator.shared.show(self.view)
        
    }
    
    func generateAccessToken(){
        
        userAccountId = txtEmail.text! //assign account id globaly
        ActivityIndicator.shared.show(self.view)//APPS02 //718testing
        BusinessLayer.accessToken(txtEmail.text!, txtPassword.text!) { (response, status, error) in
            switch status {
            case .success:
                print(response!)
                let tokenStr = response!["token"] as! String
                ActivityIndicator.shared.show(self.view)
                WebSocketManager.sharedInstance.connect(with: URL(string:API.WebsocketUrl+tokenStr)!)
                //WebSocketManager.sharedInstance.connect(with: URL(string:"wss://api.turnkeymobile.solutions/apiv1?token=02108AQAAAAQAAAABAAAABAAAAAQAAAD1mgJbAgAAAAYAAABhcHBzMDIDAAAADwAAADIwMy4xOTAuMTU0LjE0NgYAAAANAAAAMTUyNjg5NzM5NzQxOAcAAAAGAAAAdHpkZW1vCAAAAAQAAADtAAAACQAAAAgAAABHKAAAAAAAAAoAAAAFAAAAZGVtbzILAAAACAAAAGwJAAAAAAAABQAAACAAAAA94q0KImw86Z8HUow6LFNe%2B4WkxGesvukpAK%2FVcn71Tv%2F%2F%2F%2F8%3D")!)
                
                //ActivityIndicator.shared.hide()
                
                
           default:
                ActivityIndicator.shared.hide()
                UIAlertController.show(self, AppTitle.appTital, "Please fill valid login details!")
                //self.errorHandling(response, status, error)
            }
        }
      
    }

}
 

 

extension LoginVC {
    
    // MARK:  UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.buttonAnimation()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.buttonTopriviousPosition()
        return true
    }
    
    @objc func textFieldDidChange(_ textField : UITextField){
        if txtEmail.text?.length() == 0 || txtPassword.text?.length() == 0 {
            btnNext.isUserInteractionEnabled = false
            btnNext.alpha = 0.5
        }else{
            btnNext.isUserInteractionEnabled = true
            btnNext.alpha = 1.0
        }
    }
    
      func textFieldDidEndEditing(_ textField: UITextField) {
        

    }
    
}
 
 // MARK: - WebSocketDelegate
 extension LoginVC : WebSocketAdvancedDelegate {
    
    func websocketDidConnect(socket: WebSocket) {
        print("web socket is connected")
       if socket.isConnected{
            print("conected")
        

        var dic = [String:AnyObject]()
        dic["rt"] = "L1Subscribe" as AnyObject
        dic["sym"] = "AAPL" as AnyObject
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted);
            if let string = String(data: jsonData, encoding: String.Encoding.utf8){
                print(string)
                socket.write(string: string)
            } else {
                print("Couldn't create json string");
            }
        } catch let error {
            print("Couldn't create json data: \(error)");
        }
        
        
        
        }
    }
    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
        print("websocketDidDisconnect")
    }
    func websocketDidReceiveMessage(socket: WebSocket, text: String, response: WebSocket.WSResponse) {
        print("websocketDidReceiveMessage",text)
        
        if let data = text.data(using: String.Encoding.utf8){
            do {
                let JSON : [String:AnyObject] = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                print("We've successfully parsed the message into a Dictionary! Yay!\n\(JSON)")
                let message : String = JSON["data"] as? String ?? "Not Found"
                
                let alert = UIAlertController(title: AppTitle.appTital, message: "\(message)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
               // self.present(alert, animated: true, completion: nil)
            } catch let error{
                print("Error parsing json: \(error)")
            }
        }
        
        print("First frame for this message arrived on \(response.firstFrame)")
    }
    func websocketDidReceiveData(socket: WebSocket, data: Data, response: WebSocket.WSResponse) {
        if let returnData = String(data: data, encoding: .utf8) {
            print(returnData)
        }
        print("websocketDidReceiveData")
    }
    
    func websocketHttpUpgrade(socket: WebSocket, request: String) {
        print("the http request was sent we can check the raw http if we need to")
    }
    func websocketHttpUpgrade(socket: WebSocket, response: String) {
        print("the http response has returned.")
    }
    

    
 }
 
 
