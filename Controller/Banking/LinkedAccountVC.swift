//
//  LinkedAccountVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit
import LinkKit

class LinkedAccountVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "LinkedAccountVC"
    }
    
    @IBOutlet weak var tblLinkedAccount: UITableView!
    
    var addedBankList = [KeyValue]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        let allVC = self.navigationController?.viewControllers
        
        if  let transferToBankVC = allVC![allVC!.count - 2] as? TransferToBankVC {
            transferToBankVC.banklistArr = self.addedBankList
            self.navigationController!.popToViewController(transferToBankVC, animated: true)
        }

    }
    
    @IBAction func LinkedAnotherBankAccount(_ sender: Any) {
        
        //Plaid integration
        
        
        // With custom configuration
        
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        // With shared configuration from Info.plist
        let linkViewDelegate = self
        let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            linkViewController.modalPresentationStyle = .formSheet;
        }
        present(linkViewController, animated: true)
        // <!-- SMARTDOWN_PRESENT_SHARED -->
        
    }
   

}
extension LinkedAccountVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return addedBankList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LinkedAccountCell
        cell.selectionStyle = .none
        
        cell.lblBankName.text = addedBankList[indexPath.row]["Bankname"] as? String
        cell.lblAccountId.text = addedBankList[indexPath.row]["account"] as? String
        
        cell.btnSelected.isSelected = false
        if let defaultValue = addedBankList[indexPath.row]["default1"] as? Int {
            if defaultValue == 1{
                cell.btnSelected.isSelected = true
            }else{
                cell.btnSelected.isSelected = false
            }
        }
        
        cell.btnSelected.addTarget(self, action: #selector(LinkedAccountVC.changeBank), for: .touchUpInside)

        cell.btnSelected.tag = indexPath.row
        
        return cell
    }
    
    @objc func changeBank(sender: UIButton){
        let buttonTag = sender.tag
        self.changeBankForTransition(addedBankList[buttonTag]["Customer_id"] as! String)
    }
    
    
    // Bank Account Authentication
    // MARK:  API Methods
    private func getAccessTokenAPI(_ publicToken: String,plaidAccountId: String) {
        
       
        //http://tradezero.co:2701/api/SignProcess/GetCustomerToken?public_token=public-sandbox-3cf817af-3458-4248-bd62-0f12fc18508e&plaidaccount_id=yLJXxJbdg1Upo6z4dnxZCxWnpK7RpockXgep9&Accountid=10609'
        
        
//        guard let accountId = signUpDictionary[AppKey.accountId] else{
//            return
//        }
        
        //signUpDictionary[AppKey.plaidAccountId] = accountId// save plaid account id
        
        let url  = "\(API.signUp)/GetCustomerToken?public_token=\(publicToken)&plaidaccount_id=\(plaidAccountId)&Accountid=9690"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                ActivityIndicator.shared.hide()
                
                self.getRegisteredBanks()
                
                //self.getBankToken(plaidAccessToken: accessToken as! String, plaidAccountId: accountId)
                
            default:
                self.errorHandling(response, status, error)
            }
        }
    }
    
    
    private func getRegisteredBanks(){
        
        //http://tradezero.co:2701/api/Transactions/LinkupBanks?Accountid=10609
        
        
        let url  = "http://tradezero.co:2701/api/Transactions/LinkupBanks?Accountid=9690"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                if let bankList = response!["bankList"] as? [KeyValue]{
                    self.addedBankList = bankList
                    self.tblLinkedAccount.reloadData()
                }
                ActivityIndicator.shared.hide()
                
            default:
                
                self.errorHandling(response, status, error)
                
            }
        }
        
        
    }
    
    
    private func changeBankForTransition(_ CustomerId : String){
        
        //http://tradezero.co:2701/api/Transactions/checkbank?Accountid=9690&Customer_id=cus_CJh11x9B9j4uAE&default1=true
        
        let url  = "http://tradezero.co:2701/api/Transactions/checkbank?Accountid=9690&Customer_id=\(CustomerId)&default1=true"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                if let bankList = response!["bankList"] as? [KeyValue]{
                    self.addedBankList = bankList
                    self.tblLinkedAccount.reloadData()
                }
                ActivityIndicator.shared.hide()
                
            default:
                
                self.errorHandling(response, status, error)
                
            }
        }
        
        
    }
    
    
}

extension LinkedAccountVC : PLKPlaidLinkViewDelegate{
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didSucceedWithPublicToken publicToken: String, metadata: [String : Any]?) {
        dismiss(animated: true) {
            // Handle success, e.g. by storing publicToken with your service
            NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
            
            self.getAccessTokenAPI(publicToken, plaidAccountId: metadata!["account_id"] as! String)
            //self.handleSuccessWithToken(publicToken, metadata: metadata)
        }
    }
    
    func linkViewController(_ linkViewController: PLKPlaidLinkViewController, didExitWithError error: Error?, metadata: [String : Any]?) {
        dismiss(animated: true) {
            if let error = error {
                NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
                //self.handleError(error, metadata: metadata)
            }
            else {
                NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
                //self.handleExitWithMetadata(metadata)
            }
        }
    }
}
