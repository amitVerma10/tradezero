//
//  BankingVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/21/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import PageMenu

class BankingVC:  BaseVC {
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var lblBuyingPower: UILabel!
    @IBOutlet weak var lblPortfolioValue: UILabel!
    
    @IBOutlet weak var segmentView: UIView!
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "BankingVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        let storyboard = UIStoryboard(name: "Banking", bundle: nil)
        let channelVC = storyboard.instantiateViewController(withIdentifier: "BankingTransferVC") as! BankingTransferVC
        let channelVC1 = storyboard.instantiateViewController(withIdentifier: "BankingBuySellVC") as! BankingBuySellVC
       
        
        channelVC.title = "Transfer"
        channelVC1.title = "Buy & Sell"

        controllerArray.append(channelVC)
        controllerArray.append(channelVC1)
      
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor(red: 13.0/255.0, green: 11.0/255.0, blue: 32.0/255.0, alpha: 1.0)),
            .viewBackgroundColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 4.0/255.0, green: 5.0/255.0, blue: 36.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)),
            .menuHeight(40.0),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .menuItemFont(UIFont(name: "Raleway-SemiBold", size: 17.0)!),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorRoundEdges(false),
            .selectionIndicatorHeight(2.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 320, width: self.view.frame.size.width, height: self.view.frame.size.height - 320 ), pageMenuOptions: parameters)
        
        self.view.addSubview(pageMenu!.view)


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let bpValue = signUpDictionary[AppKey.buyingPower] as? Double{
            lblBuyingPower.text = "$ \(String(describing: bpValue))"
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func transferToTradeZeroAction(_ sender: Any) {
        self.navigationController?.pushViewController(transferToBankVC(), animated: true)
    }
    
    @IBAction func transferToBankAction(_ sender: Any) {
//        self.navigationController?.pushViewController(transferToBankVC(), animated: true)
    }
    
    @IBAction func linkedAccountAction(_ sender: Any) {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

