//
//  TransferToBankVC.swift
//  TradeZero
//
//  Created by Amit Verma on 12/1/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class TransferToBankVC: BaseVC {
    
    @IBOutlet weak var lblBuyingPower: UILabel!
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "TransferToBankVC"
    }

    @IBOutlet weak var navigationBar: UINavigationItem!
    var banklistArr = [KeyValue]()
    
    @IBOutlet weak var txtAmmount: UITextField!
    @IBOutlet weak var lblbankName: UILabel!
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var imgBank: UIImageView!
    var stringCoustumerId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.title = "Transfer To TradeZero"
        self.getRegisteredBanks()//get All registered banks in a list formate

        // Do any additional setup after loading the view.
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let bpValue = signUpDictionary[AppKey.buyingPower] as? Double{
            lblBuyingPower.text = "$ \(String(describing: bpValue))"
        }
        if banklistArr.count > 0{
            for i in 0..<banklistArr.count {
                if let defaultValue = self.banklistArr[i]["default1"] as? Int{
                    if defaultValue == 1{
                        self.loadBankList(self.banklistArr[i])
                    }
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeBankAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: AppKey.banking, bundle:nil)
        if let linkedAccountVC = storyboard .instantiateViewController(withIdentifier: "LinkedAccountVC") as? LinkedAccountVC {
             linkedAccountVC.addedBankList = self.banklistArr
              self.navigationController?.pushViewController(linkedAccountVC, animated: true)
            
           }
    }
    
    
    private func getRegisteredBanks(){
        
       //http://tradezero.co:2701/api/Transactions/LinkupBanks?Accountid=10609
        
        
        let url  = "http://tradezero.co:2701/api/Transactions/LinkupBanks?Accountid=9690"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                if let bankList = response!["bankList"] as? [KeyValue]{
                    self.banklistArr = bankList
                }
                ActivityIndicator.shared.hide()
                
                for i in 0..<self.banklistArr.count  {
                    if let defaultValue = self.banklistArr[i]["default1"] as? Int{
                        if defaultValue == 1{
                            self.loadBankList(self.banklistArr[i])
                        }
                    }
                }
                
            default:
                
              self.errorHandling(response, status, error)
                  
                
            }
        }
        
        
    }
    
    private func loadBankList(_ bankdata : KeyValue){
        lblbankName.text = bankdata["Bankname"] as? String
        lblAccountNumber.text = bankdata["account"] as? String
        stringCoustumerId = (bankdata["Customer_id"] as? String)!
    }
    
    @IBAction func amountBtnAction(_ sender: UIButton) {
        if sender.tag == 50{
            txtAmmount.text = "50"
        }
        else if sender.tag == 100{
            txtAmmount.text = "100"
        }
        else if sender.tag == 200{
            txtAmmount.text = "200"
        }
    }
    
    
    
    
    
    
    @IBAction func proceedTransiction(_ sender: Any) {
        
        if txtAmmount.text?.characters.count > 0{
            
            //http://tradezero.co:2701/api/SignProcess/TransAmount?Accountid=10632&Customer_Id=cus_CHKeEp3ELqtYiL&Amount=7
            
            
            
            let url  = "http://tradezero.co:2701/api/SignProcess/TransAmount?Accountid=9690&Customer_Id=\(stringCoustumerId)&Amount=\(String(describing: txtAmmount.text!))"
            
            ActivityIndicator.shared.show(self.view)
            BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
                switch status {
                case .success:
                    
                    ActivityIndicator.shared.hide()
                    UIAlertController.show(self, AppTitle.appTital, response?["Message"] as? String)
                    
                    
                default:
                    
                    self.errorHandling(response, status, error)
                    
                    
                }
            }
            
        }else{
            
            
            let banner = StatusBarNotificationBanner(title: "Please Fill  Amount for transition", style: .danger)
            banner.show()
        }
        
       
        
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

}
