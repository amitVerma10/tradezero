//
//  BankingTransferVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class BankingTransferVC: BaseVC {
    
    
    @IBOutlet weak var tblTran: UITableView!
    var tansitionListArr = [KeyValue]()

    override func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getRegisteredBanks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getRegisteredBanks(){
        
        //http://tradezero.co:2701/api/Transactions/LinkupBanks?Accountid=10609
        
        
        let url  = "http://tradezero.co:2701/api/Transactions/Transactionslist?Accountid=9690"
        
        ActivityIndicator.shared.show(self.view)
        BusinessLayer.sendRequest(urlStr: url) { (response, status, error) in
            switch status {
            case .success:
                if let List = response!["transactionlist"] as? [KeyValue]{
                    self.tansitionListArr = List
                }
                self.tblTran.reloadData()
                ActivityIndicator.shared.hide()
                
                
            default:
                
                self.errorHandling(response, status, error)
                
            }
        }
        
        
    }
    
    
    

}
extension BankingTransferVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tansitionListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HistoryCell
        cell.selectionStyle = .none
        
        cell.lblOrderName.text = "$ \(String(describing: (tansitionListArr[indexPath.row]["Amount"] as? Int)!/100))"
        cell.lblOrderDate.text = tansitionListArr[indexPath.row]["created"] as? String
        cell.lblOrderStatus.text = tansitionListArr[indexPath.row]["status"] as? String
        
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}
