//
//  WatchListVC.swift
//  TradeZero
//
//  Created by Amit Verma on 2/26/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class WatchListVC: BaseVC {
    
    let date = Date()
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "WatchListVC"
    }

    @IBOutlet weak var headerImgHeight: NSLayoutConstraint!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    fileprivate var symbolArrTrade = [String]()
    @IBOutlet weak var tblWatchList: UITableView!
    
    var isFirstTime: Bool = true
    
    var arrWatchListDetails = [WatchList]()
    var indexPathValue = IndexPath()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeInHomeVC), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteInHomeVC), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.accountDetailsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.accountDetailsResponse), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.createWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.createWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFromWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil)
        
        // give header image value
        headerImgHeight.constant = 0.0
        titleHeight.constant = 40.0
        self.tblWatchList.tableHeaderView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 150)
        self.tblWatchList.allowsMultipleSelectionDuringEditing = false
        
    }
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewControllerName = ""//for default
        
        super.viewWillAppear(true)
        UserDefaults.standard.set(true, forKey: "isHomeView")
        
        symbolArrTrade = [String]()
        arrWatchListDetails = [WatchList]()
        
       UIView.performWithoutAnimation {
         tblWatchList.reloadData()
       }
        
        
        WebSocketManager.sharedInstance.accountDetailRequrst(accountId: userAccountId)//APPS02
        
        //ActivityIndicator.shared.show(self.view)
        
        
        
      
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        isFirstTime = true
        //Unsubscribe All Symbol
        for i in 0 ..< symbolArrTrade.count {
            WebSocketManager.sharedInstance.unSubscribeCompany(symbol: symbolArrTrade[i])
        }
    }
    
    //when data is not loaded
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
       
        
        for i in 0 ..< publicWatchListArr.count {
            if let  symbol = publicWatchListArr[i] as? String{
                
                //subscribe of Company data from websocket
                WebSocketManager.sharedInstance.subscribeCompany(symbol:symbol)
                
            }
        }
        
    }
    
    @objc func removeFromWatchList(_ notification: NSNotification){
        
        
        
    }
    
    
    @objc func accountDetailsData(_ notification : NSNotification){
        
        //Create a watchList
        WebSocketManager.sharedInstance.createWatchList(name: userAccountId)
        
        
        //handel account data
        guard let accountDic = notification.userInfo!["data"] as? KeyValue else{
            return
        }
        signUpDictionary[AppKey.buyingPower] = accountDic[AppKey.buyingPower] as! Double
        
        guard let accountArr = accountDic["positions"] as? [KeyValue],accountArr.count > 0 else{
            return
        }
        //assign position value globaly
        accountPositionArr = accountArr
        
        guard let position = accountDic["positions"] as? [KeyValue],accountArr.count > 0 else{
            return
            
        }
        //assign value globaly
        for i in 0 ..< accountArr.count {
            if let  dataDic = position[i] as? KeyValue{
                accountPositionDic[dataDic["symbol"] as! String] = dataDic["type"] as? String
                accountOpenQuantityDic[dataDic["symbol"] as! String] = dataDic["quantityOpen"] as? Double
                accountEntryPriceDic[dataDic["symbol"] as! String] = dataDic["entryPriceCents"] as? Double
            }
        }
        
        
    }
    
    
    
    @objc func createWatchList(_ notification : NSNotification){
        //retrieve a watchList
        WebSocketManager.sharedInstance.retrieveWatchList(name: userAccountId)
        NotificationCenter.default.addObserver(self, selector: #selector(self.retrieveWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.retrieveWatchList), object: nil)
        
    }
    
    @objc func retrieveWatchList(_ notification : NSNotification){
        
        if isFirstTime{
            isFirstTime = false
            guard let watchListArr = notification.userInfo!["symbols"] as? [String]else{
                return
            }
            publicWatchListArr = watchListArr
            print(watchListArr)
            
            //ActivityIndicator.shared.show(self.view)
            
            for i in 0 ..< publicWatchListArr.count {
                if let  symbol = publicWatchListArr[i] as? String{
                    
                    //subscribe of Company data from websocket
                    WebSocketManager.sharedInstance.subscribeCompany(symbol:symbol)
                    
                }
            }
        }
        
        
        
        if publicWatchListArr.count == 0{
            headerImgHeight.constant = 297// give header image value
            titleHeight.constant = 40.0
            self.tblWatchList.tableHeaderView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 450)
            tblWatchList.reloadData()
        }else{
            
            headerImgHeight.constant = 0.0
            titleHeight.constant = 40.0
            self.tblWatchList.tableHeaderView?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 150)
            self.tblWatchList.allowsMultipleSelectionDuringEditing = false
        }

    }
    
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            quoteDic = companyData
            //check for update model value
            
                
              if symbolArrTrade.containsObject(companyData["sym"] as! String){
                
                let index = symbolArrTrade.index{$0 == companyData["sym"] as! String}
                if arrWatchListDetails.count > 0 {
                    let model = arrWatchListDetails[index!]
                    self.updateQuotesValuesInModel(model, companyData: companyData , index: index! )
                }
            }
          
        }
    }
    
    
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadTradeData(quoteDic, tradeDic)
        }
        
    }
    
    
    //MARK: Load both Quote and Trade Value
    private func loadTradeData(_ quoteDic1:KeyValue, _ tradeDic1: KeyValue){
        //print(quoteDic,tradeDic)
        let dataDic = quoteDic1.merged(with: tradeDic1)
        
        if publicWatchListArr.containsObject(dataDic["sym"] as! String){
            // this is for update stock details value in real time
            if symbolArrTrade.containsObject(dataDic["sym"] as! String){
                let index = symbolArrTrade.index{$0 == dataDic["sym"] as! String} // 0
                if arrWatchListDetails.count > 0{
                    let model = arrWatchListDetails[index!]
                    self.updateTradeValuesInModel(model, companyData: dataDic, index: index!)//tradeDic1
                }
                
            }else{
                
                symbolArrTrade.append(dataDic["sym"] as! String)
                arrWatchListDetails.append(WatchList.init(dataDic)!)

                
                if publicWatchListArr.count == symbolArrTrade.count{
                    ActivityIndicator.shared.hide()
                    UIView.performWithoutAnimation {
                        tblWatchList.reloadData()
                    }
                }
            }
            
      }
        
        
        
        
    }
    
    
    //MARK: update quote value when data change
    func updateQuotesValuesInModel(_ model : WatchList,companyData:KeyValue , index : Int){
        model.bid = companyData[AppKey.quoteBestBid] as? Double ?? 0.0
        model.ask = companyData[AppKey.quoteBestAsk] as? Double ?? 0.0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        
        if publicWatchListArr.count == symbolArrTrade.count{
            ActivityIndicator.shared.hide()
            UIView.performWithoutAnimation {
                let indexPath = IndexPath(item: index, section: 0)
                self.updatePerticularCell(indexPath: indexPath)
            }
      }
        
        
        
    }
    //MARK: update Trade value when data change
    func updateTradeValuesInModel(_ model : WatchList, companyData : KeyValue , index : Int){
        
        model.currentPrice = date.isBetweenTwoTime() ? companyData[AppKey.lastTradePrice] as? Double ?? 0.00 : companyData[AppKey.lastTradePrice] as? Double ?? 0.00
        model.isUp = calculateGainOrLoss(companyData)
        model.percentValue = calculateGainOrLossValue(companyData)
        model.time = companyData[AppKey.orderCreated] as? Int ?? 0
        model.hour = companyData[AppKey.quoteHour] as? Int ?? 0
        model.minut = companyData[AppKey.quoteMinut] as? Int ?? 0
        model.volume = companyData[AppKey.tradeVolume] as? Int ?? 0
        model.stocktType = companyData[AppKey.orderType] as? String ?? "N/A"
        
        if publicWatchListArr.count == symbolArrTrade.count{
            ActivityIndicator.shared.hide()
            UIView.performWithoutAnimation {
                let indexPath = IndexPath(item: index, section: 0)
                self.updatePerticularCell(indexPath: indexPath)
            }
        }
    }
    
    private func updatePerticularCell(indexPath : IndexPath){
        
        let cell = tblWatchList.cellForRow(at: indexPath) as? HomeCell
        
        cell?.lblSymbol.text = arrWatchListDetails[indexPath.row].companySym
        
        cell?.lblCurrentPrice.text = "$\(String(describing : arrWatchListDetails[indexPath.row].currentPrice!))"
        cell?.lblPercentage.text = arrWatchListDetails[indexPath.row].percentValue
        
        
        var hourStr = String(describing: arrWatchListDetails[indexPath.row].hour!)
        if hourStr.characters.count == 1{
            hourStr = "0\(hourStr)"
        }
        var minutStr = String(describing: arrWatchListDetails[indexPath.row].minut!)
        if minutStr.characters.count == 1{
            minutStr = "0\(minutStr)"
        }
        
        cell?.lblTime.text = "\(hourStr):\(minutStr)"
        cell?.lblBid.text = "Bid : $\(String(describing: arrWatchListDetails[indexPath.row].bid!))"
        cell?.lblAsk.text = "Ask : $\(String(describing: arrWatchListDetails[indexPath.row].ask!))"
        cell?.lblVolume.text = "Volume : \(String(describing: arrWatchListDetails[indexPath.row].volume!))"
        
        
        if arrWatchListDetails[indexPath.row].isUp == true{
            cell?.imgUpDown?.image = #imageLiteral(resourceName: "up_arrow")
            cell?.lblPercentage.textColor = AppColor.gainColor
        }else{
            cell?.imgUpDown?.image = #imageLiteral(resourceName: "down_arrow")
            cell?.lblPercentage.textColor = AppColor.lossColor
        }
    }
    
    
    //calculate gain or loss of stock
    private func calculateGainOrLoss(_ tradeDic:KeyValue)->Bool
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return false
        }
        
        if Double(truncating: lastTrade as! NSNumber) >= Double(truncating: prevoiusClose as! NSNumber) {
            return true
        }else{
            return false
        }
    }
    
    //calculate gain or loss of stock
    private func calculateGainOrLossValue(_ tradeDic:KeyValue)->String
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return "0.0 (0%)"
        }
        let diffrence = (Double(truncating: lastTrade as! NSNumber) - Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        let percentage = (diffrence*100/Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        
        return "\(String(diffrence)) (\(String(percentage))%)"
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func searchAction(_ sender: Any) {
        self.navigationController?.pushViewController(searchCompanyVC(), animated: true)
    }
    
    

}


extension WatchListVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrWatchListDetails.count
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? HomeCell{
            cell.selectionStyle = .none
            cell.lblSymbol.text = arrWatchListDetails[indexPath.row].companySym
            
            cell.lblCurrentPrice.text = "$\(String(describing : arrWatchListDetails[indexPath.row].currentPrice!))"
            cell.lblPercentage.text = arrWatchListDetails[indexPath.row].percentValue
            
            var hourStr = String(describing: arrWatchListDetails[indexPath.row].hour!)
            if hourStr.characters.count == 1{
                hourStr = "0\(hourStr)"
            }
            var minutStr = String(describing: arrWatchListDetails[indexPath.row].minut!)
            if minutStr.characters.count == 1{
                minutStr = "0\(minutStr)"
            }
            
            cell.lblTime.text = "\(hourStr):\(minutStr)"
            cell.lblBid.text = "Bid : $\(String(describing: arrWatchListDetails[indexPath.row].bid!))"
            cell.lblAsk.text = "Ask : $\(String(describing: arrWatchListDetails[indexPath.row].ask!))"
            cell.lblVolume.text = "Volume : \(String(describing: arrWatchListDetails[indexPath.row].volume!))"
            
            
            if arrWatchListDetails[indexPath.row].isUp == true{
                cell.imgUpDown?.image = #imageLiteral(resourceName: "up_arrow")
                cell.lblPercentage.textColor = AppColor.gainColor
            }else{
                cell.imgUpDown?.image = #imageLiteral(resourceName: "down_arrow")
                cell.lblPercentage.textColor = AppColor.lossColor
            }
            
            return cell
        }
        return UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let dic:KeyValue = [ "cc" : "USD",
                             "e" : "NQNM",
                             "s" : arrWatchListDetails[indexPath.row].companySym!]
        let storyboard = UIStoryboard(name: AppKey.companyDetail, bundle:nil)
        if let companyVC = storyboard .instantiateViewController(withIdentifier: CompanyVC.identifier()) as? CompanyVC {
            CompanyVC.compDic = dic
            print(accountPositionDic,accountOpenQuantityDic,accountEntryPriceDic)//print global assign value of position
            
            CompanyVC.positionValue = "no position"// no position if not in stock for user
            if let position = accountPositionDic[arrWatchListDetails[indexPath.row].companySym!]{
                print(position)
                CompanyVC.positionValue = position
            }
            CompanyVC.entryPrice = 0.0 //predefine value  for entry price
            if let entryPrice = accountEntryPriceDic[arrWatchListDetails[indexPath.row].companySym!]{
                CompanyVC.entryPrice = (entryPrice/100).rounded(toPlaces: 2)
            }
            CompanyVC.quantityOpen = 0//predefine value  for open Quantity
            if let openQuantity = accountOpenQuantityDic[arrWatchListDetails[indexPath.row].companySym!] {
                CompanyVC.quantityOpen = openQuantity
            }
            
        }
        self.navigationController?.pushViewController(companyVC(), animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
         return "WATCHLIST"
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.color(4.0/255.0, 4.0/255.0, 36.0/255.0, 1.0)
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 5, width:
            tableView.bounds.size.width, height: 40))
        headerLabel.font = UIFont(name: "Raleway-Bold", size: 15)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(self.tblWatchList, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            
            let symbol = arrWatchListDetails[indexPath.row].companySym!
            let indexForPublicArr = publicWatchListArr.index{$0 == symbol}
            publicWatchListArr.remove(at: indexForPublicArr!)
            
            let indexForSymbolArr = symbolArrTrade.index{$0 == symbol}
            symbolArrTrade.remove(at: indexForSymbolArr!)
            
            let indexForLocal = arrWatchListDetails.index{$0.companySym == symbol}
            arrWatchListDetails.remove(at: indexForLocal!)
            tblWatchList.deleteRows(at: [indexPath], with: .none)
            
            WebSocketManager.sharedInstance.unSubscribeCompany(symbol: symbol)
            WebSocketManager.sharedInstance.removeFromWatchList(symbolName: symbol, name: userAccountId)
            
        default:
            break
        }
       
    }
    
    
    
}

