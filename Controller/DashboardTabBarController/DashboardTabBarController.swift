//
//  DashboardTabBarController.swift
//  TradeZero
//
//  Created by Amit Verma on 11/20/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class DashboardTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        UserDefaults.standard.set(false, forKey: "isStats")// set defaults flase for company detail

        super.viewWillAppear(animated)
        let item1 = companyDetailsVC()
        let item2 = statsVC()
        let item3 = newsVC()
        let item4 = infoVC()
        let icon1 = UITabBarItem(title: "Detail", image: #imageLiteral(resourceName: "detail_act"), selectedImage: #imageLiteral(resourceName: "detail_act-1"))
        let icon2 = UITabBarItem(title: "Stats", image:#imageLiteral(resourceName: "stats") , selectedImage: #imageLiteral(resourceName: "stats_act"))
        let icon3 = UITabBarItem(title: "News", image: #imageLiteral(resourceName: "news"), selectedImage: #imageLiteral(resourceName: "news_act"))
        let icon4 = UITabBarItem(title: "Info", image:#imageLiteral(resourceName: "info"), selectedImage: #imageLiteral(resourceName: "info_act"))
        item1.tabBarItem = icon1
        item2.tabBarItem = icon2
        item3.tabBarItem = icon3
        item4.tabBarItem = icon4
        let controllers = [item1,item2,item3]  //array of the root view controllers displayed by the tab bar interface
        self.viewControllers = controllers
    }
    
    //Delegate methods
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print("Should select viewController: \(viewController.title) ?")
        if viewController.title == "Stats"{
            UserDefaults.standard.set(true, forKey: "isStats")
        }else{
            UserDefaults.standard.set(false, forKey: "isStats")
        }
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
