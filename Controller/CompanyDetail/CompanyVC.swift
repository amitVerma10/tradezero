//
//  CompanyVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/20/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import SwiftChart

enum ChartTime : String{
    case oneDay         = "IntraDay"
    case oneMonth       = "oneMonth"
    case threeMonth     = "threeMonth"
    case sixMonth       = "sixMonth"
    case oneYear        = "oneYear"
    case max            = "max"
}



class CompanyVC: BaseVC ,ChartDelegate{
    
    let date = Date()
    
    static public var compDic = KeyValue()
    public var symbolStr = String()
    static public var positionValue = String()
    static public var quantityOpen : Double = 0//open quantity Value
    static public var entryPrice : Double = 0.0//entry price Value
    
    @IBOutlet weak var btnLocate: UIBarButtonItem!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblExchangeName_Sym: UILabel!
    @IBOutlet weak var lblBorrowStatus: UILabel!
    
    @IBOutlet weak var imgOneDay: UIImageView!
    @IBOutlet weak var imgOneMonth: UIImageView!
    @IBOutlet weak var imgThreeMonths: UIImageView!
    @IBOutlet weak var imgSixMonths: UIImageView!
    @IBOutlet weak var imgOneYear: UIImageView!
    @IBOutlet weak var imgMax: UIImageView!
    
    @IBOutlet weak var likeButton: UIBarButtonItem!
    
    
    //collectionview outlet
    @IBOutlet weak var collectionShares: UICollectionView!
    
    @IBOutlet weak var labelLeadingMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var chart: Chart!
    @IBOutlet var chartLandScape: Chart!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelLandScape: UILabel!
    
    @IBOutlet weak var lblPreviousClose: UILabel!
    @IBOutlet weak var imgGainLoss: UIImageView!
    @IBOutlet weak var lblGainLossPercentage: UILabel!
    
    @IBOutlet weak var lblCloseTime: UILabel!
    @IBOutlet weak var lblVolume: UILabel!
    
    @IBOutlet weak var btnSell: CustomButton!
    @IBOutlet weak var btnBuy: CustomButton!
    
    @IBOutlet var landScapeChartView: UIView!
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    fileprivate var isGain = Bool()
    fileprivate var collectionTitleArr = [String]()
    fileprivate var collectionValueArr = [Double]()
    
    
    var chartTime = ChartTime.oneDay
    
    fileprivate var lastOrientation:UIDeviceOrientation!

    fileprivate var labelLeadingMarginInitialConstant: CGFloat!
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "CompanyVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBuy.alpha = 0.2
        btnSell.alpha = 0.2
        btnBuy.isEnabled = false
        btnSell.isEnabled = false
        btnLocate.isEnabled = false
        likeButton.isEnabled = false
        
        inWatchList = false
        
        labelLeadingMarginInitialConstant = labelLeadingMarginConstraint.constant
        
        if let companyName = CompanyVC.compDic[AppKey.searchCompanyName] as? String{
            lblCompanyName.text = companyName
        }
        if let companyExchange = CompanyVC.compDic[AppKey.companyExchangeName] as? String{
            if let companySym = CompanyVC.compDic[AppKey.companySym] as? String{
                lblExchangeName_Sym.text = "\(String(describing: companyExchange)):\(String(describing: companySym))"
          }
        }
        
        //Call Get Equities of symbol array
        
        WebSocketManager.sharedInstance.getEquities(symArr: [CompanyVC.compDic[AppKey.companySym] as! String])
        
        
        
        
        
        //Notification Center for
         NotificationCenter.default.addObserver(self, selector: #selector(self.equitiesResponse(_:)), name: NSNotification.Name(rawValue: SocketConstants.getEquities), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.borrowStatus(_:)), name: NSNotification.Name(rawValue: SocketConstants.borrowStatus), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTrade), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuote), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.chartData(_:)), name: NSNotification.Name(rawValue: SocketConstants.chartDataRecived), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addtoWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.addToWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFromWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForegroundCompany), object: nil)
        
        
        
        //chart work
        imgOneDay.isHidden = true
        imgOneMonth.isHidden = true
        imgThreeMonths.isHidden = true
        imgSixMonths.isHidden = true
        imgOneYear.isHidden = false
        imgMax.isHidden = true
        
        chartTime = ChartTime.oneDay//set default chart time

        // Do any additional setup after loading the view.
        
        //device orientation work
        lastOrientation = UIDevice.current.orientation
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        //watchList work check symbol is available in watchlist
        if publicWatchListArr.containsObject(CompanyVC.compDic[AppKey.companySym] as! String){
            inWatchList = true
        }else{
            inWatchList = false
        }
        
        if inWatchList
        {
           likeButton.tintColor = AppColor.gainColor
            
        }else{
            likeButton.tintColor = .white
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        viewControllerName = ""//for default
        UserDefaults.standard.set(false, forKey: "isHomeView")//set for Notification handling
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //unsubscribe company
    }
    
    
    @objc func rotated(){
        
        
        let currentOrientation = UIDevice.current.orientation
        
        guard UIDeviceOrientationIsLandscape(currentOrientation) || UIDeviceOrientationIsPortrait(currentOrientation) else {   // we are only interested in Portrait and Landscape orientations
            return
        }
        
        guard currentOrientation != lastOrientation else { //remember the case of Portrait-FaceUp-Portrait? Here we make sure that in such cases we don't reload table view
            return
        }
        if UIDeviceOrientationIsPortrait(currentOrientation){
            landScapeChartView.removeFromSuperview()
            print("portrait")
        }else if UIDeviceOrientation.landscapeLeft == currentOrientation{
            
            landScapeChartView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)

            landScapeChartView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.addSubview(landScapeChartView)
            print("landscapeLeft")
        }
        else if UIDeviceOrientation.landscapeRight == currentOrientation {
            
            landScapeChartView.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            
            landScapeChartView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.addSubview(landScapeChartView)
            print("landscapeRight")
        }
        
        lastOrientation = currentOrientation
        
    }
    //MARK: getequities response
    @objc func equitiesResponse(_ notification: NSNotification){
        if let equitiesList = notification.userInfo!["equities"] as? [KeyValue] {
            lblCompanyName.text = equitiesList[0]["n"] as? String
            lblExchangeName_Sym.text = "\(String(describing: equitiesList[0]["e"] as! String)) : \(String(describing: equitiesList[0]["s"] as! String))"
        }
        //Call Borrow Status
        WebSocketManager.sharedInstance.borrowStatus(requestStatus : "".randomString(length: 10), symbol : CompanyVC.compDic[AppKey.companySym] as! String)
        
    }
    
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
        WebSocketManager.sharedInstance.getEquities(symArr: [CompanyVC.compDic[AppKey.companySym] as! String])
    }
    
    //Recive Borrow Status notification response
    @objc func borrowStatus(_ notification: NSNotification){
        if let borrowStatus = notification.userInfo!["status"] as? String {
            if borrowStatus == "EasyToBorrow"{
                lblBorrowStatus.text = "Easy to Borrow : Y"
                btnLocate.isEnabled = false
            }else{
                lblBorrowStatus.text = "Easy to Borrow : N"
                btnLocate.isEnabled = true
            }
            self.setTradeOrderButtons(borrowStatus: borrowStatus, position: CompanyVC.positionValue)
        }
        //Call Company data from websocket
        WebSocketManager.sharedInstance.subscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)
        
    }
    
    private func setTradeOrderButtons(borrowStatus:String,position:String){
        print(position,CompanyVC.positionValue)
        btnBuy.alpha = 1
        btnSell.alpha = 1
        btnBuy.isEnabled = true
        btnSell.isEnabled = true
        
        if CompanyVC.positionValue == "Long"{//Long position
           //no change required here
            
        }else if CompanyVC.positionValue == "Short"{//Short position
          
          btnSell.setTitle("Cover", for: .normal)
          btnBuy.setTitle("Short", for: .normal)
            
        }else{//no position
            
            if borrowStatus == "EasyToBorrow"{
                btnSell.setTitle("Short", for: .normal)
                btnBuy.setTitle("Buy", for: .normal)
            }else{
                btnSell.isEnabled = false
                btnSell.alpha = 0.2
                btnBuy.setTitle("Buy", for: .normal)
            }
        }
        
    }
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            likeButton.isEnabled = true
            quoteDic = companyData
        }
    }
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            if CompanyVC.compDic[AppKey.companySym] as? String == tradeDic[AppKey.tradeSymbol] as? String{
                loadTradeData(quoteDic, tradeDic)
            }
            
        }
        
    }
    
   
    //Recive chart data response from websocket
    @objc func chartData(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"]as? KeyValue {
            
            DispatchQueue.global(qos: .background).sync {
                guard let chartArr = companyData["data"] as? Array<NSDictionary>,chartArr.count > 0 else{
                    UIAlertController.show(self, "Message", "Chart data not found!")
                    return
                }
                self.initializeChart(chartArr)
            }
        }
        
    }
    
    //MARK: Add to watchList
    @objc func addtoWatchList(_ notification: NSNotification){
        
        if let status = notification.userInfo!["status"]as? String
        {
            if status == "MaxLimitReached"{
                
                UIAlertController.show(self, AppTitle.appTital, "Maximum limit has been reached for add in watchlist.")
                
            }else{
                
                inWatchList = true
                likeButton.tintColor = AppColor.gainColor
                publicWatchListArr.append(CompanyVC.compDic[AppKey.companySym] as! String)
                
            }
        }
        
        
    }
    
    @objc func removeFromWatchList(_ notification: NSNotification){
        
        inWatchList = false
        likeButton.tintColor = .white
        
        let index = publicWatchListArr.index(of: CompanyVC.compDic[AppKey.companySym] as! String)
        publicWatchListArr.remove(at: index!)
        
    }
    
    //Load data to view objects
    private func loadTradeData(_ quoteDic:KeyValue, _ tradeDic: KeyValue){
        
        
        if let previousClose = tradeDic[AppKey.lastTradePrice] as? Double {
            lblPreviousClose.text = "$\(String(describing: previousClose.rounded(toPlaces: 2)))"
        }
        if let volume = tradeDic[AppKey.tradeVolume] as? Double {
            lblVolume.text = "Volume : \(String(describing: Int(volume)))"
        }
        if let timeHour = tradeDic[AppKey.tradeHour] as? Int {
            if let timeMin = tradeDic[AppKey.tradeMinut] as? Int {
                
                
                var hourStr = String(describing: timeHour)
                if hourStr.characters.count == 1{
                    hourStr = "0\(hourStr)"
                }
                var minutStr = String(describing: timeMin)
                if minutStr.characters.count == 1{
                    minutStr = "0\(minutStr)"
                }
                
                lblCloseTime.text = "Time : \(hourStr):\(minutStr)"
            }
        }
        
       /* if date.isBetweenTwoTime(){
            if let lastTradePrice = tradeDic[AppKey.lastTradePrice] as? Double {
                lblPreviousClose.text = String(describing: lastTradePrice.rounded(toPlaces: 2))
            }
        }else{//AppKey.previousClose
            if let previousClose = tradeDic[AppKey.lastTradePrice] as? Double {
                lblPreviousClose.text = String(describing: previousClose.rounded(toPlaces: 2))
            }
        }*/
        
        
        if calculateGainOrLoss(tradeDic) ==  true{
            isGain = true
            imgGainLoss.image = UIImage.init(named: "up_arrow")
            lblGainLossPercentage.textColor = AppColor.gainColor
            btnBuy.backgroundColor = AppColor.gainColor
            btnBuy.borderColor = AppColor.gainColor
            btnSell.borderColor = AppColor.gainColor
            btnSell.setTitleColor(AppColor.gainColor, for: .normal)
        }else{
            isGain = false
            lblGainLossPercentage.textColor = AppColor.lossColor
            imgGainLoss.image = UIImage.init(named: "down_arrow")
            btnBuy.backgroundColor = AppColor.lossColor
            btnBuy.borderColor = AppColor.lossColor
            btnSell.borderColor = AppColor.lossColor
            btnSell.setTitleColor(AppColor.lossColor, for: .normal)
        }
        
        //load bottom collection view
        self.bottomCollectionLoad()
        
        
        switch chartTime {
        case .oneDay:
            self.oneDayChartData(self)//call one day Chart Data
        case .oneMonth:
            self.oneMonthChartData(self)//call one month Chart Data
        case .threeMonth:
            self.threeMonthChartData(self)//call three months Chart Data
        case .sixMonth:
            self.sixMonthChartData(self)//call one six months Data
        case .oneYear:
            self.oneYearChartData(self)//call one year Chart Data
        case .max:
            self.maxChartData(self)
        default:
            return
        }
        
        
        
    }
    
    private func bottomCollectionLoad(){
    
        collectionTitleArr = ["SHARES","ENTRY PRICE","NET CHANGE"]
        collectionValueArr = [CompanyVC.quantityOpen,
                              CompanyVC.entryPrice ,
                              calculateGain(quoteDic)!
        ]
        collectionShares.reloadData()
        
    }
    //calculate gain
    private func calculateGain(_ quoteDic:KeyValue)-> Double?{
        
        guard let bid =  quoteDic[AppKey.quoteBestBid]  , let ask = quoteDic[AppKey.quoteBestAsk]  else {
            return 0.0
        }
        if CompanyVC.positionValue == "Long"{
            let diffrence = (Double(truncating: bid as! NSNumber) - CompanyVC.entryPrice)*CompanyVC.quantityOpen
            
            return diffrence.rounded(toPlaces: 2)
            
        }else if CompanyVC.positionValue == "Short"{
            
            let diffrence = (Double(truncating: ask as! NSNumber) - CompanyVC.entryPrice)*CompanyVC.quantityOpen
            
            return diffrence.rounded(toPlaces: 2)
        } else{
            return 0.0
        }
        
        
    }
    //calculate gain or loss of stock
    private func calculateGainOrLoss(_ tradeDic:KeyValue)->Bool
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return false
        }
        let diffrence = (Double(truncating: lastTrade as! NSNumber) - Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        let percentage = (diffrence*100/Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        lblGainLossPercentage.text = "\(String(diffrence)) (\(String(percentage))%)"
        if Double(truncating: lastTrade as! NSNumber) >= Double(truncating: prevoiusClose as! NSNumber) {
            return true
        }else{
            return false
        }
    }
    
    
    
    
    //Load Chart data
    @IBAction func oneDayChartData(_ sender: Any) {
        chartTime = ChartTime.oneDay
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "IntraDay" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 30 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
        
        
        
    }
    
    @IBAction func oneMonthChartData(_ sender: Any) {
        chartTime = ChartTime.oneMonth
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 15 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 30 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    
    @IBAction func threeMonthChartData(_ sender: Any) {
        chartTime = ChartTime.threeMonth
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 90 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    
    @IBAction func sixMonthChartData(_ sender: Any) {
        chartTime = ChartTime.sixMonth
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 180 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    
    @IBAction func oneYearChartData(_ sender: Any) {
        chartTime = ChartTime.oneYear
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 365 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    
    @IBAction func maxChartData(_ sender: Any) {
        chartTime = ChartTime.max
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 1825 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    

    
    
    @IBAction func backaction(_ sender: Any) {
        //unsubscribe company
        WebSocketManager.sharedInstance.unSubscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)
        UserDefaults.standard.set(false, forKey: "isStats")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBuyActio(_ sender: Any) {
        
        let viewController = UIStoryboard(name: AppKey.companyDetail,bundle:nil).instantiateViewController(withIdentifier: BuySharesVC.identifier()) as? BuySharesVC
        viewController?.sideValue = (btnBuy.titleLabel?.text)!
        viewController?.symbolName = CompanyVC.compDic[AppKey.companySym] as? String ?? "N/A"
        
        if (btnBuy.titleLabel?.text)! == "Buy"{
            viewController?.estimateCostValue = quoteDic[AppKey.quoteBestAsk]  as? Double ?? 0.00
        }else{
            viewController?.estimateCostValue  = 0.00
        }
        
        self.navigationController?.pushViewController(viewController!, animated: true)
        
    }
    @IBAction func btnSellAction(_ sender: Any) {
        
        let viewController = UIStoryboard(name: AppKey.companyDetail,bundle:nil).instantiateViewController(withIdentifier: BuySharesVC.identifier()) as? BuySharesVC
        viewController?.sideValue = (btnSell.titleLabel?.text)!
        viewController?.symbolName = CompanyVC.compDic[AppKey.companySym] as? String ?? "N/A"
        if (btnSell.titleLabel?.text)! == "Short"{
            viewController?.estimateCostValue = quoteDic[AppKey.quoteBestBid]  as? Double ?? 0.00
        }else{
            viewController?.estimateCostValue = 0.00
        }
        
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func push(_ sender: Any) {
    }
    
    @IBAction func locateAction(_ sender: Any) {
        self.navigationController?.pushViewController(locateVC(), animated: true)
    }
    
    
    @IBAction func likeAction(_ sender: UIButton) {
        //for testing perpose
        
        if inWatchList == false{
            WebSocketManager.sharedInstance.addToWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
        }else{
             WebSocketManager.sharedInstance.removeFromWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
       
        }
        
    }
    
    //MARK: Initialise Chart
    func initializeChart(_ chartArr:Array<NSDictionary>) {
        
        label.text = ""
        
        chart.delegate = self
        chart.removeAllSeries()
        chart.layer.borderColor = UIColor.lightGray.cgColor
        chart.layer.borderWidth = 1
        chart.layer.cornerRadius = 3
        
        chartLandScape.delegate = self
        chartLandScape.removeAllSeries()
        chartLandScape.layer.borderColor = UIColor.lightGray.cgColor
        chartLandScape.layer.borderWidth = 1
        chartLandScape.layer.cornerRadius = 3
        
        
        
        // Initialize data series and labels
        
        let stockValues = getStockValuesFromSocket(chartArr)
        
        
        var serieData: [Float] = []
        var labels: [Float] = []
        var labelsAsString: Array<String> = []
        
        imgOneDay.isHidden = true
        imgOneMonth.isHidden = true
        imgThreeMonths.isHidden = true
        imgSixMonths.isHidden = true
        imgOneYear.isHidden = true
        imgMax.isHidden = true
        
        switch chartTime {
        case .oneDay:
            imgOneDay.isHidden = false

            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            
            
             let calendar = Calendar.current
            let oneDaysAgo = calendar.date(byAdding: .day, value: -2, to: Date())
            
           let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd"
            let oneDaysAgoStr:String = dateFormatter1.string(from: oneDaysAgo as! Date)
            let strTodate: Date = dateFormatter1.date(from: oneDaysAgoStr)!
            let convertTo = NSMutableArray(array: stockValues)
            print(convertTo)
            
            let filteredOneDay : Array<Dictionary<String, Any>> = (convertTo).filtered(using: NSPredicate(format: "d > %@", strTodate as NSDate)) as! Array<Dictionary<String, Any>>
            print(filteredOneDay )
            
            
            
            var indexForSlip = 0
            for (i, value) in filteredOneDay.enumerated()  {
                indexForSlip += 1
                serieData.append(value[AppKey.close] as! Float)
                
                
                
                    let monthAsString:String = dateFormatter.string(from: value[AppKey.date] as! Date)
                    
                    if (labels.count == 0 || labelsAsString.last != monthAsString && indexForSlip == 2 ) {
                        print(monthAsString)
                        indexForSlip = 0
                        labels.append(Float(i))
                        labelsAsString.append(monthAsString)
                    }
             

               
                
            }
        
        case .oneMonth:
            imgOneMonth.isHidden = false

            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd"
            
            var indexForSlip = 0
            for (i, value) in stockValues.enumerated() {
                indexForSlip += 1
                serieData.append(value[AppKey.close] as! Float)
                
                let monthAsString:String = dateFormatter.string(from: value[AppKey.date] as! Date)
                
                if (labels.count == 0 || labelsAsString.last != monthAsString && indexForSlip == 5) {
                    //print(monthAsString)
                    indexForSlip = 0
                    labels.append(Float(i))
                    labelsAsString.append(monthAsString)
                }
            }
            
            
        case .threeMonth :
            
            imgThreeMonths.isHidden = false

            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            
            var indexForSlip = 0
            for (i, value) in stockValues.enumerated() {
                indexForSlip += 1
                serieData.append(value[AppKey.close] as! Float)
                
                // Use only one label for each month
                let month = Int(dateFormatter.string(from: value[AppKey.date] as! Date))!
                 let monthAsString:String = dateFormatter.shortMonthSymbols[month - 1]
                
                if (labels.count == 0 || labelsAsString.last != monthAsString ) {
                   // print(monthAsString)
                    indexForSlip = 0
                    labels.append(Float(i))
                    labelsAsString.append(monthAsString)
                }
            }
            
        case  .sixMonth:
            
            imgSixMonths.isHidden = false
            
            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM"
            
            var indexForSlip = 0
            for (i, value) in stockValues.enumerated() {
                indexForSlip += 1
                serieData.append(value[AppKey.close] as! Float)
                
                // Use only one label for each month
                let month = Int(dateFormatter.string(from: value[AppKey.date] as! Date))!
                let monthAsString:String = dateFormatter.shortMonthSymbols[month - 1]
                
                if (labels.count == 0 || labelsAsString.last != monthAsString ) {
                    // print(monthAsString)
                    indexForSlip = 0
                    labels.append(Float(i))
                    labelsAsString.append(monthAsString)
                }
            }
            
        case .oneYear:
            imgOneYear.isHidden = false

            
            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM yyyy"
            
            var indexForSlip = 0
            for (i, value) in stockValues.enumerated() {
                
                indexForSlip += 1
                
                serieData.append(value[AppKey.close] as! Float)
                
                let monthAsString:String = dateFormatter.string(from: value[AppKey.date] as! Date)
                if (labels.count == 0 || (labelsAsString.last != monthAsString && indexForSlip == 80)) {
                        indexForSlip = 0
                        //print(monthAsString)
                        labels.append(Float(i))
                        labelsAsString.append(monthAsString)
                   
                   
                }
            }
           
           
           
            
        case .max:
            imgMax.isHidden = false
            // Date formatter to retrieve the month names
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            
            var indexForSlip = 0
            for (i, value) in stockValues.enumerated() {
                indexForSlip += 1
                serieData.append(value[AppKey.close] as! Float)
                
                // Use only one label for each month
                
                let monthAsString:String = dateFormatter.string(from: value[AppKey.date] as! Date)

                
                if (labels.count == 0 || labelsAsString.last != monthAsString ) {
                    //print(monthAsString)
                    indexForSlip = 0
                    labels.append(Float(i))
                    labelsAsString.append(monthAsString)
                }
            }
        
        }
        
       
        
        let series = ChartSeries(serieData)
        if isGain{
            series.color = AppColor.gainColor
            chart.highlightLineColor = AppColor.gainColor
        }else{
            series.color = AppColor.lossColor
            chart.highlightLineColor = AppColor.lossColor
        }
        series.area = true
        
        // Configure chart layout
        
        chart.lineWidth = 1.5
        chart.labelFont = UIFont.systemFont(ofSize: 12)
        chart.xLabels = labels
        chart.xLabelsFormatter = { (labelIndex: Int, labelValue: Float) -> String in
            return labelsAsString[labelIndex]
        }
        chart.xLabelsTextAlignment = .left
        chart.yLabelsOnRightSide = true
        // Add some padding above the x-axis
       
            chart.minY = serieData.min()! - 5
            chart.add(series)
       
        
        
        
        
        
        chartLandScape.backgroundColor = UIColor.white
        chartLandScape.lineWidth = 1.5
        chartLandScape.labelFont = UIFont.systemFont(ofSize: 12)
        chartLandScape.xLabels = labels
        chartLandScape.xLabelsFormatter = { (labelIndex: Int, labelValue: Float) -> String in
            return labelsAsString[labelIndex]
        }
        chartLandScape.xLabelsTextAlignment = .left
        chartLandScape.yLabelsOnRightSide = true
        // Add some padding above the x-axis
       
          chartLandScape.minY = serieData.min()! - 5
            chartLandScape.add(series)
        
        
        
        
        
    }
    //MARK: Chart delegate
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Float, left: CGFloat) {
        
        if let value = chart.valueForSeries(0, atIndex: indexes[0]) {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.minimumFractionDigits = 2
            numberFormatter.maximumFractionDigits = 2
            label.text = "$\(String(describing: numberFormatter.string(from: NSNumber(value: value))!))"
            labelLandScape.text = numberFormatter.string(from: NSNumber(value: value))
            
            // Align the label to the touch left position, centered
            var constant = labelLeadingMarginInitialConstant + left - (label.frame.width / 2)
            //var constant = labelLeadingMarginInitialConstant + left - (15 / 2)
            
            // Avoid placing the label on the left of the chart
            if constant < labelLeadingMarginInitialConstant {
                constant = labelLeadingMarginInitialConstant
            }
            
            // Avoid placing the label on the right of the chart
            let rightMargin = chart.frame.width - label.frame.width
            if constant > rightMargin {
                constant = rightMargin
            }
            
            labelLeadingMarginConstraint.constant = constant
            
        }
        
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        label.text = ""
        labelLandScape.text = ""
        labelLeadingMarginConstraint.constant = labelLeadingMarginInitialConstant
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
    //Get stockValue From FB.json
    func getStockValues() -> Array<Dictionary<String, Any>> {
        
        // Read the JSON file
        let filePath = Bundle.main.path(forResource: CompanyVC.compDic[AppKey.companySym] as! String, ofType: "json")!
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: filePath))
        let json: NSDictionary = (try! JSONSerialization.jsonObject(with: jsonData!, options: [])) as! NSDictionary
        let jsonValues = json["quotes"] as! Array<NSDictionary>
        
        // Parse data
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let values = jsonValues.map { (value: NSDictionary) -> Dictionary<String, Any> in
            let date = dateFormatter.date(from: value["date"]! as! String)
            let close = (value["close"]! as! NSNumber).floatValue
            return ["date": date!, "close": close]
        }
        
        return values
        
    }
    
    
    
    
    
    func getStockValuesFromSocket(_ chartArr: Array<NSDictionary>) -> Array<Dictionary<String, Any>> {
        
        let calendar = Calendar.current
        let oneDaysAgo = calendar.date(byAdding: .day, value: -1, to: Date())
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        
        let oneDaysAgoStr:String = dateFormatter1.string(from: (oneDaysAgo as? Date)!)
        
        let jsonValues = chartArr
        
        // Parse data
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let values = jsonValues.map { (value: NSDictionary) -> Dictionary<String, Any> in
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            let date = dateFormatter.date(from: value[AppKey.date]! as! String)
             let close = (value[AppKey.close]! as! NSNumber).floatValue
            return [AppKey.date: date!, AppKey.close: close]
            
        }
        
        return values
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        
        // Redraw chart on rotation
        chart.setNeedsDisplay()
        
    }
    

}

extension CompanyVC:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionTitleArr.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
          if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as? CompanyCell{
            
            if indexPath.row == 0{
                cell.lblValue.text = String(describing: Int(collectionValueArr[indexPath.row] as! Double))
            }
            if indexPath.row == 1{
                
                cell.lblValue.text = "$\(String(describing: collectionValueArr[indexPath.row]))"
            }
           
            cell.lblTitle.text = String(describing: collectionTitleArr[indexPath.row])
            
            if indexPath.row == 2{
                cell.lblValue.text = "$\(String(describing: collectionValueArr[indexPath.row]))"

                if (collectionValueArr[indexPath.row] as! Double) > 0{
                   cell.lblValue.textColor = AppColor.gainColor
                }else{
                    cell.lblValue.textColor = AppColor.lossColor
                }
            }else{
                cell.lblValue.textColor = UIColor.white
            }
            
            return cell
        }
            
        return UICollectionViewCell()
    }
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
