//
//  BuySharesVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

 enum ShareOrderType: String {
    case market           = "Market"
    case limit            = "Limit"
    case stop             = "Stop Market"
    case stopLimit        = "Stop Limit"
    case range            = "Range"
    case marketOnOpen     = "Market On Open"
    case marketOnClose    = "Market On Close"
    case limitOnOpen      = "Limit On Open"
    case limitOnClose      = "Limit On Close"
}


class BuySharesVC: BaseVC {
    
     public var estimateCostValue = Double()

    @IBOutlet weak var titleText: UINavigationItem!
    
    override class func identifier() -> String {
        return "BuySharesVC"
    }
    
    @IBOutlet weak var lblBuyingPoewr: UILabel!
    @IBOutlet weak var txtSharesNo: UITextField!
    
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnTimeInForce: UIButton!
    
    @IBOutlet weak var txtShare: UITextField!
    @IBOutlet weak var txtOrderType: UITextField!
    @IBOutlet weak var txtTimeInForce: UITextField!
    @IBOutlet weak var txtRoute: UITextField!
    
    
    
    @IBOutlet weak var lblLastPrice: UILabel!
    @IBOutlet weak var lblEstCost: UILabel!
    
    @IBOutlet weak var tblBuyShare: UITableView!
    
    var orderType = ShareOrderType(rawValue: "Market")
    var sideValue = String()
    var symbolName = String()
    var shareOf = String()
    var isUpdateTF : Bool = false
    var noOfShare : Double = 0.0
    var limitValue : Double = 0.0
    
    
    
    let orderDicType: KeyValue = ["Market":"Market","Limit":"Limit","Stop Market":"Stop","Stop Limit":"StopLimit","Range":"Range","Market On Open":"MarketOnOpen","Market On Close":"MarketOnClose","Limit On Open":"LimitOnOpen","Limit On Close":"LimitOnClose","IntraDay":"Day","Good Til Canceled":"GoodTilCanceled","Good Til Crossing":"GoodTilCrossing","Fill Or Kill":"FillOrKill","At The Opening":"AtTheOpening",]
    
    
    
    var fieldArr = [String]()
    var timeInForceArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.title = "\(sideValue) Shares"
        shareOf = "SHARE OF \(symbolName)"
        
        //set dynamic frame of tableview
        tblBuyShare.estimatedRowHeight = 75
        tblBuyShare.rowHeight = UITableViewAutomaticDimension
        
        lblEstCost.text = "$0.0"//"$\(String(estimateCostValue))"
        txtShare.placeholder = shareOf
        txtOrderType.text = "Market"
        txtTimeInForce.text = "IntraDay"
        
        txtShare.addTarget(self, action: #selector(BuySharesVC.textFieldDidChangeForShare(_:)), for: UIControlEvents.editingChanged)
        
        
        orderTypeValueChange()//change frame according to ordertype
        
        //Notification Center for
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderApiResponseData(_:)), name: NSNotification.Name(rawValue: SocketConstants.orderApiResponse), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.invelidData(_:)), name: NSNotification.Name(rawValue: SocketConstants.invalidrequest), object: nil)

        //txtSharesNo.addTarget(self, action: #selector(SignUpNameVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
    }
    @objc func textFieldDidChange(_ textField : UITextField){
        guard let shareNo = textField.text,shareNo != "" else {
            lblEstCost.text = "$0"
            return
        }
        if (textField.text?.contains("."))!{
            return
        }
        let totalValue = Double(shareNo)!*estimateCostValue
        lblEstCost.text = "$\(String(totalValue))"

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func orderApiResponseData(_ notification: NSNotification){
        
        
        if let responseData = notification.userInfo!["data"] as? KeyValue {
            
            guard let eventType = responseData["eventType"]  as? String  else {
                
                if let messageStr = (responseData["details"] as! KeyValue)["message"] as? String{
                    if messageStr != "" {
                        UIAlertController.show(self, "Message", messageStr)
                    }
                }
                return
            }
            
            if eventType == "Accepted"{
                
                let alertController = UIAlertController(title: "Your order has been accepted!", message: "", preferredStyle: .alert)
                let saveAction = UIAlertAction(title: "OK", style: .destructive, handler: {
                    alert -> Void in
                    
                    let accountStoryboard = UIStoryboard(name: AppKey.portfolioSB, bundle:nil)
                    let centerViewController = accountStoryboard .instantiateViewController(withIdentifier: PortfolioVC.identifier()) as? PortfolioVC
                    let navController = UINavigationController(rootViewController:centerViewController!)
                    self.slideMenuController()?.changeMainViewController(navController, close: true)
                    
                    //KAppDelegate.goToRootViewOrderPlaced()
                    
                    /*let viewController = UIStoryboard(name: AppKey.companyDetail,bundle:nil).instantiateViewController(withIdentifier: PurchasedShareDetailsVC.identifier()) as? PurchasedShareDetailsVC
                    viewController?.orderDic = responseData["details"] as! KeyValue
                    viewController?.pricePershare = self.lblEstCost.text!
                    self.navigationController?.pushViewController(viewController!, animated: true)*/
                    
                })
                alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
           else if eventType == "Fill"{
                UIAlertController.show(self, "Message", "Your order has been filled")
            }
            else if eventType == "Reject"{
                UIAlertController.show(self, "Message", "Your order has been rejected")
            }
           
            
            guard let messageStr = (responseData["details"] as! KeyValue)["message"] as? String  else {
                return
            }
            UIAlertController.show(self, "Message", messageStr)
            
        }
    }
    
    @objc func invelidData(_ notification: NSNotification){
        if let responseString = notification.userInfo!["data"] as? String {
            UIAlertController.show(self, "Message", responseString)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShareAction(_ sender: Any) {
    }
    
    
    @IBAction func btnRouteAction(_ sender: Any) {
        
        //lblEstCost.text = "$0.0"
        
        isUpdateTF = false// not update text field in scrolling
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in ["SIM", "ARCA"] {
            alert.view.tintColor = AppColor.backGroundColor
            alert.addAction(UIAlertAction(title: i, style: .default, handler: actionSheetRouteHandler))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
     @IBAction func btnTypeAction(_ sender: Any) {
        //lblEstCost.text = "$0.0"
        
        
        isUpdateTF = false// not update text field in scrolling
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
       
        for i in ["Market", "Limit", "Stop Market", "Stop Limit","Range"] {
            alert.view.tintColor = AppColor.backGroundColor
            alert.addAction(UIAlertAction(title: i, style: .default, handler: actionSheetHandler))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Timein force value with action sheet
    
    @IBAction func timeInForceAction(_ sender: Any) {
        
        
        switch orderType {
        case .market?:
            timeInForceArr = ["IntraDay"]
            openActioSheetTimeInForce(timeInForceArr)//load action sheet
            break
        case .limit?:
            timeInForceArr = ["IntraDay","Good Til Canceled","Good Til Crossing","Fill Or Kill","At The Opening"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .stop?:
            timeInForceArr = ["Day","Good Til Canceled","Good Til Crossing"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .stopLimit?:
            timeInForceArr = ["IntraDay","Good Til Canceled","Good Til Crossing"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .range?:
            timeInForceArr = ["IntraDay","Good Til Canceled","Good Til Crossing"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .marketOnOpen?:
            timeInForceArr = ["IntraDay"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .marketOnClose?:
            timeInForceArr = ["IntraDay"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .limitOnOpen?:
            timeInForceArr = ["IntraDay"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        case .limitOnClose?:
            timeInForceArr = ["IntraDay"]
            openActioSheetTimeInForce(timeInForceArr)
            break
        default:
            break
        }
        
        
        
    }
    //Time In Force ActionSheet
    private func openActioSheetTimeInForce(_ timeArr : [String]){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for i in timeArr {
            alert.view.tintColor = AppColor.backGroundColor
            alert.addAction(UIAlertAction(title: i, style: .default, handler: actionSheetHandlerForTimeInForce))
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Action Sheet Route Handler
    func actionSheetRouteHandler(action: UIAlertAction){
        txtRoute.text = action.title
    }
    
    // Action Sheet handler for ORDER TYPE
    func actionSheetHandler(action: UIAlertAction) {
        //Use action.title
       
        isUpdateTF = true

        orderType = ShareOrderType(rawValue: action.title!)
        orderTypeValueChange()//change frame according to ordertype

        
        txtTimeInForce.text = "IntraDay"//set default value to day when ordertype change
        txtOrderType.text = action.title
    }
    
    // Action Sheet handler for ORDER TYPE
    func actionSheetHandlerForTimeInForce(action: UIAlertAction) {
        //Use action.title
        
        
        txtTimeInForce.text = action.title
    }
    
    //change frame according to ordertype

    private func orderTypeValueChange(){
        
        //set limit value regarding share value
        lblEstCost.text = "$\(String(limitValue*noOfShare))"
        print(limitValue,noOfShare)
        
        switch orderType {
        case .market?:
            lblEstCost.text = "$\(String(Double(estimateCostValue)*noOfShare))"
            
            fieldArr = []//[shareOf]
            tblBuyShare.reloadData()
            break
        case .limit?:
            
            fieldArr = ["LIMIT PRICE"]//[shareOf,"LIMIT PRICE"]
            tblBuyShare.reloadData()
            
            break
        case .stop?:
            fieldArr = ["STOP PRICE"]//[shareOf,"STOP PRICE"]
            tblBuyShare.reloadData()
            
            break
        case .stopLimit?:
            
            fieldArr = ["LIMIT PRICE","STOP PRICE"]//[shareOf,"LIMIT PRICE","STOP PRICE"]
            tblBuyShare.reloadData()
            
            break
        case .range?:
            lblEstCost.text = "$\(String(Double(estimateCostValue)*noOfShare))"
            
            fieldArr = ["PRICE ABOVE","PRICE BELOW"]//[shareOf,"PRICE ABOVE","PRICE BELOW"]
            tblBuyShare.reloadData()
            break
        case .marketOnOpen?:
            fieldArr = []//[shareOf]
            tblBuyShare.reloadData()
            break
        case .marketOnClose?:
            fieldArr = []//[shareOf]
            tblBuyShare.reloadData()
            break
        case .limitOnOpen?:
            fieldArr = ["LIMIT PRICE"]//[shareOf,"LIMIT PRICE"]
            tblBuyShare.reloadData()
            break
        case .limitOnClose?:
            fieldArr = ["LIMIT PRICE"]//[shareOf,"LIMIT PRICE"]
            tblBuyShare.reloadData()
            break
        default:
            break
        }
        
    }
    
    @IBAction func btnProceedAction(_ sender: Any) {
        
        if txtShare.text?.characters.count == 0{
            UIAlertController.show(self, "Alert!", "Please fill no of shares field")
            return
        }
        
        var textFieldValueArr = [String]()
        for i in 0 ..< fieldArr.count{
            
            
            guard let textValue = textFieldsValueFromCell(indexValue: i) ,textValue != "" else{
                UIAlertController.show(self, "Alert!", "Please fill \(String(describing: placeHolderValueOfTextFieldFromCell(indexValue: i))) field")
                return
            }
            textFieldValueArr.append(textValue)
            
        }
        print(textFieldValueArr)
        
        if isInternetReachable() {
            //call place order Api
            placeOrderApiCall(textFieldArr : textFieldValueArr, orderType : (orderType?.rawValue)!)
        }else{
            //FMStatusBarAlert(duration: KTimeDuration, delay: KDelayTime, position: .statusBar).message(message: "No Internet connection").messageColor(color: .white).bgColor(color: .red).completion {}.show()
            UIAlertController.show(self, "Message", "No Internet connection!")
        }
        
        
  
    }
    
    private func placeOrderApiCall(textFieldArr:[String],orderType:String){
        //["Market", "Limit", "Stop Loss", "Stop Limit","Range","Market On Open","Market On Close","Limit On Open","Limit On Close"]
        
        var postDic = KeyValue()
        postDic["clientOrderId"] = "".randomString(length: 30)
        postDic["accountId"] = userAccountId
        postDic["symbol"] = symbolName
        postDic["side"] = sideValue
        postDic["orderType"] = orderDicType[txtOrderType.text!]
        postDic["timeInForce"] = orderDicType[txtTimeInForce.text!]
        postDic["route"] = txtRoute.text
        postDic["quantity"] = Int(txtShare.text!)
        
       //add all limit value
        if orderType == "Limit" || orderType == "Stop Limit" || orderType == "Limit On Open" || orderType == "Limit On Close"{
            postDic["limitPriceCents"] = Float(textFieldArr[0])!*100
            if orderType == "Stop Limit"{
                postDic["stopPriceCents"] = Float(textFieldArr[1])!*100
            }
        }
        else if orderType == "Stop Market"{
            postDic["stopPriceCents"] = Float(textFieldArr[0])!*100
        } else if orderType == "Range"{
            postDic["highPriceCents"] = Float(textFieldArr[0])!*100
            postDic["lowPriceCents"] = Float(textFieldArr[1])!*100
        }
        
       print(postDic)
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Place Order Dictionary
            dic["requestType"] = "PlaceOrder" as AnyObject//
            dic["data"] = postDic as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
        
    }
    
    
    //Get textField Value from tableview cell
    private func textFieldsValueFromCell(indexValue:Int) -> String?{
        let indexPath = IndexPath(item: indexValue, section: 0)
        let cell = tblBuyShare.cellForRow(at: indexPath) as! BuyShareCell
        return cell.txtField.text
    }
    
    //Get Placeholder value of textField from tableview cell
    private func placeHolderValueOfTextFieldFromCell(indexValue:Int) -> String{
        let indexPath = IndexPath(item: indexValue, section: 0)
        let cell = tblBuyShare.cellForRow(at: indexPath) as! BuyShareCell
        return cell.txtField.placeholder!
    }
    
    
    

}


extension BuySharesVC{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtOrderType{
            btnTypeAction(self)
        }
        if textField == txtTimeInForce{
            timeInForceAction(self)
        }
        
        return true
    }
}

extension BuySharesVC: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BuyShareCell{
            cell.selectionStyle = .none
            
//            if isUpdateTF == true{
//                cell.txtField.text = ""
//            }
            cell.txtField.placeholder = fieldArr[indexPath.row]
            cell.txtField.keyboardType = .decimalPad
            
             if indexPath.row == 0{
                cell.txtField.addTarget(self, action: #selector(BuySharesVC.textFieldDidChangeForLimit(_:)), for: UIControlEvents.editingChanged)
            }
            
            // change this for not update textfield
            if indexPath.row == fieldArr.count-1 {
                self.isUpdateTF = false
            }
            return cell
        }
        return UITableViewCell()
    }
    
    @objc func textFieldDidChangeForShare(_ textField : UITextField){
        
        if let txtValue = textField.text{
            if txtValue != ""{
                

                if txtOrderType.text == "Limit" || txtOrderType.text == "Stop Market" || txtOrderType.text == "Range" || txtOrderType.text == "Stop Limit"{
                    
                    let countValue:Double = Double(txtValue)!
                    noOfShare = countValue
                    lblEstCost.text = "$\(String(limitValue*countValue))"
                    
                }else{
                    let countValue:Double = Double(txtValue)!
                    noOfShare = countValue
                    lblEstCost.text = "$\(String(Double(estimateCostValue)*countValue))"
                }
                
                
            }else{
                noOfShare = 0
                lblEstCost.text = "$0.0"
            }
        }
       
    }
    
    @objc func textFieldDidChangeForLimit(_ textField : UITextField){
        
        if let txtValue = textField.text{
            if txtValue != ""{
                let amountValue:Double = Double(txtValue)!
                limitValue = amountValue
                lblEstCost.text = "$\(String(noOfShare*amountValue))"
            }else{
                limitValue = 0
                
                
                
                
            }
        }
        
    }
    
    @objc func handelDatePicker(_ sender: UIDatePicker)    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateStr = dateFormatter.string(from:sender.date)
        
        let indexPath = IndexPath(item: 1, section: 0)
        let cell = tblBuyShare.cellForRow(at: indexPath) as! BuyShareCell
        cell.txtField.text = dateStr
    }
    
    
}






