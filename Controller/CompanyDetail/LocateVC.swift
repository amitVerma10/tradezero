//
//  LocateVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/16/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class LocateVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "LocateVC"
    }

    @IBOutlet weak var txtSymbol: UITextField!
    @IBOutlet weak var txtShares: UITextField!
    
    var historyArr = [KeyValue]()
    var cancelArr = [String]()
    
    @IBOutlet weak var tblLocate: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblLocate.estimatedRowHeight = 129
        tblLocate.rowHeight = UITableViewAutomaticDimension
        tblLocate.tableFooterView = UIView()
        
        txtSymbol.text = CompanyVC.compDic[AppKey.companySym] as? String
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.locateHistoryData(_:)), name: NSNotification.Name(rawValue: SocketConstants.locateHistory), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.locateRequestData(_:)), name: NSNotification.Name(rawValue: SocketConstants.locate), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.locateAcceptData(_:)), name: NSNotification.Name(rawValue: SocketConstants.locateAccept), object: nil)

        // Do any additional setup after loading the view.
        
        
        WebSocketManager.sharedInstance.locateHistory(accountId: userAccountId)
        
        if UserDefaults.standard.value(forKey: "savedCancelArr") != nil{
            cancelArr = (UserDefaults.standard.value(forKey: "savedCancelArr") as! [String])
        }
        
       
    }
    
    //MARK: Locate History Data response
    @objc func locateHistoryData(_ notification: NSNotification){
        if let historyData = notification.userInfo!["locates"] as? [KeyValue] {
            if historyData.count == 0{
                if UserDefaults.standard.value(forKey: "savedCancelArr") != nil{
                    UserDefaults.standard.removeObject(forKey: "savedCancelArr")
                }
            }
            historyArr = historyData
            tblLocate.reloadData()
            print(historyData)
        }
        
    }
    
    //MARK: Locate Request Data response
    @objc func locateRequestData(_ notification: NSNotification){
        WebSocketManager.sharedInstance.locateHistory(accountId: userAccountId)
        if let requestData = notification.userInfo!["status"] as? String {
            UIAlertController.show(self, "Message", requestData)
            print(requestData)
        }
        
    }
    
    //MARK: Locate Accept Data response
    @objc func locateAcceptData(_ notification: NSNotification){
        WebSocketManager.sharedInstance.locateHistory(accountId: userAccountId)
        if let acceptData = notification.userInfo!["data"] as? KeyValue {
            print(acceptData)
        }
        
    }

    //MARK: Locate request action
    @IBAction func locateAction(_ sender: Any) {
        
        if txtShares.text == "" {
            UIAlertController.show(self, AppTitle.appTital, "Please fill quantity")
            return
        }
        if Int(txtShares.text!)! % 100 == 0{
            WebSocketManager.sharedInstance.locateRequest(symbol: CompanyVC.compDic[AppKey.companySym] as! String, accountId: userAccountId,quantity : Int(txtShares.text!)!)
        }else{
            UIAlertController.show(self, AppTitle.appTital, "Please fill quantity multiple of hundred")
        }
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension LocateVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if cancelArr.containsObject(historyArr[indexPath.row]["requestId"] as! String){
            
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? LocateCellOne{
                cell.selectionStyle = .none
                cell.lblSymbol.text = historyArr[indexPath.row]["symbol"] as? String
                cell.lblAccount.text = historyArr[indexPath.row]["account"] as? String
                cell.lblPrice.text = "$\(String(describing: (historyArr[indexPath.row]["retailPrice"] as! Double)/10000))"
                cell.lblTotal.text = "$\(String(describing: ((historyArr[indexPath.row]["retailPrice"] as! Double)*(historyArr[indexPath.row]["quantity"] as! Double))/10000))"
                cell.lblShares.text = String(describing: historyArr[indexPath.row]["quantity"] as! Int)
                
                cell.lblStatus.text = "Cancelled"
                
                return cell
            }
            
        }else{
            
            if historyArr[indexPath.row]["status"] as? String != "Unaccepted"{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? LocateCellOne{
                    cell.selectionStyle = .none
                    cell.lblSymbol.text = historyArr[indexPath.row]["symbol"] as? String
                    cell.lblAccount.text = historyArr[indexPath.row]["account"] as? String
                    cell.lblPrice.text = "$\(String(describing: (historyArr[indexPath.row]["retailPrice"] as! Double)/10000))"
                    cell.lblTotal.text = "$\(String(describing: ((historyArr[indexPath.row]["retailPrice"] as! Double)*(historyArr[indexPath.row]["quantity"] as! Double))/10000))"
                    cell.lblShares.text = String(describing: historyArr[indexPath.row]["quantity"] as! Int)
                    
                    
                    cell.lblStatus.text = historyArr[indexPath.row]["status"] as? String
                    
                    return cell
                }
            }else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "CellTwo") as? LocateCellTwo{
                    cell.selectionStyle = .none
                    
                    cell.lblSymbol.text = historyArr[indexPath.row]["symbol"] as? String
                    cell.lblAccount.text = historyArr[indexPath.row]["account"] as? String
                    cell.lblPrice.text = "$\(String(describing: (historyArr[indexPath.row]["retailPrice"] as! Double)/10000))"
                    cell.lblTotal.text = "$\(String(describing: ((historyArr[indexPath.row]["retailPrice"] as! Double)*(historyArr[indexPath.row]["quantity"] as! Double))/10000))"
                    cell.lblShares.text = String(describing: historyArr[indexPath.row]["quantity"] as! Int)
                    cell.lblStatus.text = historyArr[indexPath.row]["status"] as? String
                    
                    
                    cell.btnAccept.addTarget(self, action: #selector(LocateVC.buttonAccepedTapped), for: .touchUpInside)
                    cell.btnAccept.tag = indexPath.row
                    
                    cell.btnReject.addTarget(self, action: #selector(LocateVC.buttonRejectTapped), for: .touchUpInside)
                    cell.btnAccept.tag = indexPath.row
                    return cell
                }
            }
            
        }
        
        
      
      
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func buttonAccepedTapped(sender: UIButton){
        let buttonTag = sender.tag
         WebSocketManager.sharedInstance.locateAccept(requestId: historyArr[buttonTag]["requestId"] as! String, symbol: historyArr[buttonTag]["symbol"] as! String, accountId: historyArr[buttonTag]["account"] as! String, quantity: historyArr[buttonTag]["quantity"] as! Int)
    }
    
    @objc func buttonRejectTapped(sender: UIButton){
        let buttonTag = sender.tag
        cancelArr.append(historyArr[buttonTag]["requestId"] as! String)
        UserDefaults.standard.set(cancelArr, forKey: "savedCancelArr")
        tblLocate.reloadData()
    }
    
   
}
