//
//  PurchasedShareDetailsVC.swift
//  TradeZero
//
//  Created by Amit Verma on 1/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class PurchasedShareDetailsVC: BaseVC {
    
    @IBOutlet weak var lblSharePurchased: UILabel!
    @IBOutlet weak var lblTotalCost: UILabel!
    
    @IBOutlet weak var lblNewPosition: UILabel!
    @IBOutlet weak var lblPricePerShare: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    
    var orderDic = KeyValue()
    var pricePershare = String()
    
    
    override class func identifier() -> String {
        return "PurchasedShareDetailsVC"
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        lblSharePurchased.text = String(describing: orderDic["totalQuantity"]!)
        lblPricePerShare.text = pricePershare

        let price = Float(pricePershare.replacingOccurrences(of: "$", with: ""))
        lblTotalCost.text = String(price! * Float(orderDic["totalQuantity"] as! Int))
        // Do any additional setup after loading the view.
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        KAppDelegate.goToRootViewAfterLogin()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
