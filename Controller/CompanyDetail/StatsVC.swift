//
//  StatsVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/20/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import SwiftCharts
import NotificationBannerSwift

class StatsVC: BaseVC {
    
   @IBOutlet weak var likeButton: UIBarButtonItem!
    
    fileprivate var chart: Chart? // arc
    let sideSelectorHeight: CGFloat = 0
     var groupsData: [(title: String, [(min: Double, max: Double)])] = [(title: String, [(min: Double, max: Double)])]()
    
    @IBOutlet weak var chartView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionStats: UICollectionView!
    
    @IBOutlet weak var lblExpectedEps: UILabel!
    @IBOutlet weak var lblActualEps: UILabel!
    
    //Variables
    fileprivate var quoteDic = KeyValue()
    fileprivate var tradeDic = KeyValue()
    fileprivate var statsDic = KeyValue()
    fileprivate var statsValueArr = [Any]()
    fileprivate var statsTitleArr = [String]()
    fileprivate var epsValueDic   = [String : Double]()
    
    var isFirstTime: Bool = true
    
    
    override class func identifier() -> String {
        return "StatsVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.isDirectionalLockEnabled = true
        collectionStats.isDirectionalLockEnabled = true
        collectionStats.isScrollEnabled = false
        likeButton.isEnabled = false
        
        
        
        //Call stats data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()
            dic["rt"] = "GetSymbolMetrics" as AnyObject
            dic["symbol"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
        //Notification Center for state data
        NotificationCenter.default.addObserver(self, selector: #selector(self.statsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.statsDataRecived), object: nil)
        
        //Notification Center for trade data
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataTrade(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeForState), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.companyDataQuote(_:)), name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteForState), object: nil)
        
        //Notification Center for chart data  SocketConstants.earnings
         NotificationCenter.default.addObserver(self, selector: #selector(self.chartData(_:)), name: NSNotification.Name(rawValue: SocketConstants.earnings), object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.chartData(_:)), name: NSNotification.Name(rawValue: SocketConstants.CandelchartDataRecived), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addtoWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.addToWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFromWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadSocketDatafromForeground(_:)), name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForegroundInStats), object: nil)
        
        
        if inWatchList
        {
            likeButton.tintColor = AppColor.gainColor
        }else{
            likeButton.tintColor = .white
        }
        
        //Dummy use of candelistic chart
        

        // Do any additional setup after loading the view.
    }
    
    
    @objc func reloadSocketDatafromForeground(_ notification: NSNotification){
        //Call stats data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()
            dic["rt"] = "GetSymbolMetrics" as AnyObject
            dic["symbol"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    
    //MARK: Add to watchList
    @objc func addtoWatchList(_ notification: NSNotification){
        
        if let status = notification.userInfo!["status"]as? String
        {
            if status == "MaxLimitReached"{
                
                UIAlertController.show(self, AppTitle.appTital, "Maximum limit has been reached for add in watchlist.")
                
            }else{
                
                inWatchList = true
                likeButton.tintColor = AppColor.gainColor
                
            }
        }
        
    }
    
    @objc func removeFromWatchList(_ notification: NSNotification){
        
        inWatchList = false
        likeButton.tintColor = .white
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
    }
    
    private func oneYearChartData(_ sender: Any) {
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Chart
            dic["rt"] = "OHLCVChart" as AnyObject
            dic["rid"] = "1234abc" as AnyObject
            dic["ct"] = "Daily" as AnyObject
            dic["res"] = 60 as AnyObject
            dic["sm"] = 570 as AnyObject
            dic["em"] = 960 as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["fb"] = true as AnyObject
            dic["nc"] = 30 as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
    }
    
    private func earningApiRequist(){
        
        if WebSocketManager.sharedInstance.isConnected(){
            
            var dic = [String:AnyObject]()
            dic["tickers"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            dic["rt"] = "Earnings" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
            
        }
        
    }
    
    
    //Recive STATS Data notification response
    @objc func statsData(_ notification: NSNotification){
        
        if let statsDictionary = notification.userInfo!["metrics"] as? KeyValue {
            statsDic = statsDictionary
        }
        else{
            
            UIAlertController.show(self, "Message", "Stats values  not found!")
        }
        WebSocketManager.sharedInstance.subscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)

    }
    
    
    
    
    //Recive Quote Data notification response
    @objc func companyDataQuote(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            likeButton.isEnabled = true
            quoteDic = companyData
        }
    }
    
    
    //Recive Trade Data notification response
    @objc func companyDataTrade(_ notification: NSNotification){
        if let companyData = notification.userInfo!["data"] as? KeyValue {
            tradeDic = companyData
            self.loadStatsData(statsDictionary: statsDic)
            self.earningApiRequist()
        }
        
    }
    
    //Recive chart data response from websocket
    @objc func chartData(_ notification: NSNotification){
        if let companyData = notification.userInfo!["earnings"]as? Array<NSDictionary> {
            
            
            DispatchQueue.global(qos: .background).sync {
//                guard let chartArr = companyData["earnings"] as? Array<NSDictionary>,chartArr.count > 0 else{
//                    UIAlertController.show(self, "Message", "Chart data not found!")
//                    return
//                }
                 //loadCanldleChart(chartArr)
                
                if isFirstTime{
                    isFirstTime = false
                    loadBarChart(companyData)
                }
                
            }
        }
        
    }
    
    @IBAction func likeAction(_ sender: Any) {
        
        //for testing perpose
        
        if inWatchList == false{
            WebSocketManager.sharedInstance.addToWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
        }else{
            WebSocketManager.sharedInstance.removeFromWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
            
        }
    }
    
   
    
    
    
    private func loadStatsData(statsDictionary: KeyValue){
        
        statsTitleArr = ["OPEN","HIGH","LOW","MKT CAP","P/E RATIO","DIV YIELD","VOL","VOL AVG","EPS","52 WEEK LOW","52 WEEK HIGH","SHARES"]
        statsValueArr = [Any]()
        statsValueArr.append(tradeDic[AppKey.open] ?? "N/A")
        statsValueArr.append(tradeDic[AppKey.tradeDayLow] ?? "N/A")
        statsValueArr.append(tradeDic[AppKey.tradeDayHigh] ?? "N/A")
        statsValueArr.append(statsDictionary[AppKey.marketCapital] ?? "N/A")
        statsValueArr.append(statsDictionary[AppKey.PERation] ?? "N/A")
        statsValueArr.append(statsDictionary[AppKey.DIVYield] ?? "N/A")
        if let tradeVolume = tradeDic[AppKey.tradeVolume] as? Double{
            statsValueArr.append("\((tradeVolume/1000000).rounded(toPlaces: 2) )M")
        }else {
            statsValueArr.append("N/A")
        }
        if let tradeAvgVolume = tradeDic[AppKey.tradeVolume] as? Double{
            statsValueArr.append("\((tradeAvgVolume/1000000).rounded(toPlaces: 2) )M")
        }else {
            statsValueArr.append("N/A")
        }
        if let esplatest = statsDictionary[AppKey.epsLatest] as? Double{
            statsValueArr.append("$\((esplatest).rounded(toPlaces: 2) )")
        }else {
            statsValueArr.append("N/A")
        }
        
        if let fiftyTwoWeekLow = statsDictionary[AppKey.fiftyTwoWeekLow] as? Double{
            statsValueArr.append("$\(fiftyTwoWeekLow)")
        }else {
            statsValueArr.append("N/A")
        }
        
        
        if let fiftyTwoWeekHigh = statsDictionary[AppKey.fiftyTwoWeekHigh] as? Double{
            statsValueArr.append("$\(fiftyTwoWeekHigh)")
        }else {
            statsValueArr.append("N/A")
        }
        
        if let shares = statsDictionary[AppKey.shares] as? Double{
           statsValueArr.append("\((shares/1000000).rounded(toPlaces: 2) )M")
        }else {
            statsValueArr.append("N/A")
        }
        print(statsValueArr)
        
        collectionStats.reloadData()
    }
    
    
    
    /*---------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------------------------------------------------------------*/
    
    //MARK: work on candlistic charts
    func loadCanldleChart(_ chartArr:Array<NSDictionary>){
        
        
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        var readFormatter = DateFormatter()
        readFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        var displayFormatter = DateFormatter()
        displayFormatter.dateFormat = "MMM yyyy"
        
        let date = {(str: String) -> Date in
            return readFormatter.date(from: str)!
        }
        
        let calendar = Calendar.current
        
        let dateWithComponents = {(day: Int, month: Int, year: Int) -> Date in
            var components = DateComponents()
            components.day = day
            components.month = month
            components.year = year
            return calendar.date(from: components)!
        }
        
        func filler(_ date: Date) -> ChartAxisValueDate {
            let filler = ChartAxisValueDate(date: date, formatter: displayFormatter)
            filler.hidden = true
            return filler
        }
        let chartPoints = getCandelisticChartValueFromSocket(chartArr)
        
        let openArr = getOpenValueArrar(chartArr)
        let diffrenceBy = (openArr.max()! - openArr.min()!)/3
        
        
        guard  diffrenceBy != 0 else {
            
            UIAlertController.show(self, "Message", "chart value  not found!")

            return
        }
        
        let yValues = stride(from: openArr.min()!-5 , through: openArr.max()!+5, by: diffrenceBy).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)}
        
        
        
        let xGeneratorDate = ChartAxisValuesGeneratorDate(unit: .day, preferredDividers:2, minSpace: 1, maxTextSize: 12)
        let xLabelGeneratorDate = ChartAxisLabelsGeneratorDate(labelSettings: labelSettings, formatter: displayFormatter)
        let firstDate = date(chartArr[0].value(forKey: AppKey.date) as! String )
        let lastDate = date(chartArr[chartArr.count-1].value(forKey: AppKey.date) as! String)
        let xModel = ChartAxisModel(firstModelValue: firstDate.timeIntervalSince1970, lastModelValue: lastDate.timeIntervalSince1970, axisTitleLabels: [ChartAxisLabel(text: "", settings: labelSettings)], axisValuesGenerator: xGeneratorDate, labelsGenerator: xLabelGeneratorDate)
        
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings.defaultVertical()))
        let chartFrame = ExamplesDefaults.chartFrame(chartView.bounds)
        
        let chartSettings = ExamplesDefaults.chartSettings // for now zoom & pan disabled, layer needs correct scaling mode.
        
        let coordsSpace = ChartCoordsSpaceRightBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
        let chartPointsLineLayer = ChartCandleStickLayer<ChartPointCandleStick>(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints, itemWidth:  5, strokeWidth:  0.6, increasingColor: UIColor.green, decreasingColor: UIColor.red)
        
        
        let settings = ChartGuideLinesLayerSettings(linesColor: UIColor.white, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)
        
        let dividersSettings =  ChartDividersLayerSettings(linesColor: UIColor.white, linesWidth: ExamplesDefaults.guidelinesWidth, start: 3, end: 0)
        let dividersLayer = ChartDividersLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: dividersSettings)
        
        let chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                dividersLayer,
                chartPointsLineLayer
            ]
        )
        
        
        chartView.addSubview(chart.view)
        self.chart = chart
        
        
    }
    
    //maping in chart value
    func getCandelisticChartValueFromSocket(_ chartArr: Array<NSDictionary>) -> Array<ChartPointCandleStick> {
        
        let readFormatter = DateFormatter()
        readFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        let displayFormatter = DateFormatter()
        displayFormatter.dateFormat = "MMM yyyy"
        
        let date = {(str: String) -> Date in
            return readFormatter.date(from: str)!
        }
        
        let jsonValues = chartArr
        
        // Parse data
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let values = jsonValues.map { (value: NSDictionary) -> ChartPointCandleStick in
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            let closeValue = (value[AppKey.close]! as! NSNumber).doubleValue
            let openValue = (value[AppKey.open]! as! NSNumber).doubleValue
            let lowValue = (value[AppKey.low]! as! NSNumber).doubleValue
            let hignValue = (value[AppKey.high]! as! NSNumber).doubleValue
            return ChartPointCandleStick(date: date(value[AppKey.date]! as! String), formatter: displayFormatter, high: hignValue, low: lowValue, open: openValue, close: closeValue)
        }
        
        return values
        
    }
    
    //maping in close value
    func getOpenValueArrar(_ chartArr: Array<NSDictionary>) -> Array<Int> {
        let jsonValues = chartArr
       
        let values = jsonValues.map { (value: NSDictionary) -> Int in
            let openValue = (value[AppKey.close]! as! NSNumber).intValue
            return openValue
        }
        
        return values
        
    }
    
   /*---------------------------------------------------------------------------------------------------------*/
   /*---------------------------------------------------------------------------------------------------------*/
    
    
    
    //MARK: work on Bar charts
    
    func loadBarChart(_ chartArr:Array<NSDictionary>){
        
        groupsData = [(title: String, [(min: Double, max: Double)])]()
        
        var validTo = 0
        for i in 0..<chartArr.count {
            if validTo < 4{
                if chartArr[i]["dateConfirmed"] as? Int == 1{
                    
                    if let epsValue = chartArr[i]["eps"] as? Double {
                        
                        validTo = validTo + 1
                        groupsData.append((title: "\((chartArr[i]["period"] as? String)!) \(String(describing :(chartArr[i]["periodYear"] as? Int)!))", [(min: -1, max: epsValue)]))
                        epsValueDic[String(describing : epsValue)] = (chartArr[i]["epsEst"] as? Double)!
                        
                        if validTo == 1{
                            lblExpectedEps.text = "$\(String(describing: (epsValue)))"
                            lblActualEps.text = "$\(String(describing: (chartArr[i]["epsEst"] as? Double)!))"
                        }
                    }
                    
                }
            }
           
            
        }
        print(epsValueDic)
        
        showChart(horizontal: false)
        if let chart = chart {
            let sideSelector = DirSelector(frame: CGRect(x: 0, y: chart.frame.origin.y + chart.frame.size.height, width: view.frame.size.width, height: sideSelectorHeight), controller: self)
            chartView.addSubview(sideSelector)
        }
        
    }
    
    
    fileprivate func barsChart(horizontal: Bool) -> Chart {
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        
        
        let groupColors = [AppColor.gainColor]
        
        let groups: [ChartPointsBarGroup] = groupsData.enumerated().map {index, entry in
            let constant = ChartAxisValueDouble(index)
            let bars = entry.1.enumerated().map {index, tuple in
                ChartBarModel(constant: constant, axisValue1: ChartAxisValueDouble(tuple.min), axisValue2: ChartAxisValueDouble(tuple.max), bgColor: groupColors[index])
            }
            return ChartPointsBarGroup(constant: constant, bars: bars)
        }
        
        let (axisValues1, axisValues2): ([ChartAxisValue], [ChartAxisValue]) = (
            stride(from: -1, through: 5, by: 1).map {ChartAxisValueDouble(Double($0), labelSettings: labelSettings)},
            [ChartAxisValueString(order: -1)] +
                groupsData.enumerated().map {index, tuple in ChartAxisValueString(tuple.0, order: index, labelSettings: labelSettings)} +
                [ChartAxisValueString(order: groupsData.count)]
        )
        let (xValues, yValues) = horizontal ? (axisValues1, axisValues2) : (axisValues2, axisValues1)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Period", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "EPS($)", settings: labelSettings.defaultVertical()))
        let frame = ExamplesDefaults.chartFrame(chartView.bounds)
        let chartFrame = chart?.frame ?? CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height - sideSelectorHeight)
        
        let chartSettings = ExamplesDefaults.chartSettingsWithPanZoom
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
        let barViewSettings = ChartBarViewSettings(animDuration: 0.5, selectionViewUpdater: ChartViewSelectorBrightness(selectedFactor: 1))
        
        let groupsLayer = ChartGroupedPlainBarsLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, groups: groups, horizontal: horizontal, barSpacing: 2, groupSpacing: 25, settings: barViewSettings, tapHandler: { tappedGroupBar /*ChartTappedGroupBar*/ in
            
            let barPoint = horizontal ? CGPoint(x: tappedGroupBar.tappedBar.view.frame.maxX, y: tappedGroupBar.tappedBar.view.frame.midY) : CGPoint(x: tappedGroupBar.tappedBar.view.frame.midX, y: tappedGroupBar.tappedBar.view.frame.minY)
            
            guard let chart = self.chart, let chartViewPoint = tappedGroupBar.layer.contentToGlobalCoordinates(barPoint) else {return}
            
            let viewPoint = CGPoint(x: chartViewPoint.x, y: chartViewPoint.y)
            
            let infoBubble = InfoBubble(point: viewPoint, preferredSize: CGSize(width: 50, height: 40), superview: self.chart!.view, text: tappedGroupBar.tappedBar.model.axisValue2.description, font: ExamplesDefaults.labelFont, textColor: UIColor.clear, bgColor: UIColor.clear, horizontal: horizontal)
            
            let anchor: CGPoint = {
                switch (horizontal, infoBubble.inverted(chart.view)) {
                case (true, true): return CGPoint(x: 1, y: 0.5)
                case (true, false): return CGPoint(x: 0, y: 0.5)
                case (false, true): return CGPoint(x: 0.5, y: 0)
                case (false, false): return CGPoint(x: 0.5, y: 1)
                }
            }()
            
            let animatorsSettings = ChartViewAnimatorsSettings(animInitSpringVelocity: 0)
            let animators = ChartViewAnimators(view: infoBubble, animators: ChartViewGrowAnimator(anchor: anchor), settings: animatorsSettings, invertSettings: animatorsSettings.withoutDamping(), onFinishInverts: {
                infoBubble.removeFromSuperview()
            })
            
            chart.view.addSubview(infoBubble)
            
            
            print(Double(tappedGroupBar.tappedBar.model.axisValue2.description) ?? 0.0)
            self.lblExpectedEps.text = "$\(String(describing : Double(tappedGroupBar.tappedBar.model.axisValue2.description)!.rounded(toPlaces: 2) ))"
            print(String(describing : Double(tappedGroupBar.tappedBar.model.axisValue2.description)!.rounded(toPlaces: 2) ))
            
            self.lblActualEps.text! = "$\(String(describing : self.epsValueDic[String(describing : Double(tappedGroupBar.tappedBar.model.axisValue2.description)!.rounded(toPlaces: 2) )]!.rounded(toPlaces: 2)))"
            
            
            infoBubble.tapHandler = {
                animators.invert()
            }
            
            animators.animate()
        })
        
        let guidelinesSettings = ChartGuideLinesLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, axis: horizontal ? .x : .y, settings: guidelinesSettings)
        
        return Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                groupsLayer
            ]
        )
    }
    
    fileprivate func showChart(horizontal: Bool) {
        self.chart?.clearView()
        
        let chart = barsChart(horizontal: horizontal)
        chartView.addSubview(chart.view)
        self.chart = chart
    }
    
    
    
    class DirSelector: UIView {
        
        let horizontal: UIButton
        let vertical: UIButton
        
        weak var controller: StatsVC?
        
        fileprivate let buttonDirs: [UIButton : Bool]
        
        init(frame: CGRect, controller: StatsVC) {
            
            self.controller = controller
            
            horizontal = UIButton()
            horizontal.setTitle("Horizontal", for: UIControlState())
            vertical = UIButton()
            vertical.setTitle("Vertical", for: UIControlState())
            
            buttonDirs = [horizontal : true, vertical : false]
            
            super.init(frame: frame)
            
            addSubview(horizontal)
            addSubview(vertical)
            
            for button in [horizontal, vertical] {
                button.titleLabel?.font = ExamplesDefaults.fontWithSize(14)
                button.setTitleColor(UIColor.clear, for: UIControlState())
                button.addTarget(self, action: #selector(DirSelector.buttonTapped(_:)), for: .touchUpInside)
            }
        }
        
        @objc func buttonTapped(_ sender: UIButton) {
            let horizontal = sender == self.horizontal ? true : false
            controller?.showChart(horizontal: horizontal)
        }
        
        override func didMoveToSuperview() {
            let views = [horizontal, vertical]
            for v in views {
                v.translatesAutoresizingMaskIntoConstraints = false
            }
            
            let namedViews = views.enumerated().map{index, view in
                ("v\(index)", view)
            }
            
            var viewsDict = Dictionary<String, UIView>()
            for namedView in namedViews {
                viewsDict[namedView.0] = namedView.1
            }
            
            let buttonsSpace: CGFloat = Env.iPad ? 20 : 10
            
            let hConstraintStr = namedViews.reduce("H:|") {str, tuple in
                "\(str)-(\(buttonsSpace))-[\(tuple.0)]"
            }
            
            let vConstraits = namedViews.flatMap {NSLayoutConstraint.constraints(withVisualFormat: "V:|[\($0.0)]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)}
            
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: hConstraintStr, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
                + vConstraits)
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    

    
    /*---------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------------------------------------------------------------*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menuAction(_ sender: Any) {
        //unsubscribe company
        WebSocketManager.sharedInstance.unSubscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)
        UserDefaults.standard.set(false, forKey: "isStats")
        self.navigationController?.popViewController(animated: true)
    }

}

extension StatsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return statsValueArr.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as? StatsCell{
            cell.lblValue.text = String(describing: statsValueArr[indexPath.row])
            cell.lblTitle.text = statsTitleArr[indexPath.row]
            if  indexPath.row == 1{
                cell.lblValue.textColor = AppColor.gainColor
            }
            if  indexPath.row == 2{
                cell.lblValue.textColor = AppColor.lossColor
            }
        
           return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        //let yourHeight = 50.0
        
        return CGSize(width: yourWidth, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0,0,0,0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
