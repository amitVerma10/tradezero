//
//  InfoVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/20/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class InfoVC: BaseVC {
    
    @IBOutlet weak var likeButton: UIBarButtonItem!
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "InfoVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addtoWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.addToWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFromWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil)
        
        if inWatchList
        {
            likeButton.tintColor = AppColor.gainColor
        }else{
            likeButton.tintColor = .white
        }
        

        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
   
    }
    
    //MARK: Add to watchList
    @objc func addtoWatchList(_ notification: NSNotification){
        inWatchList = true
        
        likeButton.tintColor = AppColor.gainColor
        
    }
    
    @objc func removeFromWatchList(_ notification: NSNotification){
        
        inWatchList = false
        likeButton.tintColor = .white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menuAction(_ sender: Any) {
        //unsubscribe company
        WebSocketManager.sharedInstance.unSubscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)
        UserDefaults.standard.set(false, forKey: "isStats")
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func likeAction(_ sender: Any) {
        //for testing perpose
        
        if inWatchList == false{
            WebSocketManager.sharedInstance.addToWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
        }else{
            WebSocketManager.sharedInstance.removeFromWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
            
        }
    }
    

}
