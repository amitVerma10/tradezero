//
//  NewsVC.swift
//  TradeZero
//
//  Created by Amit Verma on 11/20/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class NewsVC: BaseVC {
    
    @IBOutlet weak var likeButton: UIBarButtonItem!
    //table view outlets
    @IBOutlet weak var tblNews: UITableView!
    
    var newsArray = [KeyValue]()
    
    override class func identifier() -> String {
        return "NewsVC"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblNews.estimatedRowHeight = 91
        tblNews.rowHeight = UITableViewAutomaticDimension
        tblNews.tableFooterView = UIView()
        likeButton.isEnabled = false
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//News
            dic["rt"] = "News" as AnyObject
            dic["sym"] = CompanyVC.compDic[AppKey.companySym] as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.newsData(_:)), name: NSNotification.Name(rawValue: SocketConstants.newsDataRecived), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.addtoWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.addToWatchList), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFromWatchList(_:)), name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil)
        
        if inWatchList
        {
            likeButton.tintColor = AppColor.gainColor
        }else{
            likeButton.tintColor = .white
        }
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Add to watchList
    @objc func addtoWatchList(_ notification: NSNotification){
        if let status = notification.userInfo!["status"]as? String
        {
            if status == "MaxLimitReached"{
                
                UIAlertController.show(self, AppTitle.appTital, "Maximum limit has been reached for add in watchlist.")
                
            }else{
                inWatchList = true
                 likeButton.tintColor = AppColor.gainColor
            }
        }
        
    }
    
    @objc func removeFromWatchList(_ notification: NSNotification){
        
        inWatchList = false
        likeButton.tintColor = .white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // handle notification
    @IBAction func menuAction(_ sender: Any) {
       //unsubscribe company
        WebSocketManager.sharedInstance.unSubscribeCompany(symbol: CompanyVC.compDic[AppKey.companySym] as! String)
        UserDefaults.standard.set(false, forKey: "isStats")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func newsData(_ notification: NSNotification){
        guard let newsArr = notification.userInfo!["articles"] as? [KeyValue],newsArr.count > 0 else{
            UIAlertController.show(self, "Message", "No News found!")
            return
        }
        likeButton.isEnabled = true
        newsArray = newsArr
        tblNews.reloadData()
    }
    
    @IBAction func likeAction(_ sender: Any) {
        
        //for testing perpose
        
        if inWatchList == false{
            WebSocketManager.sharedInstance.addToWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
        }else{
            WebSocketManager.sharedInstance.removeFromWatchList(symbolName: CompanyVC.compDic[AppKey.companySym] as! String, name: userAccountId)
            
            
        }
    }
    
}

extension NewsVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? NewsCell{
            cell.selectionStyle = .none
            cell.lblNews.text = newsArray[indexPath.row]["t"] as? String ?? ""
            cell.lblChanel.text = newsArray[indexPath.row]["a"] as? String ?? ""
            cell.lblAbout.text = ""//data not recived
            
            if let timeStamp =  newsArray[indexPath.row]["c"] as? Int{
                let timeString = String(timeStamp)
                cell.lblTime.text = timeString.convertTimeStampToDate()
            }
            
            return cell
        }
        return UITableViewCell()
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: newsArray[indexPath.row]["url"] as? String ?? "http://www.google.com"),(newsArray[indexPath.row]["hl"] as? Bool)!  else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
