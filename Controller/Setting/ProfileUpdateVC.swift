//
//  ProfileUpdateVC.swift
//  TradeZero
//
//  Created by Amit Verma on 4/5/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

// enum for Sign Up screen textfield
fileprivate enum SignUpField: Int {
    case email = 100
}

class ProfileUpdateVC: BaseVC {
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "ProfileUpdateVC"
    }
    
    let updateArr = ["Email Address","Mobile Number"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func _isValidRequest(field: SignUpField, textField: UITextField) -> Bool {
        switch field {
        case .email:
            if textField.text?.length() == 0 {
                UIAlertController.show(self, KAlertTitle, KEnterEmail)
                return false
            }
            if !(textField.text?.isValidEmail())! {
                
                let alertController = UIAlertController(title: KAlertTitle, message: KEnterValidEmail, preferredStyle: .alert)
                let saveAction = UIAlertAction(title: "OK", style: .destructive, handler: {
                    alert -> Void in
                    self.showEmailAddressAlert()
                })
                
                alertController.addAction(saveAction)
                self.present(alertController, animated: true, completion: nil)
                
                
                //
                UIAlertController.show(self, KAlertTitle, KEnterValidEmail)
                return false
            }
            return true
            
        }
    }
    

}

extension ProfileUpdateVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         return updateArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingCell)
        cell.selectionStyle = .none
        
        cell.lblTitle.text = updateArr[indexPath.row]
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.sectionHeaderHeight))
        let lbl = UILabel.init(frame: CGRect(x: 18, y: 15, width: tableView.frame.size.width, height: tableView.sectionHeaderHeight))
        
        lbl.text = "Update"
        
        lbl.textColor = UIColor.color(70, 195, 128)
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.backgroundColor = .clear
        view.backgroundColor = .clear
        view.addSubview(lbl)
        // Do your customization
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            if indexPath.row == 0{
                 self.showEmailAddressAlert()
            }
        
    }
    
    private func showEmailAddressAlert(){
        
        
        let alertController = UIAlertController(title: AppTitle.appTital, message: "Please Fill Your Email Address!", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "OK", style: .destructive, handler: {
            alert -> Void in
            
            let userNameTextField = alertController.textFields![0] as UITextField
            guard let emailField = userNameTextField.text , self._isValidRequest(field: .email, textField: userNameTextField) else {
                
                return
            }
            
            //self.forgetPasswordAPI(userNameTextField.text!)
            
            print("firstName \(String(describing: emailField))")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email Address"
            textField.autocapitalizationType = UITextAutocapitalizationType.allCharacters//set key board to capital laters
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
}
