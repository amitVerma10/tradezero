//
//  SettingVC.swift
//  TradeZero
//
//  Created by Amit Verma on 12/1/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import TOPasscodeViewController

class SettingVC: BaseVC {
    
    
    
    //variables
    var passcode = String()
    let style = TOPasscodeViewStyle(rawValue: 0)
    var type = TOPasscodeType(rawValue: 0)
    
    //let deviceArr = ["Security","Two-Factor Authentication","Notification"]
    //let accountArr = ["Update Account","Investment Profile"]
    
    let deviceArr = ["Security"]
    let accountArr = [String]()
    
    //Outlets
    @IBOutlet weak var tblSetting: UITableView!
    
    
    // MARK:  View Controller Identifier
    override class func identifier() -> String {
        return "SettingVC"
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        tblSetting.sectionHeaderHeight = 40
        tblSetting.isScrollEnabled = false

        // Do any additional setup after loading the view.
    }

    @IBAction func menuButtonAction(_ sender: Any) {
        slideMenuController()?.openLeft()
    }
    @IBAction func logOutAction(_ sender: Any) {
        isUserlogin = false
        userAccountId = ""
        KAppDelegate.lououtAction()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SettingVC : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return deviceArr.count
        }else{
            return accountArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingCell)
        cell.selectionStyle = .none
        
        if indexPath.section == 0{
            cell.lblTitle.text = deviceArr[indexPath.row]
        }else{
            cell.lblTitle.text = accountArr[indexPath.row]
        }
        
        cell.btnChangePin.isHidden = false
        cell.switch.isHidden = false
        
        guard indexPath.section == 0 && (indexPath.row == 1 || indexPath.row == 0) else{
            cell.btnChangePin.isHidden = true
            cell.switch.isHidden = true
            return cell
        }
        if indexPath.section == 0 && indexPath.row == 0{
            
            cell.btnChangePin.addTarget(self, action: #selector(btnChangePinAction(_:)), for: .touchUpInside)
            cell.btnChangePin.tag = indexPath.row
            
            if UserDefaults.standard.bool(forKey: "pinCodeOn"){
                cell.switch.isOn = true
            }else{
                cell.switch.isOn = false
            }
        }else{
            cell.btnChangePin.isHidden = true
        }
        
        
        cell.switch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        cell.switch.tag = indexPath.row
        
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.sectionHeaderHeight))
        let lbl = UILabel.init(frame: CGRect(x: 18, y: 15, width: tableView.frame.size.width, height: tableView.sectionHeaderHeight))
        if section == 0{
            lbl.text = "Device"
        }else{
            lbl.text = "Account"
        }
        lbl.textColor = UIColor.color(70, 195, 128)
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.backgroundColor = .clear
        view.backgroundColor = .clear
        view.addSubview(lbl)
        // Do your customization
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 0{
               self.navigationController?.pushViewController(profileUpdateVC(), animated: true)
            }
        }
    }
    @objc func btnChangePinAction(_ sender: UIButton) {
        let settingsController = TOPasscodeSettingsViewController()
        settingsController.passcodeType = type!
        settingsController.delegate = self
        settingsController.requireCurrentPasscode = false
        navigationController?.pushViewController(settingsController, animated: true)
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if sender.tag == 0{
            if sender.isOn{
                UserDefaults.standard.set(true, forKey: "pinCodeOn")
            }else{
                UserDefaults.standard.set(false, forKey: "pinCodeOn")
            }
        }
    }
    
}
extension SettingVC :  TOPasscodeSettingsViewControllerDelegate{
    
    func passcodeSettingsViewController(_ passcodeSettingsViewController: TOPasscodeSettingsViewController, didAttemptCurrentPasscode passcode: String) -> Bool {
        return passcode == self.passcode
    }
    
    func passcodeSettingsViewController(_ passcodeSettingsViewController: TOPasscodeSettingsViewController, didChangeToNewPasscode passcode: String, of type: TOPasscodeType) {
        UserDefaults.standard.set(passcode, forKey: "passcode")
        self.passcode = passcode
        self.type = type
        navigationController?.popViewController(animated: true)
    }

}
