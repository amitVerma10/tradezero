//
//  FMConnectionManager.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

public enum Method: String {
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}

public enum ResponseStatus:Int {
    case tokenExpired = -3
    case networkNotAvailable
    case unknownError
    case failed
    case success
}

public enum Type:Int {
    case png
    case jpeg
    case video
}

fileprivate enum ResponseCode:Int {
    case success = 200
    case tokenExpired = 4000
}

typealias responseHandler = (_ response: [String: Any]?, _ status: ResponseStatus, _ error: Error?) -> Void

public class ConnectionManager: NSObject {
    
    static let shared = ConnectionManager()
    
    //MARK:  get Access token
    func getAccessTocken(_ method: Method = .POST, _ urlString: String, _ parameters: [String: Any]? = nil, _ authentication: Bool = false, _ onCompletion: @escaping responseHandler){
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let loginStr = parameters!["login"] as! String
        let passwordStr = parameters!["password"] as! String
        
        let postData = NSMutableData(data: "login=\(loginStr)".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(passwordStr)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            //let httpResponse = response as? HTTPURLResponse
            
            guard let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == ResponseCode.success.rawValue else {
                if let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == ResponseCode.tokenExpired.rawValue {
                    onCompletion(nil, .tokenExpired, error)
                    return
                }
                onCompletion(nil, .unknownError, error)
                return
            }
            guard let responseData = data else {
                onCompletion(nil, .unknownError, error)
                return
            }
            
            if (error != nil) {
                //print(error)
            } else {
                do {
                    if let returnData = String(data: data!, encoding: .utf8) {
                        onCompletion(["token":returnData], .success, nil)
                    }
                }
            }

        }
        dataTask.resume()
        
    }
    
    //MARK:  validate OTP
    func validateOTP(_ method: Method = .POST, _ urlString: String, _ parameters: [String: Any]? = nil, _ authentication: Bool = false, _ onCompletion: @escaping responseHandler){
        
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let requestIdStr = parameters!["requestID"] as! String
        var codeStr = String(parameters!["code"] as! Int)
        if codeStr.length() == 3{
            codeStr = "0\(codeStr)"
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(urlString)requestID=\(requestIdStr)&code=\(codeStr)&type=")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                onCompletion(nil, .unknownError, error)
            } else {
                self._handleResponse(data, response, error, onCompletion)
            }
            
        })
        
        dataTask.resume()
        
    }
    
    
    
    
    //MARK:  sendUrlIn header
    func sendRequestInHeader(method : Method , urlString:String,parameters:[String:Any]? = nil, authentication: Bool = false , onCompletion: @escaping responseHandler ){
        
        if isInternetReachable() {
            guard let url = URL(string: urlString) else {
                return
            }
            
            let headers = [
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                ]
            
            let request = NSMutableURLRequest(url: url)
            request.timeoutInterval = 30.0
            request.httpMethod = method.rawValue
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    onCompletion(nil, .unknownError, error)
                } else {
                    self._handleResponse(data, response, error, onCompletion)
                }
                
            })
            
            dataTask.resume()
            
            
        }
        else {
            onCompletion(nil, .networkNotAvailable, nil)
            
        }

    }
    
    
    //MARK:  Send Request
    func sendRequest(_ method: Method = .POST, _ urlString: String, _ parameters: [String: Any]? = nil, _ authentication: Bool = false, _ onCompletion: @escaping responseHandler) {
        
        if isInternetReachable() {
            _jsonRequest(urlString, parameters)
            guard let url = URL(string: urlString) else {
                return
            }
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = method.rawValue
            do {
                if let requestParameter = parameters {
                    let data = try JSONSerialization.data(withJSONObject: requestParameter, options: JSONSerialization.WritingOptions())
                    let postLength = NSString(format: "%ld", data.count)
                    request.httpBody = data
                    request.addValue(postLength as String, forHTTPHeaderField: "Content-Length")
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.timeoutInterval = 120.0
                
            }
            catch {
                let encodingError = error as NSError
                print("Error could not parse JSON: \(encodingError)")
            }
            
            let task = _session(authentication).dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                self._handleResponse(data, response, error, onCompletion)
            })
            task.resume()
        }
        else {
            onCompletion(nil, .networkNotAvailable, nil)
        }
    }
    
    //MARK:  Send Request With Multipart
    func sendMultipartRequest(_ method: Method = .POST, _ urlString: String, _ parameters: [String: Any]? = nil, _ imagesData: [Any]? = nil, _ type: Type = .jpeg,_ authentication: Bool = false, _ onCompletion: @escaping responseHandler) {
        
        if isInternetReachable() {
            _jsonRequest(urlString, parameters)
            guard let url = URL(string: urlString) else {
                return
            }
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = method.rawValue
            let boundary = _generateBoundaryString()
            let postData =  _createBodyWithParameters(parameters, imagesData, type, boundary)
            request.httpBody = postData as Data
            let postLength = NSString(format: "%ld", (postData as Data).count)
            request.addValue(postLength as String, forHTTPHeaderField: "Content-Length")
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            request.timeoutInterval = 120.0
            let task = _session(authentication).dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                self._handleResponse(data, response, error, onCompletion)
            })
            task.resume()
        }
        else {
            onCompletion(nil, .networkNotAvailable, nil)
        }
    }
    
    //MARK:  Private Methods
    private func _session(_ authentication: Bool = false) -> URLSession {
        if authentication {
            let sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default
            return URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        }
        else {
            return URLSession.shared
        }
    }
    
    private func _jsonRequest(_ urlString: String, _ parameters: [String: Any]? = nil) {
        do {
            if let requestParameter = parameters {
                let jsonData = try JSONSerialization.data(withJSONObject: requestParameter , options: JSONSerialization.WritingOptions.prettyPrinted)
                
                if let jsonRequest = NSString(data: jsonData,
                                              encoding: String.Encoding.ascii.rawValue) {
                    print("JSON Request: \(jsonRequest)")
                }
            }
            print("URL : \(urlString)")
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    private func _handleResponse(_ data: Data?, _ response: URLResponse?, _ error: Error?, _ onCompletion: @escaping responseHandler) {
        
        DispatchQueue.main.async {
            guard error == nil else {
                onCompletion(nil, .unknownError, error)
                return
            }
            do {
                
                guard let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == ResponseCode.success.rawValue else {
                    if let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == ResponseCode.tokenExpired.rawValue {
                        onCompletion(nil, .tokenExpired, error)
                        return
                    }
                    onCompletion(nil, .unknownError, error)
                    return
                }
                guard let responseData = data else {
                    onCompletion(nil, .unknownError, error)
                    return
                }
                guard let responseDictionary = try JSONSerialization.jsonObject(with: responseData, options: .mutableLeaves) as? NSDictionary else {
                    onCompletion(nil, .unknownError, error)
                    return
                }
                print("Response:\(responseDictionary)")
                guard let status = responseDictionary.value(forKey: "status") as? Int, status == 0  else {
                    if let response = responseDictionary as? [String : Any] {
                        onCompletion(response, .failed , nil)
                    }
                    return
                }
                if let response = responseDictionary as? [String : Any] {
                    print(response)
                    onCompletion(response, .success , nil)
                }
            } catch {
                onCompletion(nil, .unknownError, error)
            }
        }
    }
    
    private func _createBodyWithParameters(_ parameters: [String: Any]?,_ imagesData: [Any]?,_ type: Type, _ boundary: String) -> NSData {
        
        let body = NSMutableData()
        if let requestParameter = parameters {
            for (key, value) in requestParameter {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        if let dataArray = imagesData {
            for imageData in  dataArray {
                if let imageDatas = imageData as? [String: Data] {
                    for (key, data) in imageDatas {
                        let type = type == .jpeg ? "jpeg" : type == .png ? "png" : "mov"
                        let filename = "\(key).\(type)"
                        let mimetype = "image/\(type)"
                        body.appendString("--\(boundary)\r\n")
                        body.appendString("Content-Disposition: form-data; image=\"\(key)\"; filename=\"\(filename)\"\r\n")
                        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
                        body.append(data)
                    }
                }
            }
        }
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        return body
    }
    
    private func _generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

extension ConnectionManager: URLSessionDelegate {
    
    //MARK:  URLSession delegate
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        
        if challenge.previousFailureCount > 0 {
            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
        else {
            // Set username and password for authentication
            let credential = URLCredential(user:"", password:"", persistence: .forSession)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential,credential)
        }
    }
    
    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if let err = error {
            print("Error: \(err.localizedDescription)")
        } else {
            print("Error. Giving up")
        }
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        if let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true) {
            append(data)
        }
    }
}
