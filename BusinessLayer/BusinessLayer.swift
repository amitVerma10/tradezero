//
//  FMBusinessLayer.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class BusinessLayer: NSObject {
    
    typealias handler = (_ response: [String: Any]?, _ status: ResponseStatus, _ error: Error?) -> Void
    
    // Sign in
    
    // get access Token
    static func accessToken(_ login : String, _ password: String, _ handler: @escaping handler){
        ConnectionManager.shared.getAccessTocken(.POST, API.accessToken, Request.token(login, password), false) { (response, status, error) in
            switch status {
            case .success:
                if let tokenStr = response?[AppKey.token] as? String {
                    print(tokenStr)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    // Log In
    static func login(_ email: String, _ password: String, _ handler: @escaping handler) {
        let params = [AppKey.email: email, AppKey.password: password]
        ConnectionManager.shared.sendRequest(.POST, API.signIn, params) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
    }
    
    // send post data request address
    static func sendRequest(urlStr:String,_ handler: @escaping handler){
        ConnectionManager.shared.sendRequestInHeader(method: .POST, urlString: urlStr, parameters: nil, authentication: false) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
    }
    
    // send GET data request 
    static func sendGetRequest(urlStr:String,_ handler: @escaping handler){
        ConnectionManager.shared.sendRequestInHeader(method: .GET, urlString: urlStr, parameters: nil, authentication: false) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
    }
    
    
    
    
    // Sign Up
    static func signUp(_ signUpDic: [String:Any], _ handler: @escaping handler) {
       ////http://tradezero.co:2701/api/SignProcess/createSignupStep
        ConnectionManager.shared.sendRequest(.POST,"\(API.signUp)/createSignupStep" , Request.signUp(signUpDic[AppKey.firstName] as! String, signUpDic[AppKey.lastName] as! String, signUpDic[AppKey.dateOfBirth] as! String,signUpDic[AppKey.email] as! String, signUpDic[AppKey.password] as! String, signUpDic[AppKey.phoneNumber] as! String)) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    // SignUp with Residential Details step 2
    static func signUpWithResidential(_ signUpDic: [String:Any],_ url:String, _ handler: @escaping handler) {
        
        ConnectionManager.shared.sendRequest(.POST, url, Request.signUpWithResidential(signUpDic)) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    // SignUp with SSNnumberVerification step 3
    static func signUpWithSSNAndStep3(_ signUpDic: [String:Any],_ url:String, _ handler: @escaping handler) {
        
        ConnectionManager.shared.sendRequest(.POST, url, Request.ssnRequestDic(signUpDic)) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    
    // SignUp with SSNnumberVerification step 3
    static func signUpWithFinancialAndGoal(_ signUpDic: [String:Any],_ url:String, _ handler: @escaping handler) {
        
        ConnectionManager.shared.sendRequest(.POST, url, Request.finacialGoalDic(signUpDic)) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    
    // SignUp with SSNnumberVerification step 5
    static func signUpWithTermCodition(_ signUpDic: [String:Any],_ url:String, _ handler: @escaping handler) {
        
        ConnectionManager.shared.sendRequest(.POST, url, Request.termCoditionRequestDic(signUpDic)) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    
    
    static func checkOtp(_ signUpDic:[String : Any], _ handler: @escaping handler ){
        //http://tradezero.co:2701/api/SignProcess/OTPVerification?requestID=0fbbb6eac6714676b10330bc1c148093&code=1584&type=
        
        ConnectionManager.shared.validateOTP(.POST, BaseURL+"SignProcess/OTPVerification?", Request.signUpOTP(requestIdOtp: signUpDic[AppKey.requestId] as? String ?? "0000" , codeOtp: signUpDic[AppKey.codeOTP] as? Int ?? 0000), false) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response,  status, error)
            }
        }
        
    }
    
    // forgot Password
    static func forgotPassword(_ email: String, _ responseHandler: @escaping (_ response: [String: Any]?, _ status: ResponseStatus, _ error: Error?) -> Void) {
        ConnectionManager.shared.sendRequest(.POST, API.forgotPassword,Request.forgotPassword(email)) { (response, status, error) in
            responseHandler(response, status, error)
        }
    }
    
    
    // logout
    static func logout(_ userId: String,_ accessToken: String, _ responseHandler: @escaping (_ response: [String: Any]?, _ status: ResponseStatus, _ error: Error?) -> Void) {
        ConnectionManager.shared.sendRequest(.POST, API.logout,Request.logout(userId)) { (response, status, error) in
            responseHandler(response, status, error)
        }
    }
    
    // Sign Up with document upload
     
    static func signUpDocumentUpload(_ url:String, _ imageData: [Any], _ handler: @escaping handler) {
        ConnectionManager.shared.sendMultipartRequest(.POST, url, nil, imageData, .jpeg, false) { (response, status, error) in
            switch status {
            case .success:
                if let dictionary = response?[AppKey.data] as? KeyValue {
                    print(dictionary)
                    handler(response, status, error)
                    return
                }
                handler(response, status, error)
            default:
                handler(response, status, error)
            }
        }
    }
    
    
    
    
 
    
}
