//
//  CountryCell.swift
//  TradeZero
//
//  Created by Amit Verma on 12/13/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    

    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
