//
//  OpenOrderLimitCell.swift
//  TradeZero
//
//  Created by Amit Verma on 3/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OpenOrderLimitCell: UITableViewCell {
    
    @IBOutlet weak var lblSymbol: UILabel!
    @IBOutlet weak var lblShares: UILabel!
    @IBOutlet weak var lblSide: UILabel!
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var lblTimeInForce: UILabel!
    @IBOutlet weak var lblBid: UILabel!
    @IBOutlet weak var lblAsk: UILabel!
    @IBOutlet weak var lblRoute: UILabel!
    @IBOutlet weak var lblLimitPrice: UILabel!
    @IBOutlet weak var lblStopPrice: UILabel!
    @IBOutlet weak var lblLastPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
