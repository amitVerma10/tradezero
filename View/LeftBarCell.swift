//
//  LeftBarCell.swift
//  TradeZero
//
//  Created by Amit Verma on 11/30/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class LeftBarCell: UITableViewCell {

    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
