//
//  AddressCell.swift
//  TradeZero
//
//  Created by Amit Verma on 12/6/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
