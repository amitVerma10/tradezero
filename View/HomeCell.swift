//
//  HomeCell.swift
//  TradeZero
//
//  Created by Amit Verma on 11/22/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import SwiftChart

class HomeCell: UITableViewCell ,ChartDelegate{
    
    
    
    @IBOutlet weak var labelLeadingMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var chart: Chart!
    
    
    @IBOutlet weak var imgUpDown: UIImageView!
    @IBOutlet weak var lblSymbol: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblBid: UILabel!
    @IBOutlet weak var lblAsk: UILabel!
    @IBOutlet weak var lblVolume: UILabel!
    
    
     fileprivate var labelLeadingMarginInitialConstant: CGFloat!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func callCellMethod(_ isGreen:Bool){
        
        DispatchQueue.global(qos: .background).sync {
            self.initializeChart(isGreen)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initializeChart(_ isGreen:Bool) {
        chart.delegate = self
        chart.removeAllSeries()
        // Simple chart
        let data = [(x: 0.0, y: 0), (x: 3, y: 2.5), (x: 4, y: 2), (x: 5, y: 2.3), (x: 7, y: 3), (x: 8, y: 2.2), (x: 9, y: 2.5)]
        let series = ChartSeries(data: data)
        series.area = true
        chart.xLabels = [0, 3, 6, 9, 12, 15, 18, 21, 24]
        chart.xLabelsFormatter = { String(Int(round($1))) + "h" }
        chart.add(series)
        if isGreen == true{
            series.color = UIColor(red: 49/255, green: 202/255, blue: 150/255, alpha: 1.00)
        }else{
            series.color = UIColor(red: 247/255, green: 75/255, blue: 73/255, alpha: 1.00)
        }
        
        
    }
    // Chart delegate
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Float, left: CGFloat) {
        
        for (seriesIndex, dataIndex) in indexes.enumerated() {
            if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
                print("Touched series: \(seriesIndex): data index: \(dataIndex!); series value: \(value); x-axis value: \(x) (from left: \(left))")
            }
        }
        
    }
    
    func didFinishTouchingChart(_ chart: Chart) {

    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
    
   
 
    


}
