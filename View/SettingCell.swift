//
//  SettingCell.swift
//  TradeZero
//
//  Created by Amit Verma on 12/1/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnChangePin: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
