//
//  PositionCell.swift
//  TradeZero
//
//  Created by Amit Verma on 3/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class PositionCell: UITableViewCell {
    
    @IBOutlet weak var lblSymbol: UILabel!
    @IBOutlet weak var lblLastPrice: UILabel!
    @IBOutlet weak var lblShares: UILabel!
    @IBOutlet weak var lblEntryPrice: UILabel!
    @IBOutlet weak var lblCurrentPrice: UILabel!
    @IBOutlet weak var lblPL: UILabel!
    @IBOutlet weak var lblEntryDate: UILabel!
    @IBOutlet weak var lblEntryTime: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
