//
//  CustomTable.swift
//  TradeZero
//
//  Created by Amit Verma on 12/13/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
@IBDesignable

class CustomTable: UITableView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

}
