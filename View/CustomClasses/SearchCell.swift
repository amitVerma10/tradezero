//
//  SearchCell.swift
//  TradeZero
//
//  Created by Amit Verma on 1/8/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var lblCompanyName: UILabel!
    
    @IBOutlet weak var lblCompanySym: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
