//
//  LinkedAccountCell.swift
//  TradeZero
//
//  Created by Amit Verma on 2/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class LinkedAccountCell: UITableViewCell {
    
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblAccountId: UILabel!
    @IBOutlet weak var btnSelected: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
