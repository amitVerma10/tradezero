//
//  NewsCell.swift
//  TradeZero
//
//  Created by Amit Verma on 12/25/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var lblNews: UILabel!
    @IBOutlet weak var lblChanel: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
