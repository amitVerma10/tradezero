//
//  CompanyCell.swift
//  TradeZero
//
//  Created by Amit Verma on 1/3/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class CompanyCell: UICollectionViewCell {
    
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
