//
//  LocateCellTwo.swift
//  TradeZero
//
//  Created by Amit Verma on 1/16/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class LocateCellTwo: UITableViewCell {
    
    
    @IBOutlet weak var lblSymbol: UILabel!
    @IBOutlet weak var lblAccount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblShares: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    @IBOutlet weak var btnAccept: CustomButton!
    @IBOutlet weak var btnReject: CustomButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
