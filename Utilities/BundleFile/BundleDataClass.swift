//
//  BundleDataClass.swift
//  TradeZero
//
//  Created by Amit Verma on 12/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class BundleDataClass: NSObject {
    
    static func getCountryList() -> [Any]{
        if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [Any] {
                    return jsonResult
                }
            } catch {
                return [Any]()
            }
        } else {
            print("Invalid filename/path.")
            return [Any]()
        }
        return [Any]()
    }
    
    static func getStateListOfUSAList() -> [Any]{
        if let path = Bundle.main.path(forResource: "states_titlecase", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [Any] {
                    return jsonResult
                }
            } catch {
                return [Any]()
            }
        } else {
            print("Invalid filename/path.")
            return [Any]()
        }
        return [Any]()
    }
    

}
