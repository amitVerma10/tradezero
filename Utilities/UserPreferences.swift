//
//  UserPreferences.swift
//  FiggoApp
//
//  Created by Amit Verma on 9/11/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

class UserPreferences: NSObject {
    
    private static let deviceToken          =      "deviceToken"
    private static let loggedInUserDetails  =      "loggedInUserDetails"
    private static let signUpUserDetails    =      "signUpUserDetails"
    private static let accessToken          =      "accessToken"
    private static let creditCard           =      "creditCard"
    private static let language             =      "language"


    // acceess token : used for authentication
    static func getAccessToken() -> String? {
        return AppUserDefaults.string(forKey: accessToken)
    }
    static func setAccessToken(_ stringValue: String?) {
        if let password = stringValue {
            AppUserDefaults.set(password, forKey: accessToken)
        }
        else {
            AppUserDefaults.removeObject(forKey: accessToken)
        }
        AppUserDefaults.synchronize()
    }

   
    
    // device token
    static func getDeviceToken() -> String? {
        return AppUserDefaults.string(forKey: deviceToken)
    }
    
    static func setDeviceToken(_ token: String?) {
        if let token = token {
            AppUserDefaults.set(token, forKey: deviceToken)
        }
        else {
            AppUserDefaults.removeObject(forKey: deviceToken)
        }
        AppUserDefaults.synchronize()
    }
    
    // User Details
    static func setUserDetails(_ values: KeyValue?) {
        if let details = values {
            AppUserDefaults.set(details, forKey: loggedInUserDetails)
        }
        else {
            AppUserDefaults.removeObject(forKey: loggedInUserDetails)
        }
        AppUserDefaults.synchronize()
    }
    static func getUserDetails() -> AppUser? {
        if let dictionary = AppUserDefaults.value(forKey: loggedInUserDetails) as? KeyValue {
            let user = AppUser(dictionary)
            return user
        }
        return AppUser()
    }
    
    // SignUp Details
    static func setSignUpUserDetail(_ values: KeyValue?){
        if let details = values{
            AppUserDefaults.setValue(details, forKey: signUpUserDetails)
        }else{
            AppUserDefaults.removeObject(forKey: signUpUserDetails)
        }
        AppUserDefaults.synchronize()
    }
    
    static func getSignUpUserDetails() -> KeyValue{
        if let dictionary = AppUserDefaults.dictionary(forKey: signUpUserDetails){
            return dictionary
        }
        return KeyValue()
    }

    // User Details
    static func setCardDetails(_ values: KeyValue?) {
        if let details = values {
            AppUserDefaults.set(details, forKey: creditCard)
        }
        else {
            AppUserDefaults.removeObject(forKey: creditCard)
        }
        AppUserDefaults.synchronize()
    }
   /* static func getCardDetails() -> CreditCard? {
        if let dictionary = AppUserDefaults.value(forKey: creditCard) as? KeyValue {
            return CreditCard(dictionary)
        }
        return CreditCard()
    }*/

    // language
    static func getLanguage() -> String {
        if let language = AppUserDefaults.string(forKey: language), language.length() > 0 {
            return language
        }
         let languages = Locale.preferredLanguages
         if languages.count > 0 {
            let prefferedLanguage = languages[0] as String //en-US
            let arr = prefferedLanguage.components(separatedBy: "-")
            let deviceLanguage = arr.first //en
            if deviceLanguage == "es" {
                return KSpanish
            }
            return KEnglish
        }
        return KEnglish
    }
    
    static func setLanguage(_ stringValue: String?) {
        if let selectedlanguage = stringValue {
            AppUserDefaults.set(selectedlanguage, forKey: language)
        }
        else {
            AppUserDefaults.removeObject(forKey: language)
        }
        AppUserDefaults.synchronize()
    }

}
