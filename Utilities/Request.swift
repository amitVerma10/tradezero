//
//  Request.swift
//  FiggoApp
//
//  Created by Amit Verma on 9/11/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// local
//public let BaseURL = "http://108.168.203.227/tradezeroapi/api/"
public let BaseURL = "http://tradezero.co:2701/api/"

// live https://auth.tradezeroweb.co 
public let accessTokenURL = "https://auth.turnkeymobile.solutions/exec/api/userAuth"
public struct API {
     public static let WebsocketUrl        =           "wss://api.turnkeymobile.solutions/apiv1?token="
     public static let accessToken         =           "https://auth.tradezeroweb.co/exec/api/userAuth"
     public static let signUp              =           BaseURL+"SignProcess"
     public static let signUpWithAdderss   =           BaseURL+"SignProcess/residentialAddress?AccountId="
     public static let signUpWithEmpStatus =           BaseURL+"SignProcess/Emplyeestatus?AccountId="
     public static let signUpTermCondition =           BaseURL+"SignProcess/TermsandConditions?AccountId="
     public static let signUpUploadDocs    =           BaseURL+"SignProcess/IdentificationProcessNew?accountId="
     public static let signUpFinancial     =           BaseURL+"SignProcess/saveFinancial?accountId="
     public static let signIn              =           BaseURL+"login"
     public static let forgotPassword      =           BaseURL+"forgot_password"
     public static let logout              =           BaseURL+"logout"

     public static let abouUs              =           "https://www.google.co.in/"
     public static let FAQ                 =           "https://www.google.co.in/"
     public static let termsCondition      =           "https://www.google.co.in/"
}

class Request: NSObject {
    
    // login in request
    static func token(_ login: String, _ password: String) -> [String: Any] {
        return [AppKey.login: login, AppKey.password: password]
    }
    
    // login in request
    static func login(_ email: String, _ password: String) -> [String: Any] {
        return [AppKey.email: email, AppKey.password: password]
    }
    
    // sign Up First stage
    static func signUp(_ firstName: String, _ lastname: String,_ dob:String ,_ email: String, _ password: String , _ phoneNumber : String) -> [String : Any]{
        return [AppKey.account : accountTableData(email,password),
                AppKey.addressSignUp : addressTableData(firstName,lastname,phoneNumber),
                AppKey.pat:patTableData(dob)
        ]
    }
    
   private static func accountTableData(_ email : String , _ password : String) -> [String : Any]{
        return [AppKey.email : email,
                AppKey.password : password ,
                AppKey.customerId : email,
                AppKey.tradingType : "US Stocks" ,
                AppKey.taxId : "",
                AppKey.citizenshipCountryId : "" ,
                AppKey.accountTypeId : "1",
                AppKey.storeURI : "TradeZero" ,
                AppKey.affiliateType : "TradeZero"]
    }
    
   private static func addressTableData(_ firstName: String, _ lastname: String,_ phoneNumber : String)-> [String : Any]{
        return [AppKey.firstName : firstName,
                AppKey.lastName : lastname,
                AppKey.phoneNumber : phoneNumber,
                AppKey.addressTypeId : "1",
                AppKey.city : "",
                AppKey.state : "",
                AppKey.countryId : "",
                AppKey.zipcode : ""]
    }
    
    private static func patTableData(_ dateOfBirth : String)-> [String : Any]{
        return [AppKey.dateOfBirth : dateOfBirth,
                AppKey.identificationTypeId : "2",
                AppKey.identiticationNumber : "",
                AppKey.identificationExpiration : "",
                AppKey.residenceCountryId : "1",
                ]
    }
    
    
    // sign Up Second Stage with Residential
    static func signUpWithResidential(_ signUpDic: KeyValue) -> [String : Any]{
        
        return [AppKey.address : accountTableData(signUpDic)
                
        ]
    }
    
    private static func accountTableData(_ signUpDic : KeyValue)-> [String : Any]{
        return [AppKey.streetAddress1 : signUpDic[AppKey.streetAddress1]!,
                AppKey.apartNumberName : signUpDic[AppKey.apartNumberName]!,
                AppKey.city : signUpDic[AppKey.city]!,
                AppKey.state : signUpDic[AppKey.state]!,
                AppKey.zipcode : signUpDic[AppKey.zipcode]!,
                AppKey.countryId : signUpDic[AppKey.countryCode]!
        ]
    }
    
    // sign Up Third Stage with SSN
    static func signUpWithSocialSecurityNumber(_ signUpDic: KeyValue) -> [String : Any]{
        
        return [AppKey.address : accountTableData(signUpDic)
            
        ]
    }
    
    static func ssnRequestDic(_ signUpDic : KeyValue)-> [String : Any]{
        
        return ["empl" : employementStatus(signUpDic),
                "affi" : affliatedStatus(signUpDic),
                "tfi" : investmentStatus(signUpDic)
        ]
    }
    
    private static func employementStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["employment_status_id" : signUpDic[AppKey.employmentStatusId] ?? 0]
    }
    private static func affliatedStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["is_broker_dealer_security_dealer" : signUpDic[AppKey.isBrokerDealer] ?? false,"is_shareholder_public_company" : signUpDic[AppKey.isShareHolderCompanyName] ?? false ]
    }
    private static func investmentStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["year_mutual_funds" : signUpDic[AppKey.yearMutalFunds] ?? "NONE" ]
    }
    
    
    static func finacialGoalDic(_ signUpDic : KeyValue)-> [String : Any]{
        
        return ["financial" : financialStatus(signUpDic),
                "fund" : fundStatus(signUpDic),
                "gl" : goalStatus(signUpDic),
                "risk" : riskStatus(signUpDic)
        ]
    }
    
    private static func financialStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["annual_income" : signUpDic[AppKey.annualIncome] ?? "$25,000 and under",
                "liquid_net_worth" : signUpDic[AppKey.liquidNetWorth] ?? "Under $50,000"]
    }
    private static func fundStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["funding_option" : signUpDic[AppKey.fundingOption] ?? "Other"]
    }
    private static func goalStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["account_future" : signUpDic[AppKey.accountFuture] ?? "Other"]
    }
    private static func riskStatus(_ signUpDic : KeyValue)-> [String : Any]{
        return ["account_risk_tolerance" : signUpDic[AppKey.riskTolerance] ?? "Low"]
    }
    
    static func termCoditionRequestDic(_ signUpDic : KeyValue)-> [String : Any]{
        
        return ["agrd" : ismarginAggriment(signUpDic)]
    }
    
    private static func ismarginAggriment(_ signUpDic : KeyValue)-> [String : Any]{
        return ["is_margin_loan_agreement" : signUpDic[AppKey.isMarginLoanAgreement] ?? false , "is_borrow_funds_account" : signUpDic[AppKey.isBorrowFundsAccount] ?? false ]
    }
    
    
    
    // sign Up With OTP in request
//    static func signUpOTP( accountId: NSNumber,requestIdOtp : String , codeOtp : NSNumber) -> [String: Any] {
//        return [AppKey.accountId: accountId, AppKey.requestIdOTP:requestIdOtp,AppKey.codeOTP : codeOtp]
//    }
    static func signUpOTP(requestIdOtp : String , codeOtp : Int) -> [String: Any] {
        return [AppKey.typeOTP: "", AppKey.requestIdOTP:requestIdOtp,AppKey.codeOTP : codeOtp]
    }
    
    // sign Up With Email in request
    static func signUpWithEmail(_ email: String) -> [String: Any] {
        return [AppKey.email: email]
    }
    
   
    
    // forgot Password in request
    static func forgotPassword(_ email: String) -> [String: Any] {
        return [AppKey.email: email]
    }
    
    // log out in request
    static func logout(_ userId: String) -> [String: Any] {
        return [AppKey.userId: userId]
    }
    
}
