//
//  Constant.swift
//  TradeZero
//
//  Created by Amit Verma on 10/11/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit


/*********************************  Structures  ********************************/

public struct AppFont {
    public static let helventicaNeue     =     "HelveticaNeue"
    public static let ralewayRegular     =     "Raleway-Regular"

}
public struct AppColor {
    public static let darkGray        =     UIColor.init(cgColor: (100, 100, 100) as! CGColor)
    public static let mediumGray      =     UIColor.init(cgColor: (198, 198, 198) as! CGColor)
    public static let lightGray       =     UIColor.init(cgColor: (240, 240, 240) as! CGColor)
    public static let customBlue      =     UIColor.color(255, 74, 20)
    public static let customRed       =     UIColor.init(cgColor: (255, 74, 20) as! CGColor)
    public static let customYellow    =     UIColor.init(cgColor: (224, 168, 36) as! CGColor)
    public static let customGreen     =     UIColor.init(cgColor: (107, 217, 54) as! CGColor)
    public static let gainColor       =     UIColor.color(49, 202, 150)
    public static let lossColor       =     UIColor.color(254, 56, 54)
    public static let backGroundColor       =     UIColor.color(3, 6, 36)

}

public struct AppTitle {
     public static let appTital       =    "TradeZero"
}

public struct AppKey {
    public static let name            =    "name"
    public static let auth            =    "auth"
    

    //MARK: Storyboard Keys
    public static let main            =    "Main"
    public static let accountSB       =    "Account"
    public static let portfolioSB     =    "Portfolio"
    public static let watchList       =    "WatchList"
    public static let account         =    "account"
    public static let addressSignUp   =    "address"
    public static let pat             =    "pat"
    public static let companyDetail   =    "CompanyDetail"
    public static let history         =    "History"
    public static let banking         =    "Banking"
    public static let setting         =    "Setting"
    
    
    
    //MARK: Variable parameters key
    public static let status          =    "status"
    public static let message         =    "Message"
    public static let data            =    "data"
    public static let token           =    "token"
    public static let email           =    "email_address"
    public static let login           =    "login"
    

    public static let password        =    "password"
    public static let firstName       =    "first_name"
    public static let lastName        =    "last_name"
    public static let dateOfBirth     =    "date_of_birth"
    public static let customerId      =    "customer_id"
    public static let tradingType     =    "trading_type"
    public static let taxId           =    "tax_id"
    public static let citizenshipCountryId     =    "citizenship_country_id"
    public static let accountTypeId            =    "account_type_id"
    public static let storeURI                 =    "storeURI"
    public static let affiliateType            =    "Affiliate_Type"
    
    
    public static let addressTypeId        =    "address_type_id"
    public static let phoneNumber          =    "work_phone"
    public static let city                 =    "city"
    public static let state                =    "state"
    public static let countryId            =    "country_id"
    public static let plaidAccessToken     =    "Access_token"
    public static let plaidCustomerId      =    "CustomerId"
    public static let plaidAccountId       =    "account_id"
    public static let StripeBankToken      =    "Stripe_bank_account_token"
    
    public static let annualIncome         =    "annual_income"
    public static let liquidNetWorth       =    "liquid_net_worth"
    public static let fundingOption        =    "funding_option"
    public static let accountFuture        =    "account_future"
    public static let riskTolerance        =    "account_risk_tolerance"
    
    
    
    public static let accountId            =    "Account_id"
    public static let requestId            =    "request_id"
    public static let requestIdOTP         =    "requestID"
    public static let codeOTP              =    "code"
    public static let typeOTP              =    "type"
    
    public static let identificationTypeId              =    "identification_type_id"
    public static let identiticationNumber              =    "identitication_number"
    public static let identificationExpiration          =    "identification_expiration"
    public static let residenceCountryId                =    "residence_country_id"


    
    public static let SSN                      =    "ssn"
    public static let employmentStatusId       =    "employment_status_id"
    public static let isBrokerDealer           =    "is_broker_dealer_security_dealer"
    public static let isShareHolderCompanyName           =    "is_shareholder_public_company"
    public static let yearMutalFunds           =    "year_mutual_funds"
    public static let isMarginLoanAgreement    =    "is_margin_loan_agreement"
    public static let isBorrowFundsAccount    =    "is_borrow_funds_account"
    
    

    


    public static let address         =    "address"
    public static let addressSecondry =    "addressSecondry"
    public static let addressPlaceId  =    "addressPlaceId"
    public static let addressLat      =    "latitude"
    public static let addressLong     =    "longitude"
    public static let apartNumberName =    "address_2"
    public static let streetAddress1  =    "address_1"
    public static let streetAddress2  =    "address_3"
    public static let zipcode         =    "zipcode"
    public static let countryName     =    "countryName"
    public static let countryCode     =    "countryCode"
    public static let countryCodeID     =    "countryCodeID"
    
    public static let documentTypeId     =    "Documenttypeid"
    public static let countryIdForDoc    =    "countryid"
    public static let stateIdForDoc      =    "stateid"

    //Account Detail Api response
    
    public static let buyingPower                =    "buyingPowerDollars"
    public static let buyingPowerMultiPlier      =    "buyingPowerMultiplier"
    public static let portFolioValue             =    "portFolioValue"
    
    
    
    //MARK: Market Data Keys
    public static let responseType    =    "rt"
    public static let messageType     =    "mt"
    
    
    
    //Chart data Key
    public static let close           =    "c"
    public static let date            =    "d"
    public static let open            =    "o"
    public static let low             =    "l"
    public static let high            =    "h"


    //MARK: Quote Data Key
    public static let quoteBestAsk            =    "a"
    public static let quoteBestAksSize        =    "as"
    public static let quoteBestBid            =    "b"
    public static let quoteBestBidSize        =    "bs"
    public static let quoteHour               =    "h"
    public static let quoteMinut              =    "m"
    public static let quoteSecond             =    "s"
    public static let quoteListedExchange     =    "le"
    
    //MARK: Trade Data Key
    public static let tradeSymbol                =    "sym"
    public static let tradeHour               =    "h"
    public static let tradeMinut              =    "m"
    public static let tradeSecond             =    "s"
    public static let tradeDayHigh            =    "hi"
    public static let lastTradePrice          =    "l"
    public static let tradeListedExchange     =    "le"
    public static let tradeDayLow             =    "lo"
    public static let openingPrice            =    "o"
    public static let previousClose           =    "pc"
    public static let tradeSize               =    "sz"
    public static let tradeVolume             =    "v"


    //MARK: Search company Key
    public static let searchCompanyName          =    "n"
    public static let companySym                 =    "s"
    public static let companyExchangeName        =    "e"
    
    
    
    public static let deviceToken     =    "device_token"
    
    public static let language        =    "locale"
    public static let deviceType      =    "device"
    public static let accessToken     =    "access_token"
    public static let userId          =    "id"
    
    
    
    
    public static let deliveryAddress =    "delivery_address"
    public static let userType        =    "user_type"
    public static let iOS             =    "ios"
    
    
    
    //MARK: GetSymbolMatrics parameters
    public static let marketCapital              =    "marketCap"
    public static let PERation                   =    "peRatio"
    public static let fiftyTwoWeekLow            =    "fiftyTwoWeekLow"
    public static let fiftyTwoWeekHigh           =    "fiftyTwoWeekHigh"
    public static let epsLatest                  =    "epsLatestYearGrowth"
    public static let shares                     =    "sharesOutstanding"
    public static let DIVYield                   =    "annualYield"
    
    
    
    //MARK: Order Api Data Keys

    public static let orderResponseType    =    "requestType"
    public static let orderMessageType     =    "messageType"
    public static let orderData            =    "data"
    
    
    //MARK: Position Keys /watchlist parameters keys
    
    public static let orderSymbol               =    "symbol"
    public static let orderCreated              =    "created"
    public static let orderType                 =    "type"
    public static let orderBid                  =    "bid"
    public static let orderAsk                  =    "Ask"
    public static let orderCurrentPrice         =    "currentPrice"
    public static let orderVolume               =    "volume"
    
   
    public static let companyName               =    "companyname"
    public static let currentPrice              =    "entryPriceCents"
    public static let isUp                      =    "isUp"
    public static let percentValue              =    "percentvalue"
    
    //MARK Open Close position
    public static let closePrice               =    "closePriceCents"
    public static let entryPriceCents          =    "entryPriceCents"
    public static let quantityClosed           =    "quantityClosed"
    public static let quantityOpen             =    "quantityOpen"
    public static let createTimeDate           =    "created"
    public static let strikeprice              =    "strikePrice"
    
    //MARK: open order keys
    
    public static let limitPriceCents              =    "limitPriceCents"
    public static let stopPriceCents               =    "stopPriceCents"
    public static let openOrderType                =    "orderType"
    public static let openOrderQuantiry            =    "quantity"
    public static let openOrderRoute               =    "route"
    public static let openOrderSide                =    "side"
    public static let openOrderStatus              =    "status"
    public static let openTimeInForce              =    "timeInForce"
    public static let openOrderSymbol              =    "symbol"
    
  
}


public struct NotificationTitle {
   public static let languageChanged  =      "LanguageChanged"
}

public struct Stripe {
    public static let publishableKey  =      "pk_test_BxUSyI4Nbjo1FwpN42F8Sgyo"
    public static let secretKey       =      "sk_test_j1XLc0OJhnh8jbS1uYSrZiLW"
}

/*********************************************************************************/


/************************************ Enum ***********************************/

public enum OrderStatus {
    case placed
    case selected
}

public var orderStatus: OrderStatus? = .selected

/*********************************************************************************/


/************************************ Constant ***********************************/

let KAppDelegate: AppDelegate         =      UIApplication.shared.delegate as! AppDelegate

public let AppUserDefaults            =      UserDefaults.standard
public let AppNotificationCenter      =      NotificationCenter.default
public typealias KeyValue             =      [String : Any]


public let KGoogleKey                 =      "AIzaSyBGgGguHfpou33LFaKkuN5QyB8wPQ3KUYg"
public let KDelayTime                 =       2.0
public let KTimeDuration              =       0.3
public let KOffline                   =       "Offline"
public let KSimulatorToken            =      "simulator123456statatic12345device123456token123456"
public let KLocationServicePath       =      "prefs:root=LOCATION_SERVICES"
public let KPhoneMaxLength            =       12
public let KEmailMaxLength            =       100
public let KPasswordMaxLength         =       20
public let KPasswordMinLength         =       8
public let KFirstNameLength           =       20
public let KCommentLength             =       300
public let KCardHolderMaxLength       =       40
public let KCardNumberMaxLength       =       16
public let KCCVMaxLength              =       3
public let KCCVAmexLength             =       4

//Language code
public let KEnglish                   =        "English"
public let KSpanish                   =        "Español"



// Need to change
public var KOk: String { get { return "Ok"} }
public var KAlertTitle: String { get { return "Message" } }
public var KSelectOption: String { get { return "SelectOption" } }
public var KServerError: String { get { return "ServerError" } }
public var KInternetConnection: String { get { return "InternetConnection" } }
public var KEnterName: String { get { return "EnterName" } }
public var KEnterEmail: String { get { return "Please enter your Email." } }
public var KEnterValidEmail: String { get { return "Please enter a valid Email." } }
public var KEnterPassword: String { get { return "Please enter your Password." } }
public var KMinPasswordLength: String { get { return "Your password lenght should be greater than 9 digits." } }
public var KMaxPasswordLength: String { get { return "Your password lenght should be less than 21 digits." } }
public var KEnterSSN: String { get { return "Please enter your SSN" } }
public var KEnterResiAddress: String { get { return "Please select your Residential Address" } }
public var KEnterFirst: String { get { return "Please enter your First Digit" } }
public var KEnterSecond: String { get { return "Please enter your Second Digit" } }
public var KEnterThird: String { get { return "Please enter your Third Digit" } }
public var KEnterFourth: String { get { return "Please enter your Fourth Digit" } }
public var KEnterNewPassword: String { get { return "EnterNewPassword" } }
public var KMinimumPassword: String { get { return "MinimumPassword" } }
public var KEnterConfirmPassword: String { get { return "EnterConfirmPassword" } }
public var KPasswordNotMatch: String { get { return "PasswordNotMatch" } }
public var KNewPasswordNotMatch: String { get { return "NewPasswordNotMatch" } }
public var KEnterPhoneNumber: String { get { return "EnterPhoneNumber" } }
public var KEnterValidPhoneNumber: String { get { return "EnterValidPhoneNumber" } }
public var KCameraNotSupport: String { get { return "CameraNotSupport" } }
public var KCheckGalleryPermission: String { get { return "CheckGalleryPermission" } }
public var KCheckCameraPermission: String { get { return "CheckCameraPermission" } }
public var KAccessCurrentLocation: String { get { return "AccessCurrentLocation" } }
public var KEnableLocationService: String { get { return "EnableLocationService" } }
public var KConfirmBooking: String { get { return "ConfirmBooking" } }
public var KEnterFirstName: String { get { return "Please enter your First name." } }
public var KEnterLastName: String { get { return "Please enter your Last name." } }
public var KEnterFirstNoField: String { get { return "Please enter first three digits" } }
public var KEnterSecondNoField: String { get { return "Please enter second three digits" } }
public var KEnterThirdNoField: String { get { return "Please enter last digits." } }
public var KEnterDOB: String { get { return "Please enter correct DOB" } }
public var KLogoutConfirmation: String { get { return "LogoutConfirmation" } }
public var KSelectAddress: String { get { return "SelectAddress" } }



/*********************************************************************************/

