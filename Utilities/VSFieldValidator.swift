//
//  VSFieldValidator.swift
//  TradeZero
//
//  Created by Amit Verma on 1/16/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

// Constant will be use for field validattion
private let KCharacters       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
private let KNumeric          = "0123456789"
private let KAlphaNumeric     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
private let kFloatNumeric     = "0123456789."

private let KValidatorTypeKey = "ValidatorType"
private let KMaximumLengthKey = "maxLength"

// enum will be use for check field validation type
enum VSFieldValidatorType: Int {
    case alphabet = 1
    case numeric
    case alphaNumeric
    case floatNumeric
    case phoneNumber
    case email
    case password
    case cardNumber
    case none
}

class VSFieldValidator: UIViewController ,UIGestureRecognizerDelegate{
    
    public var fieldsArray = NSMutableArray()
    fileprivate var fieldsValidatorDictionary = NSMutableDictionary()
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
        //self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    // MARK: Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    
    // MARK: Private Methods
    /*
     method  : setValidator
     param1  : validatorType
     param2  : textField
     param3  : maximumLength
     param4  : keyboardType
     */
    public func setValidator(textField: UITextField, validatorType: VSFieldValidatorType, maximumLength: Int, keyboardType: UIKeyboardType? = .default) {
        
        textField.keyboardType = keyboardType!
        textField.isSecureTextEntry = false
        textField.autocorrectionType = .yes
        switch validatorType {
        case .email:
            textField.keyboardType = .emailAddress
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
        case .password:
            textField.isSecureTextEntry = true
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
        case .phoneNumber:
            textField.autocapitalizationType = .none
            textField.keyboardType = .phonePad
            textField.autocorrectionType = .no
        case .alphabet:
            textField.autocapitalizationType = .words
        default :
            textField.autocapitalizationType = .sentences
            break
        }
        textField.spellCheckingType = .yes
        let dict = NSMutableDictionary()
        dict.setValue(validatorType.rawValue, forKey: KValidatorTypeKey)
        dict.setValue(maximumLength, forKey: KMaximumLengthKey)
        fieldsValidatorDictionary.setObject(dict, forKey: NSValue(nonretainedObject: textField))
    }
    
    /*
     method  : setValidator
     param1  : validatorType
     param2  : textField
     param3  : maximumLength
     param4  : keyboardType
     */
    public func setValidator(textView: UITextView, validatorType: VSFieldValidatorType, maximumLength: Int, keyboardType: UIKeyboardType? = .default) {
        
        textView.keyboardType = keyboardType!
        textView.isSecureTextEntry = false
        textView.autocorrectionType = .yes
        switch validatorType {
        case .email:
            textView.keyboardType = .emailAddress
            textView.autocapitalizationType = .none
            textView.autocorrectionType = .no
        case .password:
            textView.isSecureTextEntry = true
            textView.autocapitalizationType = .none
            textView.autocorrectionType = .no
        case .alphabet:
            textView.autocapitalizationType = .words
        default :
            textView.autocapitalizationType = .sentences
            break
        }
        textView.autocorrectionType = .no
        textView.spellCheckingType = .yes
        let dict = NSMutableDictionary()
        dict.setValue(validatorType.rawValue, forKey: KValidatorTypeKey)
        dict.setValue(maximumLength, forKey: KMaximumLengthKey)
        fieldsValidatorDictionary.setObject(dict, forKey: NSValue(nonretainedObject: textView))
    }
    
    // method : setUpVSFieldValidator
    public func setUpVSFieldValidator() {
        fieldsArray.enumerateObjects({ object, index, stop in
            if let textField = object as? UITextField {
                textField.delegate = self
            }
            if let textView = object as? UITextView {
                textView.delegate = self
            }
        })
    }
    
    // MARK: Other Methods
    private func initialization() {
        fieldsArray = NSMutableArray()
        fieldsValidatorDictionary = NSMutableDictionary()
    }
    
    /*
     method  : isValidFieldForFloat
     param1  : newString
     param2  : string
     param3  : maximumLength
     return  : Bool
     */
    fileprivate func _isValidFieldForFloat(newString: String,string: String,maximumLength: Int)-> Bool {
        if string.isEmpty { // back key
            return true
        }
        let array = Array(newString.characters)
        var pointCount = 0 //count the decimal separator
        var unitsCount = 0 //count units
        var decimalCount = 0 // count decimals
        
        for character in array { //counting loop
            if character == "." {
                pointCount += 1
            } else {
                if pointCount == 0 {
                    unitsCount += 1
                } else {
                    decimalCount += 1
                }
            }
        }
        if unitsCount > maximumLength { return false } // units maximum : here 2 digits
        if decimalCount > 2 { return false } // decimal maximum
        switch string {
        case "0","1","2","3","4","5","6","7","8","9": // allowed characters
            return true
        case ".": // block to one decimal separator to get valid decimal number
            if pointCount > 2 {
                return false
            } else {
                return true
            }
        default: // manage delete key
            let array = Array(string.characters)
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    fileprivate func _cardNumberFormatForField(textField: UITextField,string: String,maximumLength: Int,range: NSRange) ->  Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > maximumLength && !hasLeadingOne) || length > maximumLength + 1 {
            let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
            return (newLength > maximumLength) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        if hasLeadingOne {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 4 {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", areaCode)
            index += 4
        }
        if length - index > 4 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }

        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        textField.text = formattedString as String
        return false
    }
    
    /*
     method  : phoneNumberFormatForField
     description : Set Phone number format in field (e.g. (123)345-6789)
     param1  : textField
     param2  : string
     param3  : maximumLength
     return  : Bool
     */
    fileprivate func _phoneNumberFormatForField(textField: UITextField,string: String,maximumLength: Int,range: NSRange) ->  Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
            let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
            return (newLength > maximumLength) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        if hasLeadingOne {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3 {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("(%@) ", areaCode)
            index += 3
        }
        if length - index > 3 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        textField.text = formattedString as String
        return false
    }
    
    /*
     method  : phoneNumberFormatForTextView
     description : Set Phone number format in textView (e.g. (123)345-6789)
     param1  : textView
     param2  : string
     param3  : maximumLength
     return  : Bool
     */
    fileprivate func _phoneNumberFormatForTextView(textView: UITextView,string: String,maximumLength: Int,range: NSRange) ->  Bool {
        
        let newString = (textView.text! as NSString).replacingCharacters(in: range, with: string)
        let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11 {
            let newLength = (textView.text! as NSString).length + (string as NSString).length - range.length as Int
            return (newLength > maximumLength) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3 {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("(%@)", areaCode)
            index += 3
        }
        if length - index > 3 {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        textView.text = formattedString as String
        return false
    }
}

extension VSFieldValidator: UITextFieldDelegate {
    // MARK:  UITextField Delegate

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (((textField.text?.isEmpty) == true) && (string == " ")) {
            return false
        }
        if (fieldsValidatorDictionary.allKeys as NSArray).contains(NSValue(nonretainedObject:textField)) {
            
            let dictionary = fieldsValidatorDictionary.object(forKey: NSValue(nonretainedObject:textField)) as? NSDictionary
            
            let maximumLength = dictionary?.object(forKey: KMaximumLengthKey) as! Int
            let validator = dictionary?.object(forKey: KValidatorTypeKey) as! Int
            
            if let validatorType = VSFieldValidatorType(rawValue: validator) {
                var validInput: String = String()
                switch validatorType {
                case .alphabet:
                    validInput = KCharacters
                case .numeric:
                    validInput = KNumeric
                case .alphaNumeric:
                    validInput = KAlphaNumeric
                case .floatNumeric:
                    return self._isValidFieldForFloat(newString: textField.text! + string, string: string, maximumLength: maximumLength)
                case .phoneNumber:
                    let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: KNumeric).inverted
                    let components = (string as NSString).components(separatedBy: numberOnly)
                    let filtered = components.joined(separator: "")
                    if string == filtered {
                        return self._phoneNumberFormatForField(textField: textField, string: string, maximumLength: maximumLength, range: range)
                    }
                case .cardNumber:
                    let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: KNumeric).inverted
                    let components = (string as NSString).components(separatedBy: numberOnly)
                    let filtered = components.joined(separator: "")
                    if string == filtered {
                        return self._cardNumberFormatForField(textField: textField, string: string, maximumLength: maximumLength, range: range)
                    }

                default:
                    let newLength = textField.text!.characters.count + string.characters.count - range.length
                    if (newLength <= maximumLength) {
                        return newLength <= maximumLength
                    }
                    break
                }
                
                let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: validInput).inverted
                let components = (string as NSString).components(separatedBy: numberOnly)
                let filtered = components.joined(separator: "")
                if string == filtered {
                    //check textfield length
                    let newLength = textField.text!.characters.count + string.characters.count - range.length
                    if (newLength <= maximumLength) {
                        return newLength <= maximumLength
                    }
                }
                return false
            }
        }
        return true
    }
}
extension VSFieldValidator: UITextViewDelegate {

    // MARK:  UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText string: String) -> Bool {
        if (((textView.text?.isEmpty)==true) && (string == " ")) {
            return false
        }
        
        if (fieldsValidatorDictionary.allKeys as NSArray).contains(NSValue(nonretainedObject:textView)) {
            let dictionary = fieldsValidatorDictionary.object(forKey: NSValue(nonretainedObject:textView)) as? NSDictionary
            
            let maximumLength = dictionary?.object(forKey: KMaximumLengthKey) as! Int
            let validator = dictionary?.object(forKey: KValidatorTypeKey) as! Int
            
            if let validatorType = VSFieldValidatorType(rawValue: validator) {
                var validInput: String = String()
                switch validatorType {
                case .alphabet:
                    validInput = KCharacters
                case .numeric:
                    validInput = KNumeric
                case .alphaNumeric:
                    validInput = KAlphaNumeric
                case .floatNumeric:
                    return self._isValidFieldForFloat(newString: textView.text! + string, string: string, maximumLength: maximumLength)
                case .phoneNumber:
                    let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: KNumeric).inverted
                    let components = (string as NSString).components(separatedBy: numberOnly)
                    let filtered = components.joined(separator: "")
                    if string == filtered {
                        return self._phoneNumberFormatForTextView(textView: textView, string: string, maximumLength: maximumLength, range: range)
                    }
                default:
                    let newLength = textView.text!.characters.count + string.characters.count - range.length
                    if (newLength <= maximumLength) {
                        return newLength <= maximumLength
                    }
                    break
                }
                
                let numberOnly: CharacterSet = NSCharacterSet.init(charactersIn: validInput).inverted
                let components = (string as NSString).components(separatedBy: numberOnly)
                let filtered = components.joined(separator: "")
                if string == filtered {
                    //check textfield length
                    let newLength = textView.text!.characters.count + string.characters.count - range.length
                    if (newLength <= maximumLength) {
                        return newLength <= maximumLength
                    }
                }
                return false
            }
        }
        return true
    }
}

