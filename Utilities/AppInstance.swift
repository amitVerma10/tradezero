//
//  Application.swift
//  TradeZero
//
//  Created by Amit Verma on 10/11/17.
//  Copyright (c) 2017 Amit Verma. All rights reserved.
//

import UIKit

private let _applicationInstance = AppInstance()


class AppInstance: NSObject {
    class var applicationInstance : AppInstance {
        
        return _applicationInstance
        
    }

    var homeVCObject : HomeVC?
    var facebookId = ""
    var device_id = ""
    var apnsToken :String?
    var isTouchEnable  :Bool = false
  
    
    var signUpDictionary = [String:Any]()
    var countryArray = [Any]()
    
    var conditionArray = [String]()
    var attachmentArray = [UIImage]()
}
