//
//  FMBaseVC.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class BaseVC: VSFieldValidator {
    
    // MARK:  View Controller Identifier
    class func identifier() -> String {
        return String(describing: self)
    }
    
    // MARK:  View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        
        _initialization()
    }
    
    // MARK:  Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:  Error Handling Methods
    public func errorHandling(_ response: [String: Any]?, _ status: ResponseStatus, _ error: Error?) {
        ActivityIndicator.shared.hide()
        switch status {
        case .failed :
            if Utility.validate(response?[AppKey.auth]) == "1" {
                UIAlertController.showWithAction(self, KAlertTitle, response?[AppKey.message] as? String, nil, nil, [KOk], true, { (title) in
                    UserPreferences.setUserDetails(nil)
                    UserPreferences.setAccessToken(nil)
                    UserPreferences.setCardDetails(nil)
                    KAppDelegate.firstVC()
                })
            }
            else {
                UIAlertController.show(self, KAlertTitle, response?[AppKey.message] as? String)
            }
        case .unknownError :
            if let error = error?.localizedDescription {
                UIAlertController.show(self, KAlertTitle, error)
            }
            else {
                UIAlertController.show(self, KAlertTitle, KServerError)
            }
        case .networkNotAvailable:
            FMStatusBarAlert(duration: KTimeDuration, delay: KDelayTime, position: .statusBar).message(message: KInternetConnection).messageColor(color: .white).bgColor(color: .red).completion {}.show()
        case .tokenExpired:
            break
        default:
            break
        }
    }
    
    // MARK:  Other Methods
    private func _initialization() {
        _setUpBackButton()
        
        NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name: NSNotification.Name(rawValue: NotificationTitle.languageChanged), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(_networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reachability().monitorReachabilityChanges()
    }
    
    
    @objc public func languageChanged() {
        
    }
    
    @objc private func _networkStatusChanged(_ notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if userInfo[AppKey.status] as? String == KOffline {
                FMStatusBarAlert(duration: KTimeDuration, delay: KDelayTime, position: .statusBar).message(message: KInternetConnection).messageColor(color: .white).bgColor(color: .red).completion {}.show()
            }
        }
    }
    
    public func _setUpBackButton() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

