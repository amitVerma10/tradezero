//
//  AppDelegate.swift
//  TradeZero
//
//  Created by Amit Verma on 11/9/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import TOPasscodeViewController
import Fabric
import Crashlytics
import NotificationBannerSwift



//set up global signup dictionary
public var signUpDictionary = KeyValue()
public var symbolWatchListArr = ["AAPL","FB","GOOCV"]
public var userAccountId = String()
public var inWatchList : Bool = false
public var publicWatchListArr = [String]()
public var accountPositionArr = [KeyValue]()
public var accountPositionDic = [String:String]()
public var accountOpenQuantityDic = [String:Double]()
public var accountEntryPriceDic   = [String:Double]()
public var viewControllerName = String()
public var isLoadFrombackground = Bool()//check for app is loading from back ground
public var isUserlogin = Bool()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,TOPasscodeViewControllerDelegate{

    var window: UIWindow?
    var passcode = String()
    let style = TOPasscodeViewStyle(rawValue: 0)
    var type = TOPasscodeType(rawValue: 0)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        // TODO: Move this to where you establish a user session
        self.logUser()



        // setup intial screen
        _setUpInitialScreen()
        
        // IQKeyboard set up
        IQKeyboardManager.sharedManager().enable = true
        //IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 50.0
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldToolbarUsesTextFieldTintColor = false
        IQKeyboardManager.sharedManager().toolbarTintColor = AppColor.customBlue
      
        //Google Place key
        GMSPlacesClient.provideAPIKey(KGoogleKey)
        
        
   
        return true
    }
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("amitk.verma@smartdatainc.net")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if isUserlogin == true{
            WebSocketManager.sharedInstance.disConnect()
            isLoadFrombackground = true
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        ActivityIndicator.shared.hide()
        if UserDefaults.standard.bool(forKey: "pinCodeOn"){
            
            if (UserDefaults.standard.value(forKey: "passcode") != nil){
                self.passcode = UserDefaults.standard.value(forKey: "passcode") as! String
                let passcodeViewController = TOPasscodeViewController(style: style!, passcodeType: type!)
                passcodeViewController.delegate = self
                
                let topVc = UIApplication.topViewController()!
                print(topVc , passcodeViewController)
                
                if(topVc.isKind(of: TOPasscodeViewController.self)){
                }
                else{
                    UIApplication.topViewController()?.present(passcodeViewController, animated: true) {() -> Void in }
                }
                
                //check wether app is connected to socket or not
                if WebSocketManager.sharedInstance.isConnected(){
                    print("connected....:)")
                    
                    
                }else{
                    print("disconnected....:(")
                }
                
            }
            
        }
       
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        isLoadFrombackground = true
    }
    
    
    func didTapCancel(in passcodeViewController: TOPasscodeViewController) {
        //self.dismiss(animated: true, completion: nil)
    }
    
    func passcodeViewController(_ passcodeViewController: TOPasscodeViewController, isCorrectCode code: String) -> Bool {
        return code == passcode
    }
    
    private func _setUpInitialScreen() {
        if let appUser = UserPreferences.getUserDetails(), appUser.id?.length() > 0 {
            homeVC()
        }
        else {
            firstVC()
        }
    }
    func homeVC() {
//        let storyboard = UIStoryboard(name: AppKey.main, bundle:nil)
//        if let homeVC = storyboard .instantiateViewController(withIdentifier: HomeVC.identifier()) as? HomeVC {
//            let navigationController = UINavigationController(rootViewController:homeVC)
//            self.window?.rootViewController = navigationController
//        }
    }
    
    
  
    func firstVC() {
        let storyboard = UIStoryboard(name: AppKey.main, bundle:nil)
        if let loginVC = storyboard .instantiateViewController(withIdentifier: FirstVC.identifier()) as? FirstVC {
            let navigationController = UINavigationController(rootViewController:loginVC)
            self.window?.rootViewController = navigationController
        }
    }
    
    // logout fucction
    func lououtAction() {
        let storyboard = UIStoryboard(name: AppKey.main, bundle:nil)
        if let loginVC = storyboard .instantiateViewController(withIdentifier: LoginVC.identifier()) as? LoginVC {
            let navigationController = UINavigationController(rootViewController:loginVC)
            self.window?.rootViewController = navigationController
           
           
        }
    }
    
    //MARK: Apply SideMenuBar and Root view
    func goToRootViewAfterLogin(){
        
        let mainStoryboard = UIStoryboard(name: AppKey.main, bundle:nil)
        let leftSideDrawerViewController = mainStoryboard .instantiateViewController(withIdentifier: LeftBarVC.identifier()) as? LeftBarVC
        let leftNavCon = UINavigationController(rootViewController:leftSideDrawerViewController!)
        
       /*let accountStoryboard = UIStoryboard(name: AppKey.accountSB, bundle:nil)
        let centerViewController = accountStoryboard .instantiateViewController(withIdentifier: HomeVC.identifier()) as? HomeVC*/
        let accountStoryboard = UIStoryboard(name: AppKey.watchList, bundle:nil)
        let centerViewController = accountStoryboard .instantiateViewController(withIdentifier: WatchListVC.identifier()) as? WatchListVC
        let centerNavCon = UINavigationController(rootViewController:centerViewController!)
        
        let slideMenuController = SlideMenuController(mainViewController: centerNavCon, leftMenuViewController:leftNavCon )
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    //MARK: Apply SideMenuBar and Root view
    func goToRootViewOrderPlaced(){
        
        let mainStoryboard = UIStoryboard(name: AppKey.main, bundle:nil)
        let leftSideDrawerViewController = mainStoryboard .instantiateViewController(withIdentifier: LeftBarVC.identifier()) as? LeftBarVC
        let leftNavCon = UINavigationController(rootViewController:leftSideDrawerViewController!)
        
        /*let accountStoryboard = UIStoryboard(name: AppKey.accountSB, bundle:nil)
         let centerViewController = accountStoryboard .instantiateViewController(withIdentifier: HomeVC.identifier()) as? HomeVC*/
        let accountStoryboard = UIStoryboard(name: AppKey.portfolioSB, bundle:nil)
        let centerViewController = accountStoryboard .instantiateViewController(withIdentifier: PortfolioVC.identifier()) as? PortfolioVC
        let centerNavCon = UINavigationController(rootViewController:centerViewController!)
        
        let slideMenuController = SlideMenuController(mainViewController: centerNavCon, leftMenuViewController:leftNavCon )
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    func goToPortfolio(){
        
    }


}

