//
//  LocationManager.swift
//  TradeZero
//
//  Created by Amit Verma on 10/15/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import CoreLocation

public let KUpdateCurrentLocation      =        "UpdateCurrentLocation"

open class LocationManager: NSObject {
    
    static let shared = LocationManager()
    
    open var allowsBackgroundEvents: Bool = false {
        didSet {
            if #available(iOS 9.0, *) {
                if let backgroundModes = Bundle.main.object(forInfoDictionaryKey: "UIBackgroundModes") as? NSArray {
                    if backgroundModes.contains("location") {
                        self.locationManager.allowsBackgroundLocationUpdates = allowsBackgroundEvents
                    } else {
                        print("You must provide location in UIBackgroundModes of Info.plist in order to use .allowsBackgroundEvents")
                    }
                }
            }
        }
    }
    
    open var pausesLocationUpdatesWhenPossible: Bool = true {
        didSet {
            self.locationManager.pausesLocationUpdatesAutomatically = pausesLocationUpdatesWhenPossible
        }
    }
    
    fileprivate var locationManager: CLLocationManager!
    fileprivate var currentLocation: CLLocation?
    fileprivate var latitude:Double = 0.0
    fileprivate var longitude:Double = 0.0
    
    public func getCurrentLocation() -> CLLocation {
        if Device.UserInterfaceIdiom.simulator {
            return CLLocation.init(latitude: 30.7046, longitude: 76.7179)
        }
        if self.currentLocation?.coordinate.latitude != nil {
            return self.currentLocation!
        }
        return CLLocation.init(latitude: 0.0, longitude: 0.0)
    }
    
    open func startUpdatingLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    open func startUpdate() {
        self.locationManager.startUpdatingLocation()
    }
    
    open func stopUpdate() {
        self.locationManager.stopUpdatingLocation()
    }

    private func _resetLocation(){
        self.currentLocation = CLLocation.init(latitude: 0.0, longitude: 0.0)
    }
    
    class func milesToMeters(_ mile: Float ) -> Float {
        return 1609.344 * mile;
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    // MARK: Location Manager Delegate
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        self.latitude  = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        var interval:TimeInterval
        var isUpdateLocation: Bool = true
        if self.currentLocation?.coordinate.latitude != nil {
            interval = location.timestamp.timeIntervalSince((currentLocation?.timestamp)!)
            let distance = location.distance(from: currentLocation!)
            if interval > 30 { //sec // meters
                isUpdateLocation = false;
            }
        }
        if isUpdateLocation {
            self.currentLocation =  location
            NotificationCenter.default.post(name: Notification.Name(rawValue: KUpdateCurrentLocation), object: currentLocation)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .restricted, .denied:
                break
            case .notDetermined:
                manager.requestWhenInUseAuthorization()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                manager.startUpdatingLocation()
                break
            }
        }
    }
    
}


