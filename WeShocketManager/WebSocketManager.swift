//
//  WebShocketManager.swift
//  TradeZero
//
//  Created by Amit Verma on 11/30/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit
import Starscream
import NotificationBannerSwift

public enum MessageType: String {
    case News                = "News"
    case companyDataT        = "t"
    case companyDataQ        = "q"
    case OHLCVSubscribe      = "OHLCVChart"
    case searchCompany       = "FindEquitiesByCompany"
    case searchCompanyBySym  = "FindEquities"
    case statsValue          = "GetSymbolMetrics"
    case watchList           = "WatchList"
    case About
    case Error               = "Error"
    case getEquities         = "GetEquities"
    case earnings            = "Earnings"
}

public enum OrderType: String{
    case placeOrder         = "OrderEvent"
    case cancelOrder        = "CancelOrder"
    case accountDetails     = "AccountDetails"
    case orderDetails       = "OrderDetails"
    case openOrder          = "OpenOrders"
    case orderEventHistory  = "OrderEventHistory"
    case tradeHistory       = "TradeHistory"
    case invalidRequest     = "InvalidRequest"
    case borrowStatus       = "BorrowStatus"
    case locateHistory      = "LocateHistory"
    case locate             = "Locate"
    case locateAccept       = "locateAccept"
    case Error               = "Error"
    
}

public enum vcForNotification: String {
    case openPosition         = "OpenPositionVC"
    case closePosition         = "ClosePositionVC"
}


class SocketConstants {
    
    
    // #SERVER METHODS
    static let connect                  = "connect"
    static let reconnect                = "reconnect"
    static let disconnect               = "disconnect"
    static let connect_error            = "connect_error"
    static let error                    = "error"
    
    
    // NSNofification  Order API
    static let placeOrderNotification               = "PlaceOrder"
    static let cancelOrderNotification              = "CancelOrder"
    static let accountDetailsNotification           = "AccountDetails"
    static let orderDetailsNotification             = "OrderDetails"
    static let openOrdersNotification               = "OpenOrders"
    static let orderEventHistoryNotification        = "OrderEventHistory"
    static let tradeDetailsNotification             = "TraderDetails"
    static let tradeHistoryNotification             = "TradeHistory"
    
    
    static let buySharesNotification                 = "Buy"
    static let sellSharesNotification                = "Sell"
    static let coverSharesNotification               = "Cover"
    static let shortSharesNotification               = "Short"
    static let locateSharesNotification              = "Locate"
    
    
    // NSNotification Market Data API
    static let messageType                           = "t"
    
    
    // NSNotification connection
    static let reloadSocketDatafromForeground                = "foreGroundConnect"
    static let reloadSocketDatafromForegroundInStats         = "foreGroundConnectStats"
    static let reloadSocketDatafromForegroundCompany       = "foreGroundConnectCompanyVC"
    static let socketConnectedNotification          = "socketConnected"
    static let socketDisConnectedNotification       = "socketDisconncted"
    static let socketAuthenticationNotification     = "socketAuthenticationFailed"
    
    static let newsDataRecived                      = "NewsDataRecived"
    static let borrowStatus                         = "borrowStatus"
    static let getEquities                          = "GetEquities"
    static let earnings                             = "Earnings"
    static let companyDataRecivedQuote              = "companyDataRecivedQuote"
    static let companyDataRecivedTrade              = "companyDataRecivedTrade"
    static let companyDataRecivedQuoteInHomeVC      = "companyDataRecivedQuoteInHomeVC"
    static let companyDataRecivedTradeInHomeVC      = "companyDataRecivedTradeInHomeVC"
    static let companyDataRecivedQuoteOpenPositionVC      = "companyDataRecivedQuoteOpenPositionVC"
    static let companyDataRecivedTradeOpenPositionVC      = "companyDataRecivedTradeOpenPositionVC"
    static let companyDataRecivedQuoteClosePositionVC       = "companyDataRecivedQuoteClosePositionVC"
    static let companyDataRecivedTradeClosePositionVC       = "companyDataRecivedTradeClosePositionVC"
    static let companyDataRecivedQuoteForState      = "companyDataRecivedQuoteForState"
    static let companyDataRecivedTradeForState      = "companyDataRecivedTradeForState"
    static let chartDataRecived                     = "ChartDataRecived"
    static let CandelchartDataRecived               = "CandelchartDataRecived"
    static let searchDataRecived                    = "searchDataRecived"
    static let statsDataRecived                     = "statsDataRecived"
    
    static let orderApiResponse                     = "orderApiResponse"
    static let orderDetailsResponse                 = "orderDetailsResponse"
    static let accountDetailsResponse               = "accountDetailsResponse"
    static let openOrderResponse                    = "openOrderResponse"
    static let cancelOrderResponse                  = "cancelOrderResponse"
    static let invalidrequest                       = "invalidrequest"
    static let orderEventHistoryResponse            = "orderEventHistoryResponse"
    
    //WatchList
    static let createWatchList                      = "createWatchList"
    static let addToWatchList                       = "addToWatchList"
    static let removeFromWatchList                  = "removeFromWatchList"
    static let retrieveWatchList                    = "retrieveWatchList"
    
    //Locate
    static let locateHistory                        = "LocateHistory"
    static let locate                               = "Locate"
    static let locateAccept                         = "LocateAccept"
    
    
}

class WebSocketManager: NSObject {
    
    
    static let sharedInstance = WebSocketManager()
    var socket: WebSocket?
    
    override init() {
        
        super.init()
        
    }
    
    func isConnected () -> Bool{
        guard (socket?.isConnected) != nil else { return false}
            return true
    }
    
    func connect(with url:URL) {
        
        guard (socket?.isConnected) != nil else {
            
            socket = WebSocket(url: url)
        
            socket?.advancedDelegate = self
            socket?.disableSSLCertValidation = true
            self.socket!.connect()
            
            return
            
        }
        socket = WebSocket(url: url)
        socket?.advancedDelegate = self
        socket?.disableSSLCertValidation = true
        self.socket!.connect()
        
        
    }
    
    func disConnect(){
        self.socket!.disconnect()
        self.socket!.delegate = nil
    }
    
    func sendDataToWebSocket(_ dic:[String: AnyObject]){
        
        
        if (socket?.isConnected)!{
            print("conected")
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted);
                if let string = String(data: jsonData, encoding: String.Encoding.utf8){
                    print(string)
                    socket?.write(string: string)
                } else {
                    print("Couldn't create json string");
                }
            } catch let error {
                print("Couldn't create json data: \(error)");
            }

        }
        
    }
    
    //MARK: account Detail request
    func accountDetailRequrst(accountId : String){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//News
            dic["requestType"] = "AccountDetails" as AnyObject//
            dic["data"] = accountId as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    
    //MARK: Borrow Status request {"requestId":"1234","symbol":"USO","requestType":"BorrowStatus"}
    func borrowStatus(requestStatus : String, symbol : String){
        //Call Company data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Borrow Status
            dic["requestId"] = requestStatus as AnyObject//
            dic["symbol"] = symbol as AnyObject
            dic["requestType"] = "BorrowStatus" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    
    
    //MARK: subscribe Company requesr
    func subscribeCompany(symbol: String){
        //Call Company data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//News
            dic["rt"] = "L1Subscribe" as AnyObject//
            dic["sym"] = symbol as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    //MARK: unSubscribe Company requesr
    func unSubscribeCompany(symbol: String){
        //Call Company data from websocket
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//News
            dic["rt"] = "L1Unsubscribe" as AnyObject
            dic["sym"] = symbol as AnyObject
            sendDataToWebSocket(dic)// send data to websocket
        }
    }
    
    //MARK: open order request
    func orderDetailRequrst(orderDic : KeyValue){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Orders Details
            dic["requestType"] = "OrderDetails" as AnyObject//
            dic["data"] = orderDic as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    //MARK: cancel order request
    func cancelOrderRequrst(orderId : String){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Orders Details
            dic["requestType"] = "CancelOrder" as AnyObject//
            dic["data"] = orderId as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
    }
    
    //MARK: Order Event history request
    func orderEventHistoryRequest(dateDic : [String: AnyObject]){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Orders Details
            dic["requestType"] = "OrderEventHistory" as AnyObject//
            dic["data"] = dateDic as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    //MARK: Trade History request
    func tradeHistoryRequest(accountId:String , startDate:String , endDate: String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//Orders Details
            dic["requestType"] = "TradeHistory" as AnyObject//
            dic["accountId"] = accountId as AnyObject
            dic["startDate"] = startDate as AnyObject
            dic["endDate"] = endDate as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    //MARK: Add symbol to watchlist
    func addToWatchList(symbolName : String, name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["sym"] = symbolName as AnyObject
            dic["cmd"] = "Add" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    //MARK: remove symbol to watchlist
    func removeFromWatchList(symbolName : String, name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["sym"] = symbolName as AnyObject
            dic["cmd"] = "Remove" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
   /* Clear - Clears a watch list
    List - Lists all watch lists
    Retrieve - Retrieves all symbols in a watch list
    Create - Creates a watch list
    Delete - Deletes a watch list*/

    //MARK: clear watchLict
    func clearWatchList(name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["cmd"] = "Clear" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    
    //MARK: Retrieve watchlist
    func retrieveWatchList(name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["cmd"] = "Retrieve" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    //MARK: create watchlist
    func createWatchList(name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["cmd"] = "Create" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    //MARK: delete watchlist
    func deleteWatchList( name : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["name"] = name as AnyObject//
            dic["rt"] = "WatchList" as AnyObject
            dic["cmd"] = "Delete" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
    
    
    func getEquities(symArr : [String]){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String:AnyObject]()//
            dic["rt"] = "GetEquities" as AnyObject
            dic["symbols"] = symArr as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)// send data to websocket
        }
        
    }
 
    //MARK: locate service work
    func locateHistory(accountId : String){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String : AnyObject]()
            dic["requestId"] = "".randomString(length: 10) as AnyObject
            dic["accountId"] = accountId as AnyObject
            dic["requestType"] = "LocateHistory" as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
        }
        
    }
    
    func locateRequest(symbol : String,accountId : String,quantity : Int){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String : AnyObject]()
            dic["requestId"] = "".randomString(length: 10) as AnyObject
            dic["symbol"] = symbol as AnyObject
            dic["quantity"] = quantity as AnyObject
            dic["requestType"] = "Locate" as AnyObject
            dic["accountId"] = accountId as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
        }
        
    }
    
    func locateAccept(requestId : String,symbol : String,accountId : String,quantity : Int){
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String : AnyObject]()
            dic["requestId"] = requestId as AnyObject
            dic["symbol"] = symbol as AnyObject
            dic["requestType"] = "LocateAccept" as AnyObject
            dic["accountId"] = accountId as AnyObject
            dic["quantity"] = quantity as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
        }
        
    }
    //PortFolio work- Open Order
    func opneOrder(accountId : String){
        
        if WebSocketManager.sharedInstance.isConnected(){
            var dic = [String : AnyObject]()
            dic["requestType"] = "OpenOrders" as AnyObject
            dic["data"] = accountId as AnyObject
            WebSocketManager.sharedInstance.sendDataToWebSocket(dic)
        }
    }
    
    
    //{"requestId":"1234","accountId":"tnky1012","symbol":"SPY","quantity":100,"requestType":"Locate”}

}

extension WebSocketManager :WebSocketAdvancedDelegate{
    func websocketDidConnect(socket: WebSocket) {
        print("web socket is connected")
        
        //load only when app come from background to forground mode
        if isLoadFrombackground == true && isUserlogin == true{
            
            isLoadFrombackground = false
            if UserDefaults.standard.bool(forKey: "isHomeView") == true{
                
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForeground), object: nil, userInfo: nil)
                
            }
            else if UserDefaults.standard.bool(forKey: "isStats") == true{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForegroundInStats), object: nil, userInfo: nil)
                
            }else if UserDefaults.standard.bool(forKey: "isHomeView") == false{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.reloadSocketDatafromForegroundCompany), object: nil, userInfo: nil)
            }
            
        }
        if isUserlogin == false{
            isUserlogin = true
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.socketConnectedNotification), object: nil, userInfo: nil)
            ActivityIndicator.shared.hide()
        }
        
       
    }
    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
        print("error=",error?.localizedDescription)
        if let errorStr = error?.localizedDescription as? String{
            if errorStr == "NotAuthenticated"{
                let banner = StatusBarNotificationBanner(title: "NotAuthenticated", style: .danger)
                banner.show()

                KAppDelegate.lououtAction()
            }else if errorStr == "Invalid HTTP upgrade"{
                let banner = StatusBarNotificationBanner(title: errorStr, style: .danger)
                banner.show()
               
                ActivityIndicator.shared.hide()
                
            }
        }else{
           socket.connect()
        }
        
        print("websocketDidDisconnect")
    }
    func websocketDidReceiveMessage(socket: WebSocket, text: String, response: WebSocket.WSResponse) {
        print("websocketDidReceiveMessage",text)
        
        if let data = text.data(using: String.Encoding.utf8){
            do {
                let JSON : [String:AnyObject] = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                print("\(JSON)")
                
                if (JSON[AppKey.messageType] != nil){
                    
                     let message = MessageType(rawValue: (JSON[AppKey.messageType] as? String)!)
                        
                        switch message{
                        case .News?:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.newsDataRecived), object: nil, userInfo: JSON)
                            break
                        case .companyDataT?:
                            
                            if UserDefaults.standard.bool(forKey: "isHomeView") == true{
                                
                                if viewControllerName == "OpenPositionVC"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeOpenPositionVC), object: nil, userInfo: JSON)
                                    
                                }else if viewControllerName == "ClosePositionVC"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeClosePositionVC), object: nil, userInfo: JSON)
                                }else{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeInHomeVC), object: nil, userInfo: JSON)
                                }
                                
                                
                                
                            }else{
                                
                                if UserDefaults.standard.bool(forKey: "isStats") == true{
                                    
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTradeForState), object: nil, userInfo: JSON)
                                    
                                }else{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedTrade), object: nil, userInfo: JSON)
                                }
                                
                            }
                            

                            break
                        case .companyDataQ?:
                            if UserDefaults.standard.bool(forKey: "isHomeView") == true{
                                
                                if viewControllerName == "OpenPositionVC"{
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteOpenPositionVC), object: nil, userInfo: JSON)
                                    
                                }else if viewControllerName == "ClosePositionVC"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteClosePositionVC), object: nil, userInfo: JSON)
                                }else{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteInHomeVC), object: nil, userInfo: JSON)
                                }
                                
                             
                                
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteInHomeVC), object: nil, userInfo: JSON)
                                
                                
                            }else{
                                
                                if UserDefaults.standard.bool(forKey: "isStats") == true{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuoteForState), object: nil, userInfo: JSON)
                                }else{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.companyDataRecivedQuote), object: nil, userInfo: JSON)
                                }
                                
                            }
                            
                            
                            
                            break
                        case .OHLCVSubscribe?:
                            if UserDefaults.standard.bool(forKey: "isStats") == true{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.CandelchartDataRecived), object: nil, userInfo: JSON)
                            }else{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.chartDataRecived), object: nil, userInfo: JSON)
                            }
                            break
                        case .searchCompany?:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.searchDataRecived), object: nil, userInfo: JSON)
                            break
                            
                        case .searchCompanyBySym?:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.searchDataRecived), object: nil, userInfo: JSON)
                            break
                        case .statsValue?:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.statsDataRecived), object: nil, userInfo: JSON)
                            break
                        case .watchList?:
                            
                            if let cmdValue = JSON["cmd"] as? String{
                                if cmdValue == "Create"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.createWatchList), object: nil, userInfo: JSON)
                                    
                                }else if cmdValue == "Retrieve"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.retrieveWatchList), object: nil, userInfo: JSON)
                                }
                                else if cmdValue == "Add"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.addToWatchList), object: nil, userInfo: JSON)
                                }
                                else if cmdValue == "Remove"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.removeFromWatchList), object: nil, userInfo: JSON)
                                }
                                else if cmdValue == "Delete"{
                                    
                                }
                                
                            }
                            
                            
                            
                            break
                        case .Error?:
                          
                            let banner = StatusBarNotificationBanner(title: (message?.rawValue)!, style: .danger)
                            banner.show()
                            break
                        case .getEquities?:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.getEquities), object: nil, userInfo: JSON)
                            break
                        case .earnings?:
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.earnings), object: nil, userInfo: JSON)
                            
                            break
                        default: break
                        }
                        
               
                        
                    let alert = UIAlertController(title: AppTitle.appTital, message: "\(String(describing: message))", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    
                    
                    
                    }
                
                if (JSON[AppKey.orderMessageType] != nil){
                    
                    if let orderType = OrderType(rawValue: (JSON[AppKey.orderMessageType] as? String)!){
                        
                        switch orderType{
                            
                        case .placeOrder:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.orderApiResponse), object: nil, userInfo: JSON)
                            
                            break
                        case .cancelOrder:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.cancelOrderResponse), object: nil, userInfo: JSON)
                            break
                        case .accountDetails:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.accountDetailsResponse), object: nil, userInfo: JSON)
                            break
                        case .orderDetails:
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.orderDetailsResponse), object: nil, userInfo: JSON)
                            break
                        case.openOrder:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.openOrderResponse), object: nil, userInfo: JSON)
                            break
                        case .orderEventHistory:
                            
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.orderEventHistoryResponse), object: nil, userInfo: JSON)
                            
                            break
                            
                        case .tradeHistory:
                            
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.tradeHistoryNotification), object: nil, userInfo: JSON)
                            break
                        case .invalidRequest:
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.invalidrequest), object: nil, userInfo: JSON)
                            
                            let banner = StatusBarNotificationBanner(title: JSON["data"] as! String, style: .danger)
                            banner.show()
                            
                            break
                        case .borrowStatus:
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.borrowStatus), object: nil, userInfo: JSON)
                            break
                            
                        case .locateHistory:
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.locateHistory), object: nil, userInfo: JSON)
                            break
                            
                            
                        case .locate:
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.locate), object: nil, userInfo: JSON)
                            break
                            
                        case .locateAccept:
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SocketConstants.locateAccept), object: nil, userInfo: JSON)
                            break
                            
                        case .Error:
                            
                            if let messageStr = JSON["data"]!["message"] as? String{
                                FMStatusBarAlert(duration: KTimeDuration, delay: KDelayTime, position: .statusBar).message(message: messageStr).messageColor(color: .white).bgColor(color: .red).completion {}.show()
                                let banner = StatusBarNotificationBanner(title: messageStr, style: .danger)
                                banner.show()
                            }
                            

                            break
                            
                        }
                    }
                }
                
                
                
                
                
                // self.present(alert, animated: true, completion: nil)
            } catch let error{
                print("Error parsing json: \(error)")
            }
        }
        
        print("First frame for this message arrived on \(response.firstFrame)")
    }
    func websocketDidReceiveData(socket: WebSocket, data: Data, response: WebSocket.WSResponse) {
        if let returnData = String(data: data, encoding: .utf8) {
            print(returnData)
        }
        print("websocketDidReceiveData")
    }
    
    func websocketHttpUpgrade(socket: WebSocket, request: String) {
        print("the http request was sent we can check the raw http if we need to")
    }
    func websocketHttpUpgrade(socket: WebSocket, response: String) {
        print("the http response has returned.")
    }
    /*  if socket.isConnected{
     print("conected")
     
     var dic = [String:AnyObject]()//News
     dic["rt"] = "News" as AnyObject
     dic["sym"] = "AAPL" as AnyObject
     /*dic["rt"] = "OHLCVChart" as AnyObject
     dic["rid"] = "1234abc" as AnyObject
     dic["ct"] = "IntraDay" as AnyObject
     dic["res"] = 15 as AnyObject
     dic["sm"] = 570 as AnyObject
     dic["em"] = 960 as AnyObject
     dic["sym"] = "AAPL" as AnyObject
     dic["fb"] = true as AnyObject
     dic["nc"] = 1 as AnyObject*/
     
     /* dic["requestType"] = "PlaceOrder" as AnyObject
     var dicData = [String:AnyObject]()
     dicData["clientOrderId"] = "order123" as AnyObject
     dicData["accountId"] = "account1234" as AnyObject
     dicData["symbol"] = "AAPL" as AnyObject
     dicData["side"] = "Buy" as AnyObject
     dicData["orderType"] = "StopLimit" as AnyObject
     dicData["timeInForce"] = "Day" as AnyObject
     dicData["stopPriceCents"] = "9950" as AnyObject
     dicData["limitPriceCents"] = "10000" as AnyObject
     dicData["route"] = "ARCA" as AnyObject
     dicData["quantity"] = "100" as AnyObject
     dic["data"] = dicData as AnyObject
     */
     
     do {
     let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions.prettyPrinted);
     if let string = String(data: jsonData, encoding: String.Encoding.utf8){
     print(string)
     socket.write(string: string)
     } else {
     print("Couldn't create json string");
     }
     } catch let error {
     print("Couldn't create json data: \(error)");
     }
     
     
     
     }*/
    
    
    
    
    
}
