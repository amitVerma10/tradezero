//
//  OrderHistoryModel.swift
//  TradeZero
//
//  Created by Amit Verma on 2/5/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OrderHistoryModel{
    
    public var symbolName : String?
    public var orderStatus : String?
    public var date : String?
    public var orderId : Int?
    public var orderType : String?
    public var quantity : Int?
    
   

    required public init?(_ dictionary: KeyValue) {
        
        if let dic = dictionary["details"] as? KeyValue {
            self.symbolName = dic["symbol"] as? String ?? "N/A"
            self.orderId = dic["internalOrderId"] as? Int ?? 0
            self.orderType = dic["orderType"] as? String ?? "N/A"
            self.quantity = dic["totalQuantity"] as? Int ?? 0
        }
        
        if let timeStamp =  dictionary["timestamp"] as? Int{
            print(timeStamp)
            let timeString = String(timeStamp/1000)
            self.date = timeString.convertTimeStampToDate64Bit()
        }
        
        self.orderStatus = dictionary["eventType"] as? String ?? ""
        
        
        
    }
    
}
