//
//  AppUser.swift
//  FiggoApp
//
//  Created by Amit Verma on 9/11/17.
//  Copyright © 2017 Amit Verma. All rights reserved.
//

import UIKit

public class AppUser {
    public var id : String?
    public var firstName : String?
    public var lastName : String?
    public var email : String?
    public var password : String?
    public var phoneNumber : String?
    public var dateOfBirth : String?
    public var address : String?
    public var addressSecondry : String?
    public var addressPlaceId : String?
    public var addressLat : Double?
    public var addressLong : Double?
    
    
    required public init?() {
        
    }
    
    required public init?(_ dictionary: KeyValue) {
        self.id = Utility.validate(dictionary[AppKey.userId])
        self.firstName = Utility.validate(dictionary[AppKey.firstName])
        self.lastName = Utility.validate(dictionary[AppKey.lastName])
        self.email = Utility.validate(dictionary[AppKey.email])
        self.password = Utility.validate(dictionary[AppKey.password])
        self.phoneNumber = Utility.validate(dictionary[AppKey.phoneNumber])
        self.dateOfBirth = Utility.validate(dictionary[AppKey.dateOfBirth])
        self.address = Utility.validate(dictionary[AppKey.address])
        self.addressSecondry = Utility.validate(dictionary[AppKey.addressSecondry])
        self.addressPlaceId = Utility.validate(dictionary[AppKey.addressPlaceId])
        //self.addressLat = Utility.validate(dictionary[AppKey.addressLat])
        //self.addressLong = Utility.validate(dictionary[AppKey.addressLong])

        
        if let accessToken = dictionary[AppKey.accessToken] as? String, accessToken.length() > 0 {
            UserPreferences.setAccessToken(accessToken)
        }
        UserPreferences.setUserDetails(_createDictionary(self))
    }
    
    public func _createDictionary(_ user: AppUser) -> KeyValue {
        var parameters = KeyValue()
        parameters[AppKey.userId] = self.id ?? ""
        parameters[AppKey.firstName] = self.firstName ?? ""
        parameters[AppKey.lastName] = self.lastName ?? ""
        parameters[AppKey.phoneNumber] = self.phoneNumber ?? ""
        parameters[AppKey.email] = self.email ?? ""
        parameters[AppKey.address] = self.address ?? ""
        parameters[AppKey.addressSecondry] = self.addressSecondry ?? ""
        parameters[AppKey.addressPlaceId] = self.addressPlaceId ?? ""
        parameters[AppKey.addressLat] = self.addressLat ?? 0.00000
        parameters[AppKey.addressLong] = self.addressLong ?? 0.0000
        
        return parameters
    }
}

