//
//  PositionListModel.swift
//  TradeZero
//
//  Created by Amit Verma on 2/6/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class PositionListModel {
    
    var date = Date()

    public var companySym : String?
    public var currentPrice : Double?
    public var isUp : Bool?
    public var percentValue : String?
    public var time : Int?
    public var hour : Int?
    public var minut : Int?
    public var bid : Double?
    public var ask : Double?
    public var volume : Int?
    public var stocktType : String?
    
    
    required public init?(_ dictionary: KeyValue) {
        companySym = dictionary[AppKey.tradeSymbol] as? String ?? "N/A"
        
        currentPrice = date.isBetweenTwoTime() ? dictionary[AppKey.lastTradePrice] as? Double ?? 0.00 : dictionary[AppKey.previousClose] as? Double ?? 0.00
        isUp = calculateGainOrLoss(dictionary)
        percentValue = calculateGainOrLossValue(dictionary)
        time = dictionary[AppKey.orderCreated] as? Int ?? 0
        hour = dictionary[AppKey.quoteHour] as? Int ?? 0
        minut = dictionary[AppKey.quoteMinut] as? Int ?? 0
        bid = dictionary[AppKey.quoteBestBid] as? Double ?? 0.00
        ask = dictionary[AppKey.quoteBestAsk] as? Double ?? 0.00
        volume = dictionary[AppKey.tradeVolume] as? Int ?? 0
        stocktType = dictionary[AppKey.orderType] as? String ?? "N/A"
        
   
    }
    
    
    //calculate gain or loss of stock
    private func calculateGainOrLoss(_ tradeDic:KeyValue)->Bool
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return false
        }
        
        if Double(truncating: lastTrade as! NSNumber) >= Double(truncating: prevoiusClose as! NSNumber) {
            return true
        }else{
            return false
        }
    }
    
    //calculate gain or loss of stock
    private func calculateGainOrLossValue(_ tradeDic:KeyValue)->String
    {
        guard let lastTrade =  tradeDic[AppKey.lastTradePrice]  , let prevoiusClose = tradeDic[AppKey.previousClose]  else {
            return "0.0(0%)"
        }
        let diffrence = (Double(truncating: lastTrade as! NSNumber) - Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        let percentage = (diffrence*100/Double(truncating: prevoiusClose as! NSNumber)).rounded(toPlaces: 2)
        
        return "\(String(diffrence))(\(String(percentage))%)"
        
    }
    
    
   
    
}
