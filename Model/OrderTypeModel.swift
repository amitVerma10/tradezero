//
//  OrderTypeModel.swift
//  TradeZero
//
//  Created by Amit Verma on 1/23/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OrderTypeModel {
    public var shareOfValue : String?
    public var limitPrice : String?
    public var stopPrice : String?
    public var stopLimitPrice : String?
    public var abovePrice : String?
    public var belowPrice : String?
    
    required public init?(_ dictionary: KeyValue) {
        
        self.shareOfValue = dictionary["shareOfValue"] as? String ?? ""
        self.limitPrice = dictionary["limitPrice"] as? String ?? ""
        self.stopPrice = dictionary["stopPrice"] as? String ?? ""
        self.stopLimitPrice = dictionary["stopLimitPrice"] as? String ?? ""
        self.abovePrice = dictionary["abovePrice"] as? String ?? ""
        self.belowPrice = dictionary["belowPrice"] as? String ?? ""
        
    }
    

}
