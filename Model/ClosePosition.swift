//
//  ClosePosition.swift
//  TradeZero
//
//  Created by Amit Verma on 3/15/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class ClosePosition {

    var date = Date()
    
    public var companySym : String?
    public var currentPrice : Double?
    public var shares : Double?
    public var closePrice : Double?
    public var entryPrice : Double?
    public var enrtyDate : String?
    public var enrtyTime : String?
    public var stocktType : String?
    public var profiLoss : String?
    public var bid : Double?
    public var ask : Double?
    public var volume : Int?
    
    
    
    required public init?(_ dictionary: KeyValue) {
        currentPrice = dictionary[AppKey.lastTradePrice] as? Double ?? 0.00
        shares = dictionary[AppKey.quantityClosed] as? Double ?? 0.0
        companySym = dictionary[AppKey.orderSymbol] as? String ?? "N/A"
        closePrice = (dictionary[AppKey.closePrice] as? Double ?? 0.0)/100
        entryPrice = (dictionary[AppKey.entryPriceCents] as? Double ?? 0.0)/100
        enrtyDate = dictionary[AppKey.createTimeDate] as? String ?? "N/A"
        enrtyTime = dictionary[AppKey.createTimeDate] as? String ?? "N/A"
        profiLoss = dictionary["p/l"] as? String ?? "N/A"
        stocktType = dictionary[AppKey.orderType] as? String ?? "N/A"
        bid = dictionary[AppKey.quoteBestBid] as? Double ?? 0.00
        ask = dictionary[AppKey.quoteBestAsk] as? Double ?? 0.00
        volume = dictionary[AppKey.tradeVolume] as? Int ?? 0
        
        if let timeStamp =  dictionary[AppKey.createTimeDate] as? Int{
            print(timeStamp)
            let timeString = String(timeStamp/1000)
            self.enrtyDate = timeString.convertTimeStampToDateString()
            self.enrtyTime = timeString.convertTimeStampToTimeString()
        }
        
    }
    
}
