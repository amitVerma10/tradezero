//
//  OpenOrderModel.swift
//  TradeZero
//
//  Created by Amit Verma on 3/19/18.
//  Copyright © 2018 Amit Verma. All rights reserved.
//

import UIKit

class OpenOrderModel {
    
    public var companySym : String?
    public var limitPrice : Int?
    public var stopPrice : Int?
    public var orderType   : String?
    public var quantity  : Int?
    public var route  : String?
    public var side  : String?
    public var orderStatus : String?
    public var timeInForce : String?
    public var shares : Int?
    public var clientOrderId : String?
    
    required public init?(_ dictionary: KeyValue) {
        
        
        companySym =  dictionary[AppKey.openOrderSymbol] as? String ?? "N/A"
        shares =  dictionary[AppKey.shares] as? Int ?? 0
        limitPrice =  dictionary[AppKey.limitPriceCents] as? Int ?? 0
        stopPrice =  dictionary[AppKey.stopPriceCents] as? Int ?? 0
        orderType =  dictionary[AppKey.openOrderType] as? String ?? "N/A"
        quantity =  dictionary[AppKey.openOrderQuantiry] as? Int ?? 0
        route =  dictionary[AppKey.openOrderRoute] as? String ?? "N/A"
        side =  dictionary[AppKey.openOrderSide] as? String ?? "N/A"
        orderStatus =  dictionary[AppKey.openOrderStatus] as? String ?? "N/A"
        timeInForce =  dictionary[AppKey.openTimeInForce] as? String ?? "N/A"
        clientOrderId =  dictionary["clientOrderId"] as? String ?? "N/A"
        
        
    }
    
    

}

